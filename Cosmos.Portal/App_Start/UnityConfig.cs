﻿using Cosmos.DataServices.Dapper;
using Cosmos.DataServices.Interfaces;
using Cosmos.GeoAPI;
using Cosmos.GoogleAPI;
using Cosmos.Services.Class;
using Cosmos.Services.Interfaces;
using System.Web.Mvc;
using Unity;
using Unity.AspNet.Mvc;
using IPasswordReset = Cosmos.Services.Interfaces.IPasswordReset;

namespace Cosmos.Portal
{
    public static class UnityConfig
    {
        /// <summary>
        /// Registers the components.
        /// </summary>
        public static void RegisterComponents()
        {
            UnityContainer container = new UnityContainer();

            container.RegisterType<IUserService, UserService>();
            container.RegisterType<ICategoryService, CategoryService>();
            container.RegisterType<IAppService, AppService>();
            container.RegisterType<IAppVersionService, AppVersionService>();
            container.RegisterType<IApplicationUserActionService, ApplicationUserActionService>();
            container.RegisterType<ICarService, CarService>();
            container.RegisterType<IPasswordReset, PasswordResetService>();
            container.RegisterType<IMessageService, MessageService>();
            container.RegisterType<IMessageDataService, MessageDataServices>();
            container.RegisterType<ISOService, SOService>();
            container.RegisterType<ISODataServices, SODataServices>();
            container.RegisterType<IApproverDataServices, ApproverDataServices>();
            container.RegisterType<IApproverService, ApproverService>();
            container.RegisterType<IRegionDataServices, RegionDataServices>();
            container.RegisterType<IStateDataServices, StateDataServices>();
            container.RegisterType<IRegionService, RegionService>();
            container.RegisterType<IStateService, StateService>();
            container.RegisterType<IOpenStreetMap, OpenStreetMap>();
            container.RegisterType<IGlobalService, GlobalService>();
            container.RegisterType<IGlobalDataServices, GlobalDataServices>();
            container.RegisterType<ICityDataServices, CityDataServices>();
            container.RegisterType<ICityService, CityService>();
            container.RegisterType<IVehicleService, VehicleService>();
            container.RegisterType<IVehicle, VehicleDataServices>();
            container.RegisterType<ICar, CarDataServices>();
            container.RegisterType<ICityService, CityService>();
            container.RegisterType<ICityDataServices, CityDataServices>();
            container.RegisterType<IAppStoreDataService, AppStoreDataServices>();
            container.RegisterType<IApplicationUserAction, ApplicationUserActionDataServices>();
            container.RegisterType<ICategory, CategoryDataServices>();
            container.RegisterType<IApplicationSuggestionDataServices, ApplicationSuggestionDataServices>();
            container.RegisterType<IApplicationSuggestionService, ApplicationSuggestionService>();
            container.RegisterType<ITermsAndConditionsDataServices, TermsAndConditionsDataServices>();
            container.RegisterType<ITermsAndConditionsService, TermsAndConditionsService>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}