﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Portal.ViewModel
{
    public class SOViewModel
    {
        public SOViewModel()
        {
            SOs = new List<SO>();
            SO = new SO();
            brands = new List<Brand>();
            vehicles = new List<Vehicle>();
            vehicle = new Vehicle();
            brand = new Brand();
        }

        public List<Brand> brands { get; set; }
        public List<Vehicle> vehicles { get; set; }
        public Vehicle vehicle { get; set; }
        public Brand brand { get; set; }
        public List<SO> SOs { get; set; }
        public SO SO { get; set; }
    }
}