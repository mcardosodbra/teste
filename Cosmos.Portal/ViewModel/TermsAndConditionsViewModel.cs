﻿using System.Collections.Generic;

namespace Cosmos.Portal.ViewModel
{
    public class TermsAndConditionsViewModel
    {
        public TermsAndConditionsViewModel()
        {
            TermsAndConditions = new List<Model.TermsAndConditions>();
            TermAndCondition = new Model.TermsAndConditions();
            Brands = new List<Model.Brand>();
        }

        public List<Model.Brand> Brands { get; set; }
        public List<Model.TermsAndConditions> TermsAndConditions { get; set; }
        public Model.TermsAndConditions TermAndCondition { get; set; }
    }
}