﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cosmos.Portal.ViewModel
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            apps = new List<Model.Application>();
            categories = new List<Model.Category>();
        }

        public Model.Category category { get; set; }
        public List<Model.Application> apps { get; set; }
        public List<Model.Category> categories { get; set; }
    }
}