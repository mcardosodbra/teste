﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cosmos.Portal.ViewModel
{
    public class DocumentViewModel
    {
        public DocumentViewModel()
        {
            documents = new List<Model.Document>();
            document = new Model.Document();
            documentType = new Model.DocumentType();
            documentTypes = new List<Model.DocumentType>();
            documentOwners = new List<Model.DocumentOwner>();
        }

        public Model.Document document { get; set; }
        public Model.DocumentType documentType { get; set; }

        public List<Model.DocumentOwner> documentOwners { get; set; }
        public List<Model.Document> documents { get; set; }
        public List<Model.DocumentType> documentTypes { get; set; }
    }
}