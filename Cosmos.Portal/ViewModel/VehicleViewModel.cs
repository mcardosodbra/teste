﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cosmos.Portal.ViewModel
{
    public class VehicleViewModel
    {
        public VehicleViewModel()
        {
            vehicle = new Model.Vehicle();
            vehicles = new List<Model.Vehicle>();
            brand = new Model.Brand();
            brands = new List<Model.Brand>();
            appVersions = new List<Model.AppVersion>();
            userApps = new List<Model.UserApp>();
            cars = new List<Model.Car>();
            manuals = new List<Model.Manual>();
            guis = new List<Model.GUI>();

        }

        public Model.Vehicle vehicle { get; set; }
        public Model.Brand brand { get; set; }

        public List<Model.GUI> guis { get; set; }
        public List<Model.Manual> manuals { get; set; }
        public List<Model.Car> cars { get; set; }
        public List<Model.UserApp> userApps { get; set; }
        public List<Model.AppVersion> appVersions { get; set; }
        public List<Model.Vehicle> vehicles { get; set; }
        public List<Model.Brand> brands { get; set; }
    }
}