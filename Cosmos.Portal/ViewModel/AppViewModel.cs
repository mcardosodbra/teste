﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cosmos.Portal.ViewModel
{
    public class AppViewModel
    {
        public AppViewModel()
        {
            apps = new List<Model.Application>();
            categories = new List<Model.Category>();
            userApps = new List<Model.UserApp>();
            category = new Model.Category();
            purchases = new List<Model.Purchase>();
			appVersion = new Model.AppVersion();
			appVersions = new List<Model.AppVersion>();
            opSystem = new Model.OperationalSystem();
            opSystems = new List<Model.OperationalSystem>();
        }
        
        public Model.Application app { get; set; }
        public Model.Category category { get; set; }
		public Model.AppVersion appVersion { get; set; }
        public Model.OperationalSystem opSystem { get; set; }

        public List<Model.OperationalSystem> opSystems { get; set; }
        public List<Model.AppVersion> appVersions { get; set; } 
        public List<Model.Application> apps { get; set; }
        public List<Model.Category> categories { get; set; }
        public List<Model.UserApp> userApps { get; set; }
        public List<Model.Purchase> purchases { get; set; }

    }
}