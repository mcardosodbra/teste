﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Portal.ViewModel
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            users = new List<User>();
            types = new List<UserType>();
            apps = new List<UserApp>();
            type = new UserType();
            brand = new Brand();
            brands = new List<Brand>();
            Permissions = new List<Permission>();
        }

        public User user { get; set; }
        public UserType type { get; set; }
        public Brand brand { get; set; }
        public List<User> users { get; set; }
        public List<UserType> types { get; set; }
        public List<UserApp> apps { get; set; }
        public List<Brand> brands { get; set; }
        public List<Permission> Permissions { get; set; }
        public List<Permission> PermissionsSet { get; set; }
    }
}