﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cosmos.Portal.ViewModel
{
	public class BrandViewModel
	{
		public BrandViewModel()
		{			
			brand = new Model.Brand();
			brands = new List<Model.Brand>();
			vehicles = new List<Model.Vehicle>();
			vehicle = new Model.Vehicle();
			users = new List<Model.User>();
			user = new Model.User();
		}

		public Model.Brand brand { get; set; }
		public Model.Vehicle vehicle { get; set; }
		public Model.User user { get; set; }

		public List<Model.Brand> brands { get; set; }
		public List<Model.Vehicle> vehicles { get; set; }
		public List<Model.User> users { get; set; }
	}
}