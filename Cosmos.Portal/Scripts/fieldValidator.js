﻿
function FieldValidate(controlName, url, data, operationTitle) {
    event.preventDefault();
    $('#' + controlName).on("blur", function ()
    {
        var nameContent = $('#' + controlName).val();

        if (nameContent == '' && nameContent == undefined)
            return;

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (response) {

                console.log(response);

                if (response.result == "fail") {
                    toastr.error(response.msg, operationTitle);
                    $('#' + controlName).focus();
                }
            }
        })
    });
}
