﻿using Cosmos.Portal.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cosmos.Portal.Helpers
{
    public class WebTools
    {
        public static class Validations
        {
     
        }

        public static class Imagem
        {
            public static MvcHtmlString RenderHTMLImage(string base64Image)
            {
              return new MvcHtmlString($@"<img src='data:image/jpg;base64,{base64Image}' />");
            }

            public static string ConvertUploadImageToBase64Image(HttpPostedFileWrapper Image)
            {

                string fotoBase64 = string.Empty;

                using (BinaryReader reader = new BinaryReader(Image.InputStream))
                {
                    fotoBase64 = Convert.ToBase64String(reader.ReadBytes(Image.ContentLength));
                }

                return fotoBase64;
            }
        }

   

   }
}