﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cosmos.Portal.Helpers
{
    public static class SessionContext
    {
         /// <summary>
        /// Application session user
        /// </summary>
        public static Model.User UserData
        {
            get
            {
                if (HttpContext.Current.Session != null)
                {
                    return (Model.User)HttpContext.Current.Session["user"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session["user"] = value;
            }
        }

        public static Object Persistencia { get; set; }

        private static Model.Application appPersistence { get; set; }

        public static Model.Application AppPersistence
        {
            get
            {
                if (appPersistence == null)
                    appPersistence = new Model.Application();

                return appPersistence;
            }
            set {
                appPersistence = value;
            }
        }

        private static Model.Brand brandPersistence { get; set; }

        public static Model.Brand BrandPersistence
        {
            get
            {
                if (brandPersistence == null)
                    brandPersistence = new Model.Brand();

                return brandPersistence;
            }
            set
            {
                brandPersistence = value;
            }
        }

		private static Model.Document documentPersistence { get; set; }

		public static Model.Document DocumentPersistence
		{
			get
			{
				if (documentPersistence == null)
					documentPersistence = new Model.Document();

				return documentPersistence;
			}
			set
			{
				documentPersistence = value;
			}
		}
	}
}