﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace Cosmos.Portal.Helpers
{
    public class Arquivo
    {
        public enum TIPOS_ARQUIVO{ APK, IMAGEM, DOCUMENTO}

        public static string SaveFIleToServer(HttpPostedFileBase sentFile, TIPOS_ARQUIVO Tipo, string Version="")
        {

            string fileName = string.Empty;
            string fileExtension = string.Empty;
            var configuration = WebConfigurationManager.OpenWebConfiguration("~");
            var section = (AppSettingsSection)configuration.GetSection("appSettings");
            string diretorioDestino = string.Empty;
            string caminhoServidor = HttpRuntime.AppDomainAppPath;

            try
            {
                if (sentFile == null)
                    throw new Exception("Arquivo não informado");

                fileName = sentFile.FileName.Substring(0, sentFile.FileName.LastIndexOf("."));
                fileExtension = sentFile.FileName.Substring(sentFile.FileName.LastIndexOf("."), (sentFile.FileName.Length - sentFile.FileName.LastIndexOf(".")));
                fileName = !string.IsNullOrEmpty(Version) ? string.Format("{0}-{1}.{2}", fileName, Version, fileExtension) : sentFile.FileName;
               
                if (Tipo == TIPOS_ARQUIVO.APK)
                    diretorioDestino = string.Format(@"{0}\{1}\{2}", caminhoServidor, section.Settings["DiretorioAPKs"].Value, fileName);
                
				if (Tipo == TIPOS_ARQUIVO.DOCUMENTO)
					diretorioDestino = string.Format(@"{0}\{1}\{2}", caminhoServidor, section.Settings["DiretorioDocuments"].Value, fileName);

				using (var fileStream = new MemoryStream())
                {
                    sentFile.InputStream.CopyTo(fileStream);
                    File.WriteAllBytes(diretorioDestino, fileStream.ToArray());
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return fileName;
        }
    }
}