﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class ErrorHandlerController : Controller
    {
        // GET: ErrorHandler
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult redirectToErrorPage(string msg)
        {
            TempData["ErrorMessage"] = msg;
            return RedirectToAction("Index", "ErrorHandler");
        }
    }
}