﻿using Cosmos.DataServices;
using Cosmos.Log;
using Cosmos.Model;
using Cosmos.Portal.Helpers;
using Cosmos.Portal.ViewModel;
using Cosmos.Services.Interfaces;
using Cosmos.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class AppController : Controller
    {
        private static AppViewModel _viewModel = new AppViewModel();

        private readonly IAppService _appService;
        private readonly IApplicationSuggestionService _applicationSuggestionService;

        public AppController(IAppService appService, IApplicationSuggestionService applicationSuggestionService)
        {
            _appService = appService;
            _applicationSuggestionService = applicationSuggestionService;
        }

        public ActionResult Index()
        {
            return List();
        }

        public ActionResult Form()
        {
            AppViewModel viewmodel = new AppViewModel();
            try
            {
                SessionContext.AppPersistence.APK = null;
                SessionContext.AppPersistence.Logotype = string.Empty;
                viewmodel.categories = new DataServices.CategoryDataServices().GetAllCategory();
                viewmodel.opSystems = new DataServices.OpSystemDataServices().GetAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
            }

            return View(viewmodel);
        }

        public ActionResult RemovedApps()
        {
            AppViewModel viewmodel = new AppViewModel();
            DataServices.AppStoreDataServices svc_ = new DataServices.AppStoreDataServices();

            try
            {
                viewmodel.apps = svc_.GetRemovedApps();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                svc_ = null;
            }

            return View(viewmodel);
        }

        public ActionResult Edit(string AppId)
        {
            AppStoreDataServices appSrv = new AppStoreDataServices();

            AppViewModel viewmodel = new AppViewModel();
            viewmodel.app = _appService.GetById(AppId);
            // viewmodel.app = appSrv.GetApplicationById(AppId);
            _viewModel = viewmodel;

            return View(viewmodel);
        }

        public ActionResult Remove(string AppId)
        {
            AppStoreDataServices appSrv = new AppStoreDataServices();

            AppViewModel viewmodel = new AppViewModel();
            viewmodel.app = appSrv.GetApplicationById(AppId);
            _viewModel = viewmodel;

            return View(viewmodel);
        }

        public ActionResult Active(string AppId)
        {
            AppStoreDataServices appSrv = new AppStoreDataServices();

            AppViewModel viewmodel = new AppViewModel();
            viewmodel.app = appSrv.GetApplicationById(AppId);
            _viewModel = viewmodel;

            return View(viewmodel);
        }

        public ActionResult DownGrade(string versionId)
        {
            if (_viewModel.app.Versions.Any(x => x.Active))
            {
                _viewModel.app.ActiveVersion = _viewModel.app.Versions.FirstOrDefault(s => s.Active == true).VersionCode;
            }
            else
            {
                _viewModel.app.ActiveVersion = 0;
            }

            _viewModel.appVersion = _viewModel.app.Versions.FirstOrDefault(s => s.Id == versionId);

            if (_viewModel.app.ActiveVersion == 0)
                _viewModel.app.ActiveVersion = _viewModel.app.Versions.Max(s => s.VersionCode);
            return View(_viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DownGrade(AppViewModel viewmodel)
        {
            try
            {
                var obj = new AppVersion();
                if (_viewModel.app.Versions.Any(x => x.Active))
                {
                    obj = _viewModel.app.Versions.FirstOrDefault(s => s.Active == true);
                    obj.Active = false;
                    new DataServices.AppVersionDataServices().Update(obj);
                }

                _viewModel.appVersion.Active = true;
                _viewModel.appVersion.DowngradeJustify = viewmodel.appVersion.DowngradeJustify;

                new DataServices.AppVersionDataServices().Update(_viewModel.appVersion);
                return RedirectToAction("Profile", new { AppId = _viewModel.app.Id });
            }
            catch (Exception)
            {
                return View(viewmodel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Active(AppViewModel viewmodel)
        {
            try
            {
                new DataServices.AppStoreDataServices().Active(_viewModel.app.Id);
                return RedirectToAction("List");
            }
            catch (Exception)
            {
                return View(viewmodel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Remove(AppViewModel viewmodel)
        {
            try
            {
                new DataServices.AppStoreDataServices().Remove(_viewModel.app.Id, viewmodel.app.RemoveFromCentral);
                return RedirectToAction("List");
            }
            catch (Exception)
            {
                return View(viewmodel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(AppViewModel viewmodel)
        {
            try
            {
                _viewModel.app.Name = viewmodel.app.Name;

                _viewModel.app.Description = viewmodel.app.Description;
                _viewModel.app.DescriptionEN = viewmodel.app.DescriptionEN;
                _viewModel.app.DescriptionES = viewmodel.app.DescriptionES;
                _viewModel.app.Relevance = viewmodel.app.Relevance;
                if (viewmodel.app.LogoTypeData != null)
                {
                    var url = BlobConnector.UploadFile(viewmodel.app.LogoTypeData, "images");
                    if (string.IsNullOrEmpty(url))
                        throw new Exception("Erro ao fazer o upload da imagem");
                    _viewModel.app.Logotype = url;
                }
                _viewModel.app.LastUpdate = DateTime.Now;
                new DataServices.AppStoreDataServices().Update(_viewModel.app);
                return RedirectToAction("List");
            }
            catch (Exception)
            {
                return View(viewmodel);
            }
        }

        public ActionResult List()
        {
            AppViewModel viewmodel = new AppViewModel();
            DataServices.AppStoreDataServices svc_ = new DataServices.AppStoreDataServices();

            try
            {
                viewmodel.apps = _appService.GetAllPortal();
                _viewModel = viewmodel;
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                svc_ = null;
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(AppViewModel viewmodel)
        {
            try
            {
                if (viewmodel.app.LogoTypeData == null)
                    throw new Exception("Escolha um logotipo para a publicação do seu aplicativo.");
                var url = BlobConnector.UploadFile(viewmodel.app.LogoTypeData, "images");

                if (string.IsNullOrEmpty(url))
                    throw new Exception("Erro ao fazer o upload da imagem");

                viewmodel.app.Logotype = url;
                viewmodel.app.Status = true;
                viewmodel.app.Type = Model.ApplicationType.App;

                if (viewmodel.appVersion.ApkData == null)
                    throw new Exception("Escolha um arquivo APK para a publicação do seu aplicativo.");

                var apkUrl = BlobConnector.UploadFile(viewmodel.appVersion.ApkData, "apks");
                if (string.IsNullOrEmpty(apkUrl))
                    throw new Exception("Erro ao fazer o upload do apk");

                viewmodel.appVersion.ApkUri = apkUrl;

                viewmodel.appVersion.ApkSize = viewmodel.appVersion.ApkData.ContentLength;
                viewmodel.app.InclusionDate = DateTime.Parse(string.Format("{0:yyyy/MM/dd HH:mm:ss.fff}", DateTime.Now));
                viewmodel.appVersion.ReleaseDate = DateTime.Parse(string.Format("{0:yyyy/MM/dd HH:mm:ss.fff}", DateTime.Now));
                viewmodel.appVersion.Package = viewmodel.app.PackageName;
                viewmodel.app.Versions.Add(viewmodel.appVersion);
                new DataServices.AppStoreDataServices().Add(viewmodel.app);
                return RedirectToAction("List");
            }
            catch (Exception err)
            {
                new NLogExceptionLogger().Log("Application - Add" + err.Message, Model.LogType.Error);
                return View(viewmodel);
            }
            finally
            {
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ActiveVersion(string VersionID)
        {
            try
            {
                DataServices.AppVersionDataServices srv = new DataServices.AppVersionDataServices();

                if (srv.Active(VersionID))
                {
                    return Json(new { result = "fail", msg = "Não foi possivel ativar essa versão" });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void UploadThumbnail(IEnumerable<HttpPostedFileBase> file)
        {
            try
            {
                SessionContext.AppPersistence.Logotype = string.Empty;

                if (file != null && file.Count() > 0)
                    SessionContext.AppPersistence.Logotype = Helpers.WebTools.Imagem.ConvertUploadImageToBase64Image((System.Web.HttpPostedFileWrapper)file.First());
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                SessionContext.Persistencia = null;
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void ApkUpload(IEnumerable<HttpPostedFileBase> file)
        {
            try
            {
                SessionContext.AppPersistence.APK = null;

                if (file != null && file.Count() > 0)
                    SessionContext.AppPersistence.APK = file.First();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
        }

        public new ActionResult Profile(string AppId)
        {
            AppStoreDataServices appSrv = new AppStoreDataServices();

            AppViewModel viewmodel = new AppViewModel();
            viewmodel.app = _appService.GetApplicationByIdPortal(AppId);

            _viewModel = viewmodel;
            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult NameValidator(string Name)
        {
            try
            {
                DataServices.AppStoreDataServices srv = new DataServices.AppStoreDataServices();

                if (srv.IsApplicationNameInUse(Name))
                {
                    return Json(new { result = "fail", msg = "Nome já utilizado. Por favor, informe outro nome." });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VersionValidator(string VersionToValidate)
        {
            try
            {
                DataServices.AppVersionDataServices srv = new DataServices.AppVersionDataServices();

                if (srv.IsVersionInUse(VersionToValidate))
                {
                    return Json(new { result = "fail", msg = "Versão para esse aplicativo já está sendo utilizado. Por favor, informe outra versão." });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        public ActionResult SuggestionList()
        {
            var viewmodel = _applicationSuggestionService.SelectApplicationSuggestion();
            return View(viewmodel);
        }
    }
}