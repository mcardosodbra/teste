﻿using Cosmos.Portal.Helpers;
using Cosmos.Portal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class BrandController : Controller
    {
        public ActionResult Index()
        {
            return List();
        }

        public ActionResult Form()
        {
            BrandViewModel viewmodel = new BrandViewModel();

            SessionContext.BrandPersistence.Logotype = null;

            return View(viewmodel);
        }

        public ActionResult Edit(string BrandId)
        {
            BrandViewModel viewmodel = new BrandViewModel();

            try
            {
                SessionContext.BrandPersistence.Logotype = null;
                viewmodel.brand = new DataServices.BrandDataServices().GetBrandById(BrandId);
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult List()
        {
            BrandViewModel viewmodel = new BrandViewModel();
            DataServices.BrandDataServices brand_srv = new DataServices.BrandDataServices();

            try
            {
                viewmodel.brands = brand_srv.GetAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                brand_srv = null;
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Add(BrandViewModel viewmodel)
        {
            try
            {
                viewmodel.brand.Logotype = SessionContext.BrandPersistence.Logotype;

                if (new DataServices.BrandDataServices().AddBrand(viewmodel.brand))
                    return Json(new { result = "Ok", msg = "Montadora cadastrada com sucesso", url = Url.Action("List", "Brand") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Montadora não pode ser cadastrada" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult BrandNameValidator(string BrandName)
        {
            try
            {
                DataServices.BrandDataServices srv = new DataServices.BrandDataServices();

                if (srv.IsBrandNameInUse(BrandName))
                {
                    return Json(new { result = "fail", msg = "Nome já utilizado. Por favor, informe outro nome." });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(BrandViewModel viewmodel)
        {
            try
            {
                viewmodel.brand.Logotype = SessionContext.BrandPersistence.Logotype;

                if (new DataServices.BrandDataServices().UpdateBrand(viewmodel.brand))
                    return Json(new { result = "Ok", msg = "Montadora atualizada com sucesso", url = Url.Action("List", "Brand") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Montadora não pode ser atualizada" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void UploadThumbnail(IEnumerable<HttpPostedFileBase> file)
        {
            try
            {
                SessionContext.BrandPersistence.Logotype =
                        Helpers.WebTools.Imagem.ConvertUploadImageToBase64Image((System.Web.HttpPostedFileWrapper)file.First());
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                SessionContext.Persistencia = null;
            }
        }
    }
}