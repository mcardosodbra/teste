﻿using Cosmos.Portal.ViewModel;
using Cosmos.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using Cosmos.DataServices.Dapper;

namespace Cosmos.Portal.Controllers
{
    public class UserController : Controller
    {
        private readonly IGlobalService _globalService;

        public UserController(IGlobalService globalService)
        {
            _globalService = globalService;
        }

        public ActionResult Form()
        {
            UserViewModel viewmodel = new UserViewModel();
            try
            {
                viewmodel.types = new DataServices.UserTypeDataServices().GetAll();
                viewmodel.brands = new DataServices.BrandDataServices().GetAll();
                viewmodel.Permissions = new UserDataServices().GetPermissionList();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult Edit(string UserId)
        {
            UserViewModel viewmodel = new UserViewModel();

            try
            {
                viewmodel.user = new DataServices.UserDataServices().GetUserById(UserId);
                viewmodel.types = new DataServices.UserTypeDataServices().GetAll();
                viewmodel.brands = new DataServices.BrandDataServices().GetAll();
                viewmodel.Permissions = new UserDataServices().GetPermissionList();
                viewmodel.PermissionsSet = new UserDataServices().GetPermissionListByUser(Guid.Parse(UserId));

                foreach (var perm in viewmodel.Permissions)
                {
                    if (viewmodel.PermissionsSet.Any(p => p.Id == perm.Id))
                        perm.Checked = true;
                }
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult List()
        {
            UserViewModel viewmodel = new UserViewModel();
            DataServices.UserDataServices user_srv = new DataServices.UserDataServices();

            try
            {
                viewmodel.users = user_srv.GetUserAll().Where(x => x.TypeId != "3").ToList();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                user_srv = null;
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNewUser(UserViewModel viewmodel)
        {
            try
            {
                viewmodel.user.InclusionDate = DateTime.Parse(string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now)); ;
                var passwordHash = Utils.HashUtil.GetSha256FromString(viewmodel.user.Password);
                viewmodel.user.Password = passwordHash;

                viewmodel.user.Id = Guid.NewGuid().ToString();

                if (new UserDataServices().AddUser(viewmodel.user))
                {
                    string permissions = null;

                    foreach (var perm in viewmodel.Permissions)
                    {
                        if (perm.Checked)
                            permissions += perm.Id.ToString() + ",";
                    }

                    new Cosmos.DataServices.Dapper.UserDataServices().UpdatePermissions(Guid.Parse(viewmodel.user.Id), permissions);
                    return Json(new
                    { result = "Ok", msg = "Usuario cadastrado com sucesso", url = Url.Action("List", "User") });
                }
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Usuario não pode ser cadastrada" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult EmailValidator(string EmailToValidate)
        {
            try
            {
                DataServices.UserDataServices srv = new DataServices.UserDataServices();

                if (string.IsNullOrEmpty(EmailToValidate))
                    return Json(new { result = "fail", msg = "E-mail obrigatório. Por favor, informe." });

                if (srv.IsEmailInUse(EmailToValidate))
                {
                    return Json(new { result = "fail", msg = "E-mail utilizado. Por favor, informe outro e-mail." });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(UserViewModel viewmodel)
        {
            try
            {
                var objUser = new UserDataServices();
                var ret = objUser.UpdateUser(viewmodel.user);
                string permissions = null;

                foreach (var perm in viewmodel.Permissions)
                {
                    if (perm.Checked)
                        permissions += perm.Id.ToString() + ",";
                }

                new Cosmos.DataServices.Dapper.UserDataServices().UpdatePermissions(Guid.Parse(viewmodel.user.Id), permissions);

                if (ret)
                    return Json(new { result = "Ok", msg = "Usuário atualizado com sucesso", url = Url.Action("List", "User") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            return Json(new { result = "Fail", msg = "Usuário não pode ser atualizado" });
        }
    }
}