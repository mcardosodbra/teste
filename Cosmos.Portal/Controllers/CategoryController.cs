﻿using Cosmos.Portal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cosmos.Utils;
using Cosmos.DataServices.Interfaces;

namespace Cosmos.Portal.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategory _category;

        public CategoryController(ICategory category)
        {
            _category = category;
        }

        //public ActionResult Index()
        //{
        //    try
        //    {
        //    }
        //    catch (Exception err)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //    return List();
        //}

        public ActionResult Form()
        {
            CategoryViewModel viewmodel = new CategoryViewModel();

            return View(viewmodel);
        }

        public ActionResult Edit(string CategoryId)
        {
            CategoryViewModel viewmodel = new CategoryViewModel();

            try
            {
                viewmodel.category = new DataServices.CategoryDataServices().GetCategoryById(CategoryId);
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult List()
        {
            CategoryViewModel viewmodel = new CategoryViewModel();
            DataServices.CategoryDataServices category_srv = new DataServices.CategoryDataServices();

            try
            {
                viewmodel.categories = _category.GetAllCategory();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                category_srv = null;
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(CategoryViewModel viewmodel)
        {
            try
            {
                if (viewmodel.category.imageData != null)
                {
                    var url = BlobConnector.UploadFile(viewmodel.category.imageData, "images");
                    if (string.IsNullOrEmpty(url))
                        return Json(new { result = "Fail", msg = "Erro ao fazer o upload da imagem" });
                    viewmodel.category.IconUrl = url;
                }

                new DataServices.CategoryDataServices().UpdateCategory(viewmodel.category);
                return RedirectToAction("List", "Category");
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
                return View(viewmodel);
            }

            //  return Json(new { result = "Fail", msg = "Categoria não pode ser atualizada" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(CategoryViewModel viewmodel)
        {
            try
            {
                var url = BlobConnector.UploadFile(viewmodel.category.imageData, "images");
                if (string.IsNullOrEmpty(url))
                    return Json(new { result = "Fail", msg = "Erro ao fazer o upload da imagem" });
                viewmodel.category.IconUrl = url;
                viewmodel.category.Id = new Guid().ToString();
                viewmodel.category.Status = true;
                new DataServices.CategoryDataServices().AddCategory(viewmodel.category);
                return RedirectToAction("List", "Category");
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
                return View(viewmodel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult CategoryNameValidator(string CategoryName)
        {
            try
            {
                DataServices.CategoryDataServices srv = new DataServices.CategoryDataServices();

                if (srv.IsCategoryNameInUse(CategoryName))
                {
                    return Json(new { result = "fail", msg = "Nome já utilizado. Por favor, informe outro nome." });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ExistAppValidator(string categoryID)
        {
            try
            {
                DataServices.AppStoreDataServices srv = new DataServices.AppStoreDataServices();
                var ret = srv.GetByCategory(categoryID);
                if (ret.Any())
                    return Json(new { result = "fail", msg = "Existem Aplicativos cadastrados para essa categoria." });
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public JsonResult Delete(string categoryID)
        {
            try
            {
                DataServices.CategoryDataServices srv = new DataServices.CategoryDataServices();
                srv.Delete(categoryID);

                return Json(new { result = "fail", msg = "Categoria deletada." });
            }
            catch (Exception err)
            {
                throw err;
            }
        }
    }
}