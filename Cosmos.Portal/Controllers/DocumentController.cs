﻿using Cosmos.Portal.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cosmos.Portal.Helpers;
using System.IO;

namespace Cosmos.Portal.Controllers
{
    public class DocumentController : Controller
    {
        public ActionResult Index()
        {
            return List();
        }

        public ActionResult Form()
        {
            DocumentViewModel viewmodel = new DocumentViewModel();
            try
            {
                SessionContext.DocumentPersistence.FileDocument = null;
                viewmodel.documentTypes = new DataServices.DocumentTypeDataServices().GetDocumentTypeAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult Edit(string DocumentId)
        {
            DocumentViewModel viewmodel = new DocumentViewModel();

            try
            {
                viewmodel.document = new DataServices.DocumentsDataServices().GetDocumentsById(DocumentId);
                viewmodel.documentTypes = new DataServices.DocumentTypeDataServices().GetDocumentTypeAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult List()
        {
            DocumentViewModel viewmodel = new DocumentViewModel();
            DataServices.DocumentsDataServices doc_srv = new DataServices.DocumentsDataServices();

            try
            {
                viewmodel.documents = doc_srv.GetDocumentsAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                doc_srv = null;
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(DocumentViewModel viewmodel)
        {
            try
            {
                if (SessionContext.DocumentPersistence.FileDocument == null)
                    throw new Exception("Escolha um arquivo para o cadastro do documento.");

                viewmodel.document.InclusionDate = DateTime.Now;

                Helpers.Arquivo.SaveFIleToServer((HttpPostedFileBase)SessionContext.DocumentPersistence.FileDocument, Arquivo.TIPOS_ARQUIVO.DOCUMENTO);

                if (new DataServices.DocumentsDataServices().AddDocuments(viewmodel.document))
                    return Json(new { result = "Ok", msg = "Documento cadastrado com sucesso", url = Url.Action("List", "Document") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Documento não pode ser cadastrado" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void DocUpload(IEnumerable<HttpPostedFileBase> file)
        {
            try
            {
                SessionContext.DocumentPersistence.FileDocument = null;

                if (file != null && file.Count() > 0)
                    SessionContext.DocumentPersistence.FileDocument = file.First();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
        }
    }
}