﻿using Cosmos.Portal.ViewModel;
using Cosmos.Services.Interfaces;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class SOController : Controller
    {
        private readonly ISOService _soService;
        private readonly IVehicleService _vehicleService;

        public SOController(ISOService soService, IVehicleService vehicleService)
        {
            _soService = soService;
            _vehicleService = vehicleService;
        }

        public ActionResult Form()
        {
            var viewmodel = new SOViewModel
            {
                brands = new DataServices.BrandDataServices().GetAll(),
                vehicles = new DataServices.VehicleDataServices().GetVehicleAll()
            };

            return View(viewmodel);
        }

        public ActionResult Index()
        {
            return List();
        }

        public ActionResult List()
        {
            try
            {
                return View(_soService.GetListSO());
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
                return null;
            }
        }

        public ActionResult Edit(Guid SOId)
        {
            var viewmodel = new SOViewModel();

            try
            {
                viewmodel.SO = _soService.GetSODetail(SOId);
                viewmodel.brands = new DataServices.BrandDataServices().GetAll();
                if (viewmodel.SO.Brand != null)
                    viewmodel.vehicles = new DataServices.VehicleDataServices().GetVehicleByBrand(new Guid(viewmodel.SO.Brand.Id));
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Insert(FormCollection content)
        {
            try
            {
                var releaseDate = DateTime.ParseExact(content["SO.ReleaseDate"] + " 00:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                var objViewModel = new SOViewModel();
                objViewModel.SO.Vehicle = new Model.Vehicle { Id = content["listVehicles"], BrandId = content["SO.Brand.Id"] };
                objViewModel.SO.Version = content["SO.Version"];
                objViewModel.SO.YearFab = int.Parse(content["SO.YearFab"]);
                objViewModel.SO.YearModel = int.Parse(content["SO.YearModel"]);
                objViewModel.SO.ReleaseDate = releaseDate;

                _soService.InsertSO(objViewModel.SO);
                return Json(new { result = "Ok", msg = "SO cadastrado com sucesso", url = Url.Action("List", "SO") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "SO não pode ser cadastrado" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(FormCollection content)
        {
            try
            {
                var releaseDate = DateTime.ParseExact(content["SO.ReleaseDate"] + " 00:00", "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                var objViewModel = new SOViewModel();
                objViewModel.SO.Id = new Guid(content["SO.Id"]);
                objViewModel.SO.Vehicle = new Model.Vehicle { Id = content["listVehicles"], BrandId = content["SO.Brand.Id"] };
                objViewModel.SO.Version = content["SO.Version"];
                objViewModel.SO.YearFab = int.Parse(content["SO.YearFab"]);
                objViewModel.SO.YearModel = int.Parse(content["SO.YearModel"]);
                objViewModel.SO.ReleaseDate = releaseDate;

                _soService.UpdateSO(objViewModel.SO);
                return Json(new { result = "Ok", msg = "SO modificado com sucesso", url = Url.Action("List", "SO") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "SO não pode ser modificado" });
        }

        public ActionResult Details()
        {
            return null;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Delete(SOViewModel viewmodel)
        {
            try
            {
                _soService.DeleteSO(viewmodel.SO);
                return Json(new { result = "Ok", msg = "SO excluído com sucesso", url = Url.Action("List", "SO") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "SO não pode ser excluído" });
        }

        [HttpPost]
        public JsonResult GetVehicleList(Guid brandId)
        {
            var vehicleList = _vehicleService.GetVehicleByBrand(brandId);

            return Json(vehicleList, JsonRequestBehavior.AllowGet);
        }
    }
}