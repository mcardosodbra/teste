﻿using System;
using System.Net;
using System.Web.Mvc;
using Cosmos.DataServices;
using Cosmos.Model;
using Cosmos.Portal.Helpers;

namespace Cosmos.Portal.Controllers
{
    public class PasswordResetController : Controller
    {
        private static string token;

        private static PasswordReset user;
        // GET: PasswordReset
        public ActionResult Index()
        {
            TempData["msg"] = "false";
            if (Request.QueryString["t"] != null)
            {
                token = Request.QueryString["t"].ToString();
                user = new DataServices.PasswordResetDataServices().GetByToken(token);
                if (user != null) 
                    TempData["msg"] = "true";
            }
            return View();
        }
        [HttpPost]
        public JsonResult Update(FormCollection viewmodel)
        {
            var password = viewmodel["password"];
            try
            {
                var userId = new DataServices.PasswordResetDataServices().GetByToken(token);
                if (userId == null)
                {
                    return Json(new { result = "Fail", msg = "Token Inválido!" });
                }
                var user = new DataServices.UserDataServices().GetUserById(userId.UserId);
                user.Password = Utils.HashUtil.GetSha256FromString(password);
                if (new DataServices.UserDataServices().UpdateUserPassword(user))
                {
                    userId.Status = false;
                    var t = new DataServices.PasswordResetDataServices().UpdateTokenStatus(userId);
                 
                    return Json(new {result = "Ok", msg = "Sua Senha foi cadastrada"});
                }
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Usuário não pode ser atualizado" });

        }
    }
}