﻿using Cosmos.Services.Interfaces;
using System;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class MessageController : Controller
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        public ActionResult Index()
        {
            return List();
        }

        public ActionResult List()
        {
            try
            {
                return View(_messageService.GetMessageList());
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
                return null;
            }
        }
    }
}