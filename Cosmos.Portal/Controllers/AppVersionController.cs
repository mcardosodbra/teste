﻿using Cosmos.Portal.ViewModel;
using Cosmos.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class AppVersionController : Controller
    {
        private static AppViewModel _viewmodels = new AppViewModel();

        // GET: AppVersion
        public ActionResult Add(string AppId)
        {
            var viewmodel = new AppViewModel();

            try
            {
                viewmodel.app = new DataServices.AppStoreDataServices().GetApplicationById(AppId);
                _viewmodels = viewmodel;
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
            }

            return View(viewmodel);
        }

        public ActionResult Edit(string VersionID)
        {
            var viewmodel = new AppViewModel();

            try
            {
                viewmodel.appVersion = new DataServices.AppVersionDataServices().GetAppVersionById(VersionID);
                _viewmodels = viewmodel;
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(AppViewModel viewmodel)
        {
            try
            {
                _viewmodels.appVersion.ReleaseNote = viewmodel.appVersion.ReleaseNote;
                _viewmodels.appVersion.ReleaseNoteES = viewmodel.appVersion.ReleaseNoteES;
                _viewmodels.appVersion.ReleaseNoteEN = viewmodel.appVersion.ReleaseNoteEN;
                _viewmodels.appVersion.Version = viewmodel.appVersion.Version;
                new DataServices.AppVersionDataServices().Update(_viewmodels.appVersion);

                return RedirectToAction("Profile", "App", new { Appid = _viewmodels.appVersion.ApplicationId });
            }
            catch (Exception)
            {
                return View(viewmodel);
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(AppViewModel viewmodel)
        {
            try
            {
                viewmodel.app = _viewmodels.app;
                viewmodel.appVersion.ApkSize = viewmodel.appVersion.ApkData.ContentLength;

                if (viewmodel.appVersion.ApkData == null)
                    throw new Exception("Escolha um arquivo APK para a publicação do seu aplicativo.");
                var apkUrl = BlobConnector.UploadFile(viewmodel.appVersion.ApkData, "apks");
                if (string.IsNullOrEmpty(apkUrl))
                    throw new Exception("Erro ao fazer o upload do apk");
                viewmodel.appVersion.ApkUri = apkUrl;
                viewmodel.appVersion.Status = true;
                viewmodel.appVersion.ReleaseDate = DateTime.Parse(string.Format("{0:yyyy/MM/dd HH:mm:ss.fff}", DateTime.Now));
                viewmodel.appVersion.Package = viewmodel.app.PackageName;
                viewmodel.appVersion.ApplicationId = viewmodel.app.Id;
                new DataServices.AppVersionDataServices().AddNewVersion(viewmodel.appVersion);
                return RedirectToAction("Profile", "App", new { Appid = _viewmodels.app.Id });
            }
            catch (Exception)
            {
                return View(viewmodel);
            }
        }
    }
}