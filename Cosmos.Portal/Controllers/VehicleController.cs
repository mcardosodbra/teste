﻿using Cosmos.Portal.ViewModel;
using System;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class VehicleController : Controller
    {
        // public ActionResult Index()
        // {
        //     try
        //     {
        //     }
        //     catch (Exception err)
        //     {
        //     }
        //     finally
        //     {
        //     }
        //     return List();
        // }

        public ActionResult Form()
        {
            VehicleViewModel viewmodel = new VehicleViewModel();
            try
            {
                viewmodel.brands = new DataServices.BrandDataServices().GetAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
            }

            return View(viewmodel);
        }

        public ActionResult Edit(string VehicleId)
        {
            VehicleViewModel viewmodel = new VehicleViewModel();
            DataServices.BrandDataServices brand_srv = new DataServices.BrandDataServices();
            try
            {
                viewmodel.vehicle = new DataServices.VehicleDataServices().GetVehicleById(VehicleId);
                viewmodel.brands = brand_srv.GetAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
            }

            return View(viewmodel);
        }

        public ActionResult List()
        {
            VehicleViewModel viewmodel = new VehicleViewModel();
            DataServices.VehicleDataServices vehicle_srv = new DataServices.VehicleDataServices();
            DataServices.BrandDataServices brand_srv = new DataServices.BrandDataServices();

            try
            {
                viewmodel.vehicles = vehicle_srv.GetVehicleAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
                vehicle_srv = null;
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddNewVehicle(VehicleViewModel viewmodel)
        {
            try
            {
                if (new DataServices.VehicleDataServices().AddVehicle(viewmodel.vehicle))
                    return Json(new { result = "Ok", msg = "Veículo cadastrado com sucesso", url = Url.Action("List", "Vehicle") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Veículo não pode ser cadastrado" });
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult VehicleNameValidator(string VehicleName)
        {
            try
            {
                DataServices.VehicleDataServices srv = new DataServices.VehicleDataServices();

                if (srv.IsVehicleNameInUse(VehicleName))
                {
                    return Json(new { result = "fail", msg = "Nome já utilizado. Por favor, informe outro nome." });
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return Json("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Update(VehicleViewModel viewmodel)
        {
            try
            {
                if (new DataServices.VehicleDataServices().UpdateVehicle(viewmodel.vehicle))
                    return Json(new { result = "Ok", msg = "Veículo atualizado com sucesso", url = Url.Action("List", "Vehicle") });
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return Json(new { result = "Fail", msg = "Veículo não pode ser atualizado" });
        }
    }
}