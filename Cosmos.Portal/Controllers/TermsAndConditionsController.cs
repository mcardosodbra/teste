﻿using Cosmos.Log;
using Cosmos.Portal.ViewModel;
using Cosmos.Services.Interfaces;
using Cosmos.Utils;
using System;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class TermsAndConditionsController : Controller
    {
        private readonly ITermsAndConditionsService _termsAndConditionsService;
        private static TermsAndConditionsViewModel _viewModel = new TermsAndConditionsViewModel();

        public TermsAndConditionsController(ITermsAndConditionsService termsAndConditionsService)
        {
            _termsAndConditionsService = termsAndConditionsService;
        }

        public ActionResult Delete(Guid Id)
        {
            _viewModel.TermAndCondition = _viewModel.TermsAndConditions.Find(s => s.Id == Id);

            return View(_viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Delete(TermsAndConditionsViewModel viewModel)
        {
            _viewModel.TermAndCondition.Status = false;
            _termsAndConditionsService.Update(_viewModel.TermAndCondition);
            return RedirectToAction("Index");
        }

        public ActionResult Active(Guid Id)
        {
            _viewModel.TermAndCondition = _viewModel.TermsAndConditions.Find(s => s.Id == Id);

            return View(_viewModel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Active(TermsAndConditionsViewModel viewModel)
        {
            _termsAndConditionsService.Active(_viewModel.TermAndCondition);
            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            TermsAndConditionsViewModel viewmodel = new TermsAndConditionsViewModel();

            try
            {
                viewmodel.TermsAndConditions = _termsAndConditionsService.GetTermsAndConditions();
                _viewModel = viewmodel;
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }

            return View(viewmodel);
        }

        public ActionResult Form()
        {
            TermsAndConditionsViewModel viewmodel = new TermsAndConditionsViewModel();
            try
            {
                viewmodel.Brands = new DataServices.BrandDataServices().GetAll();
            }
            catch (Exception err)
            {
                new ErrorHandlerController().redirectToErrorPage(err.Message);
            }
            finally
            {
            }

            return View(viewmodel);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Add(TermsAndConditionsViewModel viewmodel)
        {
            try
            {
                viewmodel.TermAndCondition.FilePT = BlobConnector.UploadFile(viewmodel.TermAndCondition.PDFPT, "terms");

                if (string.IsNullOrEmpty(viewmodel.TermAndCondition.FilePT))
                {
                    throw new Exception("Erro ao fazer o upload do pdf em português!");
                }
                viewmodel.TermAndCondition.FileEN = BlobConnector.UploadFile(viewmodel.TermAndCondition.PDFEN, "terms");

                if (string.IsNullOrEmpty(viewmodel.TermAndCondition.FileEN))
                {
                    throw new Exception("Erro ao fazer o upload do pdf em inglês!");
                }
                viewmodel.TermAndCondition.FileES = BlobConnector.UploadFile(viewmodel.TermAndCondition.PDFES, "terms");

                if (string.IsNullOrEmpty(viewmodel.TermAndCondition.FileES))
                {
                    throw new Exception("Erro ao fazer o upload do pdf em espanhol!");
                }
                viewmodel.TermAndCondition.Register = DateTime.Now;
                _termsAndConditionsService.Insert(viewmodel.TermAndCondition);
                return RedirectToAction("Index");
            }
            catch (Exception err)
            {
                new NLogExceptionLogger().Log("TermsAndConditions - Add" + err.Message, Model.LogType.Error);
                return View(viewmodel);
            }
            finally
            {
            }
        }
    }
}