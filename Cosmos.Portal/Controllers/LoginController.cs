﻿using Cosmos.DataServices;
using Cosmos.Portal.Helpers;
using System;
using System.Net;
using System.Web.Mvc;

namespace Cosmos.Portal.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UserValidate(FormCollection fields_values)
        {
            var userID = fields_values["UserID"];
            var userPassword = fields_values["userPassword"];

            var passwordHash = Utils.HashUtil.GetSha256FromString(userPassword);
            userPassword = passwordHash;

            var user_srv = new UserDataServices();
            try
            {
                if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(userPassword))
                    throw new Exception("Campos inválidos. Verifique.");

                var user = user_srv.UserValidate(userID, userPassword);

                if (user == null)
                    throw new Exception("Acesso negado.");

                //considerando que o objeto user retornou certo, coloco ele na sessão de uso
                SessionContext.UserData = user;

                return Json(new { result = "Redirect", url = Url.Action("Index", "Home") });
            }
            catch (Exception err)
            {
                return Json(new { result = "Fail", msg = err.Message });
            }
            finally
            {
                user_srv = null;
            }
        }

        public ActionResult LogOut()
        {
            SessionContext.UserData = null;
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}