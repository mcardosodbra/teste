﻿using System.Collections.Generic;
using Cosmos.Cache;
using Cosmos.DataServices;
using Cosmos.DataServices.Interfaces;
using Cosmos.Services.Interfaces;

namespace Cosmos.Services.Class
{
    public class ApplicationUserActionService : IApplicationUserActionService
    {
        private readonly AppUserActionDataServices objData = new AppUserActionDataServices();
        private readonly IApplicationUserAction _applicationUserAction;
        private readonly string CacheKeyName = "ListApp";

        public ApplicationUserActionService(IApplicationUserAction applicationUserAction)
        {
            _applicationUserAction = applicationUserAction;
        }

        public bool Add(Model.ApplicationUserAction appUser)
        {
            var ret = _applicationUserAction.Add(appUser);
            Caching.Invalidate(CacheKeyName);
            return ret;
        }

        public bool Update(Model.ApplicationUserAction appUser)
        {
            var ret = objData.Update(appUser);
            Caching.Invalidate(CacheKeyName);
            return ret;
        }

        public bool Delete(int id)
        {
            return objData.Delete(id);
        }

        public List<Model.ApplicationUserAction> GetAll()
        {
            return objData.GetAll();
        }

        public int GetInstalledApps(string appId, string appVersion)
        {
            return objData.GetInstalledApps(appId, appVersion);
        }

        public int GetDownloadApps(string appId, string appVersion)
        {
            return objData.GetDownloadApps(appId, appVersion);
        }

        public int GetInstalledActive(string appId, string appVersion)
        {
            return objData.GetInstalledActive(appId, appVersion);
        }
    }
}