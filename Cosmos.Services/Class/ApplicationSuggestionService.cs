﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class ApplicationSuggestionService : IApplicationSuggestionService
    {
        private readonly IApplicationSuggestionDataServices _applicationSuggestionDataServices;

        public ApplicationSuggestionService(IApplicationSuggestionDataServices applicationSuggestionDataServices)
        {
            _applicationSuggestionDataServices = applicationSuggestionDataServices;
        }

        public List<ApplicationSuggestion> SelectApplicationSuggestion()
        {
            return _applicationSuggestionDataServices.SelectApplicationSuggestion();
        }

        public void InsertApplicationSuggestion(ApplicationSuggestion applicationsuggestion)
        {
            _applicationSuggestionDataServices.InsertApplicationSuggestion(applicationsuggestion);
        }
    }
}