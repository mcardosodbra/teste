﻿using Cosmos.Cache;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.Services.Class
{
    public class SOService : ISOService
    {
        private readonly ISODataServices _ISODataServices;
        private const string CacheKeyName = "ListSO";

        public SOService(ISODataServices ISODataServices)
        {
            _ISODataServices = ISODataServices;
        }

        public List<SO> GetListSO()
        {
            return Caching.GetObjectFromCache(CacheKeyName, 60, GetData);
        }

        private List<SO> GetData()
        {
            return _ISODataServices.GetListSO();
        }

        public Guid InsertSO(SO so)
        {
            if (CheckExistingSO(so))
                throw new Exception("Duplicated SO");

            var soId = _ISODataServices.InsertSO(so);
            Caching.Invalidate(CacheKeyName);

            return soId;
        }

        public void UpdateSO(SO so)
        {
            _ISODataServices.UpdateSO(so);
            Caching.Invalidate(CacheKeyName);
        }

        public void DeleteSO(SO so)
        {
            _ISODataServices.DeleteSO(so);
            Caching.Invalidate(CacheKeyName);
        }

        public SO GetSODetail(Guid SOId)
        {
            return _ISODataServices.GetSODetail(SOId);
        }

        private bool CheckExistingSO(SO so)
        {
            var soList = GetListSO();
            return soList.Any(x => x.Version == so.Version);
        }
    }
}