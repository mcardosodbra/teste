﻿using Cosmos.Cache;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class CityService : ICityService
    {
        private readonly ICityDataServices _cityDataServices;
        private const string CacheKeyName = "ListCity";

        public CityService(ICityDataServices cityDataServices)
        {
            _cityDataServices = cityDataServices;
        }

        public List<City> SelectCity()
        {
            return Caching.GetObjectFromCache(CacheKeyName, 60, GetData);
        }

        public List<City> GetData() => _cityDataServices.SelectCity();

        public List<City> SelectCity(Guid[] stateId)
        {
            return _cityDataServices.SelectCity(stateId);
        }
    }
}