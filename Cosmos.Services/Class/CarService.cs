﻿using Cosmos.Cache;
using Cosmos.DataServices;
using Cosmos.DataServices.Interfaces;
using Cosmos.GoogleAPI;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using Cosmos.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cosmos.Services.Class
{
    public class CarService : ICarService
    {
        private readonly IOpenStreetMap _openStreetMap;
        private readonly ICar _car;
        private readonly ICityService _cityService;
        private readonly IStateService _stateService;
        private readonly CarDataServices objApp = new CarDataServices();
        private const string CacheKeyName = "ListCars";

        public CarService(IOpenStreetMap openStreetMap, ICar car, IStateService stateService, ICityService cityService)
        {
            _openStreetMap = openStreetMap;
            _car = car;
            _stateService = stateService;
            _cityService = cityService;
        }

        public bool AddCar(Car Car)
        {
            Caching.Invalidate(CacheKeyName);
            return objApp.AddCar(Car);
        }

        public bool UpdateCar(Car Car)
        {
            Caching.Invalidate(CacheKeyName);
            return objApp.UpdateCar(Car);
        }

        public Car GetCarsById(string id)
        {
            return objApp.GetCarsById(id);
        }

        public List<Car> GetAllCars()
        {
            return Caching.GetObjectFromCache(CacheKeyName, 60, GetData);
        }

        private List<Car> GetData()
        {
            return objApp.GetAllCars();
        }

        public List<Car> GetAllCarsByYUser(string userId)
        {
            return objApp.GetAllCarsByYUser(userId);
        }

        public Car GetCarByToken(string token)
        {
            return _car.GetCarsByToken(token);
        }

        public bool Delete(string id)
        {
            Caching.Invalidate(CacheKeyName);
            return objApp.Delete(id);
        }

        public bool TokenValid(string token)
        {
            return objApp.TokenValid(token);
        }

        public bool TokenLogout(string token)
        {
            return objApp.Logout(token);
        }

        public async Task UpdateLocation(string token, List<CarLocation> carLocations)
        {
            token = token.Replace("Bearer ", "");
            var car = GetCarByToken(token);
            var carId = car.Id;

            var loc = carLocations.OrderByDescending(x => x.DtRegister).FirstOrDefault();

            // Última Atualização do location capturando da API de Geo
            if (car.DtLastLocationUpdate != null)
            {
                DateTime lastLocationUpdate = (DateTime)car.DtLastLocationUpdate;

                if ((DateTime.UtcNow - lastLocationUpdate).TotalHours > 2)
                {
                    var location = await _openStreetMap.GetLocation(loc.Latitude, loc.Longitude);
                    await _car.UpdateCurrentCarLocation(new Guid(carId), location);
                }
            }
            else
            {
                var location = await _openStreetMap.GetLocation(loc.Latitude, loc.Longitude);
                await _car.UpdateCurrentCarLocation(new Guid(carId), location);
            }

            await _car.UpdateLocation(new Guid(carId), carLocations);
        }

        public GeoCoordinate GetCarLastCoordinate(Guid carId)
        {
            return _car.GetCarLastCoordinate(carId);
        }

        public List<CarLocation> GetLocationList()
        {
            return _car.GetLocationList();
        }

        public CarCurrentLocation GetCarCurrentLocation(Guid carId)
        {
            return _car.GetCarCurrentLocation(carId);
        }
    }
}