﻿using Cosmos.Cache;
using Cosmos.DataServices;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class CategoryService : ICategoryService
    {
        private readonly CategoryDataServices objCat = new CategoryDataServices();
        private readonly ICategory _categoryDataService;
        private const string CacheKeyName = "ListCategory";

        public CategoryService(ICategory category)
        {
            _categoryDataService = category;
        }

        public Category GetCategorybyId(string id)
        {
            return objCat.GetCategoryById(id);
        }

        public List<Category> GetAll()
        {
            return Caching.GetObjectFromCache(CacheKeyName, 60, GetData);
        }

        private List<Category> GetData()
        {
            return objCat.GetAllCategoryForApi();
        }

        public bool Update(Category category)
        {
            Caching.Invalidate(CacheKeyName);
            return objCat.UpdateCategory(category);
        }

        public bool Add(Category category)
        {
            Caching.Invalidate(CacheKeyName);
            return objCat.AddCategory(category);
        }

        public bool ValidateName(string name)
        {
            return objCat.IsCategoryNameInUse(name);
        }
    }
}