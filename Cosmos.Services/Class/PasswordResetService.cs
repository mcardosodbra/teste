﻿using Cosmos.DataServices;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System;

namespace Cosmos.Services.Class
{
    public class PasswordResetService : IPasswordReset
    {
        private readonly EmailConfigDataServices _emailConfig = new EmailConfigDataServices();
        private readonly PasswordResetDataServices _passwordReset = new PasswordResetDataServices();
        private readonly UserDataServices _userServices = new UserDataServices();

        public bool SendPasswordRequisition(string email)
        {
            var user = _userServices.GetUserByEmail(email);
            if (user == null)
                throw new Exception("Email não cadastrado.");
            var existToken = _passwordReset.GetByTokenByUserId(user.Id);
            if (existToken != null)
            {
                existToken.Status = false;
                _passwordReset.UpdateTokenStatus(existToken);
            }
            var token = new PasswordReset();
            token.UserId = user.Id;
            token.Token = Utils.JwtManager.GenerateToken(user.Email, "User", 20);
            token.DtRegister = DateTime.UtcNow;
            token.Status = true;
            if (InsertToken(token))
            {
                var Email = _emailConfig.GetConfigs();
                Email.AddressTo = email;
                var link = "http://cosmos-hom.disruptiv.tech/passwordreset?t=" + token.Token;
                Email.Subject = "Link para redefinição de senha do seu Multi-app";
                Email.Message = "Olá, " + user.Name + "<br /> CADASTRE SUA NOVA SENHA DE ACESSO" +
                                "<br /><br /> Você solicitou uma nova senha de acesso em " + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + "  para acessar o Multi-App Nissan. <a href=\"" + link + "\">Clique aqui</a> para cadastrá-la ou copie e cole o seguinte endereço no seu navegador: " +
                                "<br />" +
                                "<br /><a href=\"" + link + "\"> " + link + " </a> " +
                                "<br /><br />Recomendamos que você cadastre a nova senha assim que receber esse e - mail." +
                                "<br /> A Senha de Acesso é usada para acessar sua conta no Multi-App Nissan e ter acesso a todas as funcionalidades disponíveis para a sua central multimída." +
                                "<br /> Se você não solicitou uma nova Senha de Acesso, ignore esta mensagem." +
                                "<br /> Equipe Multi-App." +
                                "<br /> Este é um e-mail automático, por favor não responda.Em caso de dúvidas, por favor, entre em contato conosco através do e-mail <a href=\"multi-app@disruptiv.tech\"> multi-app@disruptiv.tech</a>";

                Utils.SendEmail.Send(Email);
            }
            else
            {
                throw new Exception("Erro ao criar o token.");
            }

            return true;
        }

        public bool InsertToken(PasswordReset obj)
        {
            return _passwordReset.InsertToken(obj);
        }

        public PasswordReset GetToken(PasswordReset obj)
        {
            return _passwordReset.GetByToken(obj.Token);
        }

        public bool UpdateToken(PasswordReset obj)
        {
            return _passwordReset.UpdateTokenStatus(obj);
        }
    }
}