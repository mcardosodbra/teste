﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class TermsAndConditionsService : ITermsAndConditionsService
    {
        private readonly ITermsAndConditionsDataServices _termsAndConditionsDataService;

        public TermsAndConditionsService(ITermsAndConditionsDataServices termsAndConditionsDataServices)
        {
            _termsAndConditionsDataService = termsAndConditionsDataServices;
        }

        public void Active(TermsAndConditions termsAndConditions)
        {
            _termsAndConditionsDataService.Active(termsAndConditions);
        }

        public List<TermsAndConditions> GetTermsAndConditions()
        {
            return _termsAndConditionsDataService.GetTermsAndConditions();
        }

        public TermsAndConditions GetTermsAndConditions(TermsAndConditions terms)
        {
            return _termsAndConditionsDataService.GetTermsAndConditions(terms);
        }

        public void Insert(TermsAndConditions termsAndConditions)
        {
            _termsAndConditionsDataService.Insert(termsAndConditions);
        }

        public void Update(TermsAndConditions termsAndConditions)
        {
            _termsAndConditionsDataService.Update(termsAndConditions);
        }
    }
}