﻿using Cosmos.Cache;
using Cosmos.DataServices;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.Services.Class
{
    public class AppService : IAppService
    {
        private readonly IAppStoreDataService _appService;
        private readonly AppStoreDataServices objApp = new AppStoreDataServices();
        private readonly string CacheKeyName = "ListApp";
        private const string CategoryCacheKeyName = "ListCategory";

        public AppService(IAppStoreDataService appService)
        {
            _appService = appService;
        }

        public bool Add(Application app)
        {
            Caching.Invalidate(CategoryCacheKeyName);
            Caching.Invalidate(CacheKeyName);

            // validar se existe a versao, senao adiciona

            // validar se existe o packagename, senao retorna false
            return objApp.Add(app);
        }

        public List<Application> GetAllByLanguage(Language language)
        {
            return _appService.GetAllByLanguage(language);
        }

        public List<Application> GetAll()
        {
            return Caching.GetObjectFromCache(CacheKeyName, 60, GetDataAll);
        }

        public List<Application> GetDataAll() => _appService.GetAll();

        public List<Application> GetAllPortal()
        {
            return _appService.GetAllPortal();
        }

        public List<Application> GetByCategory(string categoryId)
        {
            return objApp.GetByCategoryApi(categoryId);
        }

        public Application GetById(string id)
        {
            return GetAll().Single(x => x.Id == id);
            // return _appService.GetApplicationById(id);
        }

        public Application GetApplicationByIdPortal(string id)
        {
            return _appService.GetApplicationByIdPortal(id);
        }

        public List<Application> SearchByCategory(string CategoryDescription)
        {
            return objApp.SearchByCategory(CategoryDescription);
        }

        public bool DownloadUpdate(string id)
        {
            Caching.Invalidate(CacheKeyName);

            var ret = objApp.GetApplicationById(id);

            return objApp.DownloadUpdate(ret);
        }

        public bool Update(Application App)
        {
            Caching.Invalidate(CategoryCacheKeyName);
            Caching.Invalidate(CacheKeyName);
            return objApp.Update(App);
        }

        public List<Application> GetApplicationsActiveToCentral(string tokenId)
        {
            return _appService.GetApplicationsActiveToCentral(tokenId);
        }

        public List<Application> GetApplicationsToUpdateVersion(string token)
        {
            return _appService.GetApplicationsToUpdateVersion(token);
        }

        public List<Application> GetApplicationsToRemove(string token)
        {
            return _appService.GetApplicationsToRemove(token);
        }

        public List<Application> GetMultiAppToUpdateVersion(string token)
        {
            return _appService.GetMultiAppToUpdateVersion(token);
        }

        public Model.Application GetApplicationByPackageName(string packageName)
        {
            return _appService.GetApplicationByPackageName(packageName);
        }
    }
}