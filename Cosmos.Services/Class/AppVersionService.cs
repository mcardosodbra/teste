﻿using System;
using Cosmos.Cache;
using Cosmos.DataServices;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;
using Cosmos.DataServices.Interfaces;

namespace Cosmos.Services.Class
{
    public class AppVersionService : IAppVersionService
    {
        private readonly AppVersionDataServices objData = new AppVersionDataServices();
        private readonly string CacheKeyName = "ListApp";
        private readonly IAppVersionDataServices _appVersionDataServices;

        public AppVersionService(IAppVersionDataServices appVersionDataServices)
        {
            _appVersionDataServices = appVersionDataServices;
        }

        public bool Update(AppVersion appVersion)
        {
            Caching.Invalidate(CacheKeyName);
            return objData.Update(appVersion);
        }

        public List<AppVersion> GetVersionsByApplicationId(string id)
        {
            return objData.GetVersionsByApplicationId(id);
        }

        public AppVersion GetAppVersionById(string id)
        {
            return objData.GetAppVersionById(id);
        }

        public bool Delete(string id)
        {
            Caching.Invalidate(CacheKeyName);
            return objData.Delete(id);
        }

        public bool Active(string id)
        {
            Caching.Invalidate(CacheKeyName);
            return objData.Active(id);
        }

        public bool IsVersionInUseForApp(string version, string applicationId)
        {
            return objData.IsVersionInUseForApp(version, applicationId);
        }

        public void InsertVersion(Guid versionId, Guid applicationId, int VersionCode, string packageName)
        {
            _appVersionDataServices.InsertVersion(versionId, applicationId, VersionCode, packageName);
        }
    }
}