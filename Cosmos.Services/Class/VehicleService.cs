﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicle _iVehicleService;

        public VehicleService(IVehicle vehicle)
        {
            _iVehicleService = vehicle;
        }

        public List<Vehicle> GetVehicleAll()
        {
            return _iVehicleService.GetVehicleAll();
        }

        public List<Vehicle> GetVehicleByBrand(Guid brandId)
        {
            return _iVehicleService.GetVehicleByBrand(brandId);
        }
    }
}