﻿using Cosmos.Cache;
using Cosmos.DataServices;
using Cosmos.DataServices.Interfaces;
using Cosmos.Log;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.Services.Class
{
    public class UserService : IUserService
    {
        private readonly UserDataServices objUsr = new UserDataServices();
        private readonly CarDataServices objCar = new CarDataServices();
        private readonly ISOService _SOService;
        private const string CarCacheKeyName = "ListCars";
        private readonly IUser _user;

        public UserService(ISOService SOService, IUser user)
        {
            _SOService = SOService;
            _user = user;
        }

        public void AddNew(User user)
        {
            try
            {
                var passwordHash = Utils.HashUtil.GetSha256FromString(user.Password);
                user.Password = passwordHash;
                user.InclusionDate = DateTime.Parse($"{DateTime.Now:yyyy/MM/dd HH:mm:ss}"); ;
                objUsr.AddUser(user);
            }
            catch (Exception)
            {
                throw new Exception("Usuário não pode ser cadastrado.");
            }
        }

        public List<User> GetAll()
        {
            try
            {
                var ret = objUsr.GetUserAll();
                foreach (var item in ret)
                {
                    item.Password = "";
                }
                return ret;
            }
            catch (Exception)
            {
                throw new Exception("Erro ao retornar usuários.");
            }
        }

        public User GetToken(User user)
        {
            // new NLogExceptionLogger().Log(JsonConvert.SerializeObject(user).ToString(), LogType.Info);
            if (string.IsNullOrEmpty(user.sSO))
            {
                throw new Exception("Invalid SO");
            }

            try
            {
                var passwordHash = Utils.HashUtil.GetSha256FromString(user.Password);
                user.Password = passwordHash;

                if (string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Password))
                    throw new Exception("Campos inválidos. Verifique.");
                var usr = objUsr.UserValidate(user.Email, user.Password);
                if (usr == null)
                    return null;
                usr.Password = "";
                usr.Token = Cosmos.Utils.JwtManager.GenerateToken(user.Name, "User", 20);
                usr.Brand.Logotype = "";

                // Limpa o cache dos carros
                Caching.Invalidate(CarCacheKeyName);

                if (usr.TypeId == "3")
                {
                    var soObj = GetSoByName(user.sSO);
                    user.SO = soObj;

                    if (user.SO == null)
                    {
                        var SO = new SO { Version = user.sSO };

                        SO.Id = _SOService.InsertSO(SO);
                        user.SO = SO;
                    }

                    var cars = objCar.GetAllCarsByYUser(usr.Id);
                    if (cars.Any())
                    {
                        var haveCar = cars.FirstOrDefault(s => s.UUID == user.UUID);
                        if (haveCar == null)
                        {
                            var car = new Car
                            {
                                UUID = user.UUID,
                                CAN = user.CAN,
                                LicensePlate = user.LicensePlate,
                                MCU = user.MCU,
                                SO = user.SO,
                                Status = true,
                                VIN = user.VIN,
                                UserId = usr.Id,
                                Token = usr.Token,
                                SOID = user.SO.Id
                            };
                            objCar.AddCar(car);

                            usr.LicensePlate = user.LicensePlate;
                            usr.VIN = user.VIN;
                        }
                        else
                        {
                            haveCar.Token = usr.Token;
                            haveCar.Status = true;
                            objCar.UpdateCar(haveCar);
                        }

                        if (haveCar != null)
                        {
                            usr.LicensePlate = haveCar.LicensePlate;
                            usr.VIN = haveCar.VIN;
                        }
                    }
                    else
                    {
                        var car = new Car
                        {
                            UUID = user.UUID,
                            CAN = user.CAN,
                            LicensePlate = user.LicensePlate,
                            MCU = user.MCU,
                            SO = user.SO,
                            Status = true,
                            VIN = user.VIN,
                            UserId = usr.Id,
                            Token = usr.Token,
                            SOID = user.SO.Id
                        };

                        objCar.AddCar(car);
                        usr.LicensePlate = car.LicensePlate;
                        usr.VIN = car.VIN;
                    }
                }

                return usr;
            }
            catch (Exception err)
            {
                new NLogExceptionLogger().Log("UserService Error " + err.Message + " - " + JsonConvert.SerializeObject(user).ToString(), LogType.Error);
                return null;
            }
        }

        private bool CheckExistingSO(SO so)
        {
            // retorna lista do cache
            var soList = _SOService.GetListSO();
            return soList.Any(x => x.Id == so.Id);
        }

        private SO GetSoByName(string so)
        {
            return _SOService.GetListSO().SingleOrDefault(x => x.Version == so);
        }

        public bool IsLicensePlateInUse(string Licenseplate)
        {
            try
            {
                return objUsr.IsLicensePlateInUse(Licenseplate);
            }
            catch (Exception)
            {
                throw new Exception("Erro ao retornar a placa do carro.");
            }
        }

        public Guid GetUserIdByToken(string token)
        {
            return objCar.GetUserIdByToken(token);
        }

        public bool ValidateUser(User user)
        {
            var usr = objUsr.UserValidate(user.Email, Utils.HashUtil.GetSha256FromString(user.Password));
            return usr != null;
        }

        public List<Permission> GetPermissionList()
        {
            return _user.GetPermissionList();
        }

        public List<Permission> GetPermissionListByUser(Guid userId)
        {
            return _user.GetPermissionListByUser(userId);
        }

        public Guid GetBrandIdByToken(string token)
        {
            return objCar.GetBrandIdbyToken(token);
        }

        public bool IsVinInUse(string Vin)
        {
            try
            {
                return objUsr.IsVinInUse(Vin);
            }
            catch (Exception)
            {
                throw new Exception("Erro ao retornar o VIN.");
            }
        }

        public void Update(User user)
        {
            try
            {
                var car = objCar.GetCarsById(user.UUID, user.Id);
                car.CAN = user.CAN;
                // car.SO = user.SO;
                car.VIN = user.VIN;
                car.LicensePlate = user.LicensePlate;
                car.MCU = user.MCU;
                objCar.UpdateCar(car);
                objUsr.UpdateUser(user);
            }
            catch (Exception err)
            {
                throw new Exception("Usuário não pode ser atualizado - " + err.Message);
            }
        }

        public bool ValidateEmail(string email)
        {
            try
            {
                return objUsr.IsEmailInUse(email);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}