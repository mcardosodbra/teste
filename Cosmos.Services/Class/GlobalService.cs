﻿using Cosmos.Cache;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class GlobalService : IGlobalService
    {
        private readonly IGlobalDataServices _globalDataServices;
        private const string CacheKeyName = "LanguageList";

        public GlobalService(IGlobalDataServices globalDataServices)
        {
            _globalDataServices = globalDataServices;
        }

        public List<Language> LanguageList()
        {
            var lstLanguage = Caching.GetObjectFromCache<List<Language>>(CacheKeyName, 60, GetData);

            return lstLanguage;
        }

        private List<Language> GetData()
        {
            return _globalDataServices.LanguageList();
        }

        public List<Permission> PermissionList()
        {
            return _globalDataServices.PermissionList();
        }

        public void LogtoDb(string message, string endpointcall, LogType logType)
        {
            _globalDataServices.LogtoDb(message, endpointcall, logType);
        }
    }
}