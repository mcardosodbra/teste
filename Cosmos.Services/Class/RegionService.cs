﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System.Collections.Generic;

namespace Cosmos.Services.Class
{
    public class RegionService : IRegionService
    {
        private readonly IRegionDataServices _regionDataServices;

        public RegionService(IRegionDataServices regionDataServices)
        {
            _regionDataServices = regionDataServices;
        }

        public List<Region> SelectRegion()
        {
            return _regionDataServices.SelectRegion();
        }

        public void InsertRegion(Region region)
        {
            _regionDataServices.InsertRegion(region);
        }

        public void UpdateRegion(Region region)
        {
            _regionDataServices.UpdateRegion(region);
        }

        public void DeleteRegion(Region region)
        {
            _regionDataServices.DeleteRegion(region);
        }
    }
}