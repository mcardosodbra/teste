﻿using Cosmos.DataServices;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using Cosmos.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cosmos.Services.Class
{
    public class ApproverService : IApproverService
    {
        private readonly IApproverDataServices _approverDataServices;

        public ApproverService(IApproverDataServices approverDataServices)
        {
            _approverDataServices = approverDataServices;
        }

        public List<Approver> SelectApprover(bool approved, Guid brandId)
        {
            return _approverDataServices.SelectApprover(brandId, approved);
        }

        public List<Approver> SelectApprover(bool approved, Guid brandId, ApproverType approverType)
        {
            return _approverDataServices.SelectApprover(brandId, approved, approverType);
        }

        public List<Approver> SelectApprover(bool approved, Guid brandId, Guid messageId)
        {
            return _approverDataServices.SelectApprover(brandId, approved, messageId);
        }

        public Approver SelectApproverById(Guid approverId)
        {
            return _approverDataServices.SelectApproverById(approverId);
        }

        public void InsertApprover(Approver approver)
        {
            var approverId = _approverDataServices.InsertApprover(approver);
            approver.Id = approverId;
            var approverList = SelectApprover(true, Guid.Parse(UserSession.User.BrandId));

            Task.Run(() =>
            {
                SendApproverEmail(approver);
                SendAllApproverEmail(approverList, approver, 3);
            });
        }

        public void UpdateApprover(Approver approver)
        {
            _approverDataServices.UpdateApprover(approver);
        }

        public void DeleteApprover(Approver approver, string reasonmessage)
        {
            _approverDataServices.DeleteApprover(approver);
            var approverList = SelectApprover(true, new Guid(UserSession.User.BrandId));
            Task.Run(() => { SendAllApproverEmail(approverList, approver, 4); });
        }

        public void ActivateApprover(Guid approver)
        {
            var approverObj = SelectApproverById(approver);
            var approverList = SelectApprover(true, Guid.Parse(approverObj.Brand.Id));

            _approverDataServices.ActivateApprover(approver);
            Task.Run(() => { SendAllApproverEmail(approverList, SelectApproverById(approver), 1); });
        }

        public void DeActivateApprover(Guid approver)
        {
            var approverObj = SelectApproverById(approver);
            var approverList = SelectApprover(true, Guid.Parse(approverObj.Brand.Id));
            _approverDataServices.ActivateApprover(approver);
            Task.Run(() => { SendAllApproverEmail(approverList, SelectApproverById(approver), 2); });
        }

        private void SendApproverEmail(Approver approver)
        {
            string content = GetCssForApproverEmail();

            content += $"<html><body><div class=\'mainDiv\'><div class=\'content\'>" +
                       $"Para ativar o seu cadastro clique na imagem abaixo<br/>" +
                       $" <a href=\'{GlobalVariables.ApiAddress}approver/ActivateApprover?approverId={approver.Id}\'>" +
                       $"<img class=\'imgActivate\' src=\'{GlobalVariables.PortalBrandAddress}Images/check.png\' alt=\'Smiley face\' height=\'64\' width=\'64\'></a>" +
                       $" </div></div></body></html>";

            var objMail = new Email
            {
                AddressFrom = Utils.SendEmail.EmailUser,
                AddressTo = approver.Email,
                Host = Utils.SendEmail.EmailServer,
                Port = int.Parse(Utils.SendEmail.EmailPort),
                Password = Utils.SendEmail.EmailPassword,
                Subject = "Confirmação de Cadastro de Aprovador",
                Message = content
            };

            SendEmail.Send(objMail);
        }

        private string GetCssForApproverEmail()
        {
            string css =
                "<style> .mainDiv {    background: #d8e6f1;    width: 650px;    margin: auto;    padding: 10px;    text-align: center;    -moz-border-radius: 7px;    -webkit-border-radius: 7px;    border-radius: 7px;    width: 800px;    height: 230px;    border: solid;    border-width: 1px;    border-color: darkcyan;    display: table;    vertical-align: middle;    font-family: calibri;    font-size: 26px;}.content {    margin-top: 50px;}.imgActivate {    margin-top: 30px;}</style>";
            return css;
        }

        private string GetCssForApprovedApproverEmail()
        {
            string css =
                "<style> .mainDiv {    background: #d8e6f1;    width: 650px;    margin: auto;    padding: 10px;    text-align: center;    -moz-border-radius: 7px;    -webkit-border-radius: 7px;    border-radius: 7px;    width: 800px;    height: 230px;    border: solid;    border-width: 1px;    border-color: darkcyan;    display: table;    vertical-align: middle;    font-family: calibri;    font-size: 22px;}.content {    margin-top: 50px;}.imgActivate {    margin-top: 30px;} .contentApproved { text-align:left; } </style>";
            return css;
        }

        private void SendAllApproverEmail(IEnumerable<Approver> approvers, Approver activatedApprover, int reason, string reasonmessage = "")
        {
            string message = string.Empty;
            switch (reason)
            {
                case 1: // ativado
                    message = "ativado";
                    break;

                case 2: // desativado
                    message = "desativado";
                    break; ;

                case 3: // adicionado
                    message = "adicionado";
                    break;

                case 4: // removido
                    message = "removido";
                    break;
            }
            string content = GetCssForApprovedApproverEmail();

            foreach (var approver in approvers)
            {
                if (approver.Id == activatedApprover.Id) continue;
                var objMail = new Email
                {
                    AddressFrom = Utils.SendEmail.EmailUser,
                    AddressTo = approver.Email,
                    Host = Utils.SendEmail.EmailServer,
                    Port = int.Parse(Utils.SendEmail.EmailPort),
                    Password = Utils.SendEmail.EmailPassword,
                    Subject = "Notificação de Registro de Aprovador",

                    Message = content + $"<html><body><div class=\'mainDiv\'><div class=\'content\'>" +
                          $"O Aprovador {activatedApprover.Name} foi {message} com sucesso, dados do aprovador: <div class='contentApproved'><br /> <b>Nome:</b> {activatedApprover.Name}<br /> <b>E-Mail:</b> {activatedApprover.Email}<br /> <b>Cargo:</b> {activatedApprover.Position}</div>" +
                          $" </div></div></body></html>"
                };

                if (reason == 4)
                    objMail.Message = objMail.Message + $"</BR> Motivo: {reasonmessage}";
                Utils.SendEmail.Send(objMail);
            }
        }

        public void MessageforApprovalNotification(Message message)
        {
            var approverList = SelectApprover(true, Guid.Parse(UserSession.User.BrandId), message.Id);
            SendAllApproverEmailNewMessage(approverList, message);
        }

        public List<Approver> SelectApprover(Guid brandId)
        {
            return _approverDataServices.SelectApprover(brandId);
        }

        private void SendAllApproverEmailNewMessage(IEnumerable<Approver> approvers, Message message)
        {
            string destiny = string.Empty;

            message = FillDestinyFields(message);

            if (message.RegionDestiny != null)
                destiny += $" Região: {message.RegionDestiny.Name}";

            if (message.BrandDestiny != null)
                destiny += $" Montadora: {message.BrandDestiny.Name}";

            if (message.UserDestiny != null)
                destiny += $" Usuário: {message.UserDestiny.Name}";

            MessageType msgType = (MessageType)message.MessageTypeId;

            string messageContent = "";
            string banner = "";

            foreach (var text in message.MessageText)
            {
                if (text.Text != string.Empty)
                {
                    messageContent += $"Idioma: {text.Language.Name}<br /> Título: {text.Title}<br /> Conteúdo: {text.Text}<br />";
                }
                if (text.Banner != string.Empty)
                    messageContent += $"<img src='{text.Banner}' height='200' width='200'> <br />";
            }

            foreach (var approver in approvers)
            {
                var objMail = new Email
                {
                    AddressFrom = Utils.SendEmail.EmailUser,
                    AddressTo = approver.Email,
                    Host = Utils.SendEmail.EmailServer,
                    Port = int.Parse(Utils.SendEmail.EmailPort),
                    Password = Utils.SendEmail.EmailPassword,
                    Subject = "Nova mensagem submetida para Aprovação",
                    Message = $@"Uma nova mensagem foi criada e foi submetida para aprovação, informações sobre a mensagem:" +

                              $"<br />Tipo: {msgType}" +
                              $"<br />Conteúdo: {messageContent}" +
                              $"<br />{banner}" +
                              $"<br />Data de Validade: {message.ExpireDate}" +
                              $"<br />Data de agendamento: {message.ScheduledDate}" +
                              $"<br />Destino: {destiny}" +
                              $"<br />Para Aprovar a Mensagem clique aqui: {GlobalVariables.ApiAddress}message/approvemessage?id={message.Id}&approverId={approver.Id}" +
                              $"<br />Para Reprovar a Mensagem clique aqui: {GlobalVariables.ApiAddress}message/reprovemessage?id={message.Id}" +
                              $"<br /><img src=\"https://www.selenity.com/assets/SoftwareEurope/core/Product%20Features/EXP-Easy-to-Approve.png?ext=.png\" alt=\"Smiley face\" height=\"42\" width=\"42\">"
                };
                Utils.SendEmail.Send(objMail);
            }
        }

        private Message FillDestinyFields(Message message)
        {
            //if (message.RegionIdDestiny.Length > 0)
            //{
            //    var regionList = _regionService.SelectRegion();
            //    message.RegionDestiny = region
            //}

            if (message.BrandIdDestiny != Guid.Empty)
            {
                var brandObj = new BrandDataServices();
                var brandList = brandObj.GetAll();
                message.BrandDestiny = brandList.SingleOrDefault(x => Guid.Parse(x.Id) == message.BrandIdDestiny);
            }

            if (message.UserIdDestiny != Guid.Empty)
            {
                var userObj = new UserDataServices();
                var userList = userObj.GetUserAll();
                message.UserDestiny = userList.SingleOrDefault(x => Guid.Parse(x.Id) == message.UserIdDestiny);
            }

            return message;
        }
    }
}