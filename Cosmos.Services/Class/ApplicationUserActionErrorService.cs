﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;

namespace Cosmos.Services.Class
{
    public class ApplicationUserActionErrorService : IApplicationUserActionErrorService
    {
        private readonly IApplicationUserActionErrorDataServices _applicationUserActionErrorDataServices;

        public ApplicationUserActionErrorService(IApplicationUserActionErrorDataServices applicationUserActionErrorDataServices)
        {
            _applicationUserActionErrorDataServices = applicationUserActionErrorDataServices;
        }

        public void insert(ApplicationUserActionError objUser)
        {
            _applicationUserActionErrorDataServices.insert(objUser);
        }
    }
}