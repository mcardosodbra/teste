﻿using Cosmos.Cache;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.Services.Class
{
    public class StateService : IStateService
    {
        private readonly IStateDataServices _stateDataServices;
        private const string CacheKeyName = "ListState";

        public StateService(IStateDataServices stateDataServices)
        {
            _stateDataServices = stateDataServices;
        }

        public List<State> SelectState()
        {
            return Caching.GetObjectFromCache(CacheKeyName, 60, GetData);
        }

        public List<State> GetData() => _stateDataServices.SelectState();

        public void InsertState(State state)
        {
            _stateDataServices.InsertState(state);
        }

        public void UpdateState(State state)
        {
            _stateDataServices.UpdateState(state);
        }

        public void DeleteState(State state)
        {
            _stateDataServices.DeleteState(state);
        }

        public List<State> SelectStateByRegion(Guid regionId)
        {
            return _stateDataServices.SelectState().Where(r => r.Id == regionId).ToList();
            //return _stateDataServices.SelectStateByRegion(regionId);
        }

        public List<State> SelectStateByRegion(Guid[] regionId)
        {
            return _stateDataServices.SelectStateByRegion(regionId);
        }
    }
}