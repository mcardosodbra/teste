﻿using Cosmos.Cache;
using Cosmos.DataServices.Interfaces;
using Cosmos.GoogleAPI;
using Cosmos.Log;
using Cosmos.Model;
using Cosmos.Services.Interfaces;
using Cosmos.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cosmos.Services.Class
{
    public class MessageService : IMessageService
    {
        private readonly IMessageDataService _messageDataService;
        private readonly IUserService _userService;
        private readonly IOpenStreetMap _openStreetMap;
        private readonly ICarService _carService;

        private const string MessageTextCacheKey = "ListMessageText";

        public MessageService(IMessageDataService messageDataService, IUserService userService, IOpenStreetMap openStreetMap, ICarService carService)
        {
            _messageDataService = messageDataService;
            _userService = userService;
            _openStreetMap = openStreetMap;
            _carService = carService;
        }

        public List<Message> GetMessageList()
        {
            return _messageDataService.GetMessageList();
        }

        public Message GetMessageById(Guid id)
        {
            return _messageDataService.GetMessageById(id);
        }

        public List<MessageText> GetMessageTextList()
        {
            return Caching.GetObjectFromCache(MessageTextCacheKey, 60, GetDataMessageText);
        }

        private List<MessageText> GetDataMessageText()
        {
            return _messageDataService.GetMessageTextList();
        }

        public void ApproveMessage(Guid id, Guid approverId)
        {
            var status = _messageDataService.ApproveMessage(id, approverId);

            if (status == 1) // Mensagem 100% aprovada, enviar e-mail de publicação
            {
                var messageDetail = GetMessageDetail(id).SingleOrDefault();
                SendEmailAuthorPublished(messageDetail);
            }
        }

        private async Task SendEmailAuthorPublished(Message message)
        {
            var objMail = new Email
            {
                AddressFrom = SendEmail.EmailUser,
                AddressTo = message.Creator.Email,
                Host = SendEmail.EmailServer,
                Port = int.Parse(SendEmail.EmailPort),
                Password = SendEmail.EmailPassword,
                Subject = "Comunicação de Mensagem Publicada",
                Message = $"Existe uma nova mensagem aguardando publicação, acesse o painel e efetue a publicação da mensagem."
            };

            await SendEmail.Send(objMail);
        }

        public List<Message> GetMessageDetail(Guid messageId)
        {
            return _messageDataService.GetMessageDetail(messageId);
        }

        public void ReproveMessage(Guid id, Guid approverId, string reason)
        {
            _messageDataService.ReproveMessage(id, approverId, reason);
        }

        public async Task<List<Message>> GetMessage(string token, Language language)
        {
            token = token.Replace("Bearer ", "");
            var car = _carService.GetAllCars().Single(t => t.Token == token);

            var currentCarLocation = _carService.GetCarCurrentLocation(Guid.Parse(car.Id));

            if (currentCarLocation != null)
                return _messageDataService.GetMessage(token, currentCarLocation.Region.Name, currentCarLocation.State.Name, currentCarLocation.City.Name, new Guid(car.User.BrandId), language);
            else
                return null;
        }

        public void InsertMessage(Message message)
        {
            try
            {
                Caching.Invalidate(MessageTextCacheKey);
                _messageDataService.InsertMessage(message);
            }
            catch (Exception e)
            {
                new NLogExceptionLogger().Log(e.Message, LogType.Error);
            }
        }

        public void UpdateMessage(Message message)
        {
            Caching.Invalidate(MessageTextCacheKey);
            _messageDataService.UpdateMessage(message);
        }

        public void DeleteMessage(Message message)
        {
            Caching.Invalidate(MessageTextCacheKey);
            _messageDataService.DeleteMessage(message);
        }

        public Task MessageRead(string token, Guid messageid)
        {
            token = token.Replace("Bearer ", "");

            var userId = _userService.GetUserIdByToken(token);
            return _messageDataService.MessageRead(userId, messageid, token);
        }

        public void ChangeStatus(Guid id, int status)
        {
            var message = GetMessageById(id);
            switch (status)
            {
                case 2: // Approved
                    SendEmailAuthorApproved(message);
                    break;

                case 3: // Reproved
                    SendEmailAuthorReproved(message);
                    break;
            }

            _messageDataService.ChangeStatus(id, status);
        }

        private void SendEmailAuthorReproved(Message message)
        {
            var objMail = new Email
            {
                AddressFrom = SendEmail.EmailUser,
                AddressTo = message.Creator.Email,
                Host = SendEmail.EmailServer,
                Port = int.Parse(SendEmail.EmailPort),
                Password = SendEmail.EmailPassword,
                Subject = "Comunicação de Mensagem Reproved",
                Message = //$"A mensagem {message.Title} foi reprovada, o motivo foi: {message.ReprovedReason}" +
                          $"<br />Enviar Mensagem: {GlobalVariables.ApiAddress}message/sendmessage?id={message.Id}" +
                          $"<br />Agendar Envio {message.ScheduledDate}: {GlobalVariables.ApiAddress}message/schedulesend?id={message.Id}"
            };
            SendEmail.Send(objMail);
        }

        private void SendEmailAuthorApproved(Message message)
        {
            var objMail = new Email
            {
                AddressFrom = SendEmail.EmailUser,
                AddressTo = message.Creator.Email,
                Host = SendEmail.EmailServer,
                Port = int.Parse(SendEmail.EmailPort),
                Password = SendEmail.EmailPassword,
                Subject = "Comunicação de Mensagem Approved",
                Message = //$"A mensagem {message.Title} foi aprovada." +
                          $"<br />Editar: {GlobalVariables.PortalAddress}message/Edit" +
                          $"<br />Reenviar para Aprovação: {GlobalVariables.ApiAddress}message/sendtoapproval?id={message.Id}" +
                          $"<br />Cancelar: {GlobalVariables.ApiAddress}message/cancelmessage?id={message.Id}"
            };
            SendEmail.Send(objMail);
        }
    }
}