﻿using System;
using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface ICityService
    {
        List<City> SelectCity();

        List<City> SelectCity(Guid[] stateId);
    }
}