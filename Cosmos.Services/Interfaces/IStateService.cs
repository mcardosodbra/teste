﻿using System;
using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface IStateService
    {
        List<State> SelectState();

        void InsertState(State state);

        void UpdateState(State state);

        void DeleteState(State state);

        List<State> SelectStateByRegion(Guid regionId);

        List<State> SelectStateByRegion(Guid[] regionId);
    }
}