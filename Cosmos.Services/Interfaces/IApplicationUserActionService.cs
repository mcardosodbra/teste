﻿using System.Collections.Generic;
using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface IApplicationUserActionService
    {
        bool Add(ApplicationUserAction appUser);
        bool Update(ApplicationUserAction appUser);
        bool Delete(int id);
        List<ApplicationUserAction> GetAll();
        int GetInstalledApps(string appId, string appVersion);
        int GetDownloadApps(string appId, string appVersion);
        int GetInstalledActive(string appId, string appVersion);
    }
}
