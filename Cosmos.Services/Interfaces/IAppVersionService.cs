﻿using System;
using System.Collections.Generic;
using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface IAppVersionService
    {
        bool Update(AppVersion appVersion);

        List<AppVersion> GetVersionsByApplicationId(string id);

        AppVersion GetAppVersionById(string id);

        bool Delete(string id);

        bool Active(string id);

        bool IsVersionInUseForApp(string version, string app);

        void InsertVersion(Guid versionId, Guid applicationId, int VersionCode, string packageName);
    }
}