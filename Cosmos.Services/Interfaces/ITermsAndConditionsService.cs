﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface ITermsAndConditionsService
    {
        void Insert(TermsAndConditions termsAndConditions);

        void Update(TermsAndConditions termsAndConditions);

        void Active(TermsAndConditions termsAndConditions);

        List<TermsAndConditions> GetTermsAndConditions();

        TermsAndConditions GetTermsAndConditions(TermsAndConditions terms);
    }
}