﻿using System.Collections.Generic;
using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface IRegionService
    {
        List<Region> SelectRegion();

        void InsertRegion(Region region);

        void UpdateRegion(Region region);

        void DeleteRegion(Region region);
    }
}