﻿using System;
using System.Collections.Generic;
using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface IVehicleService
    {
        List<Vehicle> GetVehicleAll();

        List<Vehicle> GetVehicleByBrand(Guid brandId);
    }
}