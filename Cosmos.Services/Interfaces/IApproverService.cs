﻿using Cosmos.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cosmos.Services.Interfaces
{
    public interface IApproverService
    {
        List<Approver> SelectApprover(bool approved, Guid brandId);

        List<Approver> SelectApprover(bool approved, Guid brandId, ApproverType approverType);

        Approver SelectApproverById(Guid approverId);

        void InsertApprover(Approver approver);

        void UpdateApprover(Approver approver);

        void DeleteApprover(Approver approver, string reasonmessage);

        void ActivateApprover(Guid approver);

        void DeActivateApprover(Guid approver);

        void MessageforApprovalNotification(Message message);

        List<Approver> SelectApprover(Guid brandId);
    }
}