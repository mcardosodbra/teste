﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface IPasswordReset
    {
        bool SendPasswordRequisition(string email);
        bool InsertToken(PasswordReset obj);
        PasswordReset GetToken(PasswordReset obj);
        bool UpdateToken(PasswordReset obj);
    }
}
