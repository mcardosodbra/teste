﻿using Cosmos.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cosmos.Services.Interfaces
{
    public interface IMessageService
    {
        List<Message> GetMessageList();

        Task<List<Message>> GetMessage(string token, Language language);

        void InsertMessage(Message message);

        void UpdateMessage(Message message);

        void DeleteMessage(Message message);

        Task MessageRead(string Token, Guid messageid);

        void ChangeStatus(Guid id, int status);

        Message GetMessageById(Guid id);

        List<MessageText> GetMessageTextList();

        void ApproveMessage(Guid id, Guid approverId);

        List<Message> GetMessageDetail(Guid messageId);

        void ReproveMessage(Guid id, Guid approverId, string reason);
    }
}