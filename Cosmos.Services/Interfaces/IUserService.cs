﻿using Cosmos.Model;
using System;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface IUserService
    {
        User GetToken(User user);

        void AddNew(User user);

        void Update(User user);

        List<User> GetAll();

        bool ValidateEmail(string email);

        bool IsVinInUse(string Vin);

        bool IsLicensePlateInUse(string Licenseplate);

        Guid GetUserIdByToken(string token);

        bool ValidateUser(User user);

        List<Permission> GetPermissionList();

        List<Permission> GetPermissionListByUser(Guid userId);
    }
}