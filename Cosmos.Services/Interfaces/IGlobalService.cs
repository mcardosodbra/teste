﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface IGlobalService
    {
        List<Language> LanguageList();

        List<Permission> PermissionList();

        void LogtoDb(string message, string endpointcall, LogType logType);
    }
}