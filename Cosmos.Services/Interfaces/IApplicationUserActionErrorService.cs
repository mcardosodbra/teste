﻿using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface IApplicationUserActionErrorService
    {
        void insert(ApplicationUserActionError objUser);
    }
}