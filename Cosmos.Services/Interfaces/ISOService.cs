﻿using System;
using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface ISOService
    {
        List<SO> GetListSO();

        Guid InsertSO(SO so);

        void UpdateSO(SO so);

        void DeleteSO(SO so);

        SO GetSODetail(Guid SOId);
    }
}