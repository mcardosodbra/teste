﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cosmos.Model;

namespace Cosmos.Services.Interfaces
{
    public interface ICategoryService
    {
        Category GetCategorybyId(string id);
        List<Category> GetAll();
        bool Update(Category category);
        bool Add(Category category);
        bool ValidateName(string name);
    }
}
