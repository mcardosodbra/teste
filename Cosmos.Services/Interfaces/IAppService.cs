﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface IAppService
    {
        bool Add(Application app);

        Application GetById(string id);

        Application GetApplicationByIdPortal(string id);

        List<Application> GetAll();

        List<Application> GetAllPortal();

        List<Application> GetByCategory(string categoryId);

        List<Application> SearchByCategory(string CategoryDescription);

        bool DownloadUpdate(string id);

        bool Update(Application App);

        List<Application> GetApplicationsActiveToCentral(string tokenId);

        List<Application> GetApplicationsToUpdateVersion(string token);

        List<Application> GetMultiAppToUpdateVersion(string token);

        List<Model.Application> GetApplicationsToRemove(string token);

        Model.Application GetApplicationByPackageName(string packageName);

        List<Application> GetAllByLanguage(Language language);
    }
}