﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cosmos.Model;
using Cosmos.Utils;

namespace Cosmos.Services.Interfaces
{
    public interface ICarService
    {
        bool AddCar(Model.Car Car);

        bool UpdateCar(Model.Car Car);

        Model.Car GetCarsById(string id);

        List<Model.Car> GetAllCars();

        Model.Car GetCarByToken(string token);

        List<Model.Car> GetAllCarsByYUser(string userId);

        bool Delete(string id);

        bool TokenValid(string token);

        bool TokenLogout(string token);

        Task UpdateLocation(string token, List<CarLocation> carLocations);

        GeoCoordinate GetCarLastCoordinate(Guid carId);

        List<CarLocation> GetLocationList();

        CarCurrentLocation GetCarCurrentLocation(Guid carId);
    }
}