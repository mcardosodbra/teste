﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.Services.Interfaces
{
    public interface IApplicationSuggestionService
    {
        List<ApplicationSuggestion> SelectApplicationSuggestion();

        void InsertApplicationSuggestion(ApplicationSuggestion applicationsuggestion);
    }
}