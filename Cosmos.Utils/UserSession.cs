﻿using System.Web;
using Cosmos.Model;

namespace Cosmos.Utils
{
    public static class UserSession
    {
        public static User User
        {
            get
            {
                return (User)HttpContext.Current.Session["User"];
            }
            set
            {
                HttpContext.Current.Session["User"] = value;
            }
        }
    }
}