﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace Cosmos.Utils
{
    public static class BlobConnector
    {
        public static string UploadFile(HttpPostedFileBase file, string container)
        {
            var url = "";

            if (file != null && file.ContentLength > 0)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["cloudStorageAccount"].ConnectionString;
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);

                //create a block blob
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

                //create a container
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

                string imageName = Guid.NewGuid().ToString() + "-" + Path.GetExtension(file.FileName);
                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(imageName);
                cloudBlockBlob.Properties.ContentType = file.ContentType;

                // Create or overwrite the "myblob" blob with contents from a local file.
                using (var fileStream = file.InputStream)
                {
                    cloudBlockBlob.UploadFromStream(fileStream);
                }
                url = cloudBlockBlob.Uri.ToString();
            }

            if (string.IsNullOrEmpty(url))
                return "";
            return url;
        }

        public static string UploadFile(HttpPostedFileBase file, string container, string customfileName)
        {
            var url = "";

            if (file != null && file.ContentLength > 0)
            {
                var connectionString = ConfigurationManager.ConnectionStrings["cloudStorageAccount"].ConnectionString;
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);

                //create a block blob
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();

                //create a container
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(container);

                string imageName = Guid.NewGuid().ToString() + "-" + Path.GetExtension(file.FileName);
                imageName = customfileName + "-" + imageName;

                CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(imageName);
                cloudBlockBlob.Properties.ContentType = file.ContentType;

                // Create or overwrite the "myblob" blob with contents from a local file.
                using (var fileStream = file.InputStream)
                {
                    cloudBlockBlob.UploadFromStream(fileStream);
                }
                url = cloudBlockBlob.Uri.ToString();
            }

            if (string.IsNullOrEmpty(url))
                return "";
            return url;
        }
    }
}