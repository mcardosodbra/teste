﻿using System.Configuration;

namespace Cosmos.Utils
{
    public static class GlobalVariables
    {
        public static string ApiAddress { get; set; } = ConfigurationManager.AppSettings["ApiAddress"];
        public static string PortalAddress { get; set; } = ConfigurationManager.AppSettings["PortalAddress"];
        public static string PortalBrandAddress { get; set; } = ConfigurationManager.AppSettings["PortalBrandAddress"];
    }
}