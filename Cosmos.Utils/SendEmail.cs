﻿using Cosmos.Model;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Cosmos.Utils
{
    public static class SendEmail
    {
        public static string EmailUser { get; set; } = ConfigurationManager.AppSettings["mailUser"];
        public static string EmailPassword { get; set; } = ConfigurationManager.AppSettings["mailPassword"];
        public static string EmailServer { get; set; } = ConfigurationManager.AppSettings["mailServer"];
        public static string EmailPort { get; set; } = ConfigurationManager.AppSettings["mailPort"];
        public static string SendGridAPIKey { get; set; } = ConfigurationManager.AppSettings["SendGridAPIKey"];

        //public static void Send(Email email)
        //{
        //    using (var smtp = new SmtpClient())
        //    {
        //        smtp.Host = email.Host;
        //        smtp.Port = email.Port;
        //        smtp.EnableSsl = true;
        //        smtp.UseDefaultCredentials = true;
        //        smtp.Credentials = new System.Net.NetworkCredential(email.AddressFrom, email.Password);
        //        using (var mail = new MailMessage())
        //        {
        //            mail.BodyEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        //            mail.SubjectEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");
        //            mail.From = new MailAddress(email.AddressFrom);
        //            mail.To.Add(new MailAddress(email.AddressTo));
        //            mail.Subject = email.Subject;
        //            mail.Body = email.Message;
        //            mail.IsBodyHtml = true;
        //            smtp.Send(mail);
        //        }
        //    }
        //}

        public static async Task Send(Email email)
        {
            var apiKey = SendGridAPIKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("no-reply@disruptiv.tech", "Disruptiv");
            var subject = email.Subject;
            var to = new EmailAddress(email.AddressTo, "User");
            var plainTextContent = email.Message;
            var htmlContent = email.Message;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}