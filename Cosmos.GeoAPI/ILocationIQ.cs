﻿using System;
using System.Threading.Tasks;
using Cosmos.Model.LocationIQ;

namespace Cosmos.GeoAPI
{
    public interface ILocationIQ
    {
        Task<LocationIQMap> GetLocation(double latitude, double longitude);
    }
}