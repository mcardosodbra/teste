﻿using System;
using System.Threading.Tasks;
using Cosmos.Model.GeoAPIOpenStreetMap;

namespace Cosmos.GoogleAPI
{
    public interface IOpenStreetMap
    {
        Task<LocationStreetMap> GetLocation(double latitude, double longitude);
    }
}