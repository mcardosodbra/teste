﻿using Cosmos.GoogleAPI;
using Cosmos.Model.GeoAPIOpenStreetMap;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cosmos.GeoAPI
{
    public class OpenStreetMap : IOpenStreetMap
    {
        public async Task<LocationStreetMap> GetLocation(double latitude, double longitude)
        {
            var url = $"https://nominatim.openstreetmap.org/reverse?format=json&lat={latitude.ToString(CultureInfo.InvariantCulture)}&lon={longitude.ToString(CultureInfo.InvariantCulture)}&zoom=18&addressdetails=1";

            LocationStreetMap locationStreetMap = null;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

                    var response = await httpClient.GetAsync(new Uri(url));
                    if (response.IsSuccessStatusCode)
                    {
                        string result = await response.Content.ReadAsStringAsync();

                        if (result.Contains("Unable to geocode"))
                        {
                            return new LocationStreetMap { address = new Address { city = "", state = "" } };
                        }
                        locationStreetMap = JsonConvert.DeserializeObject<LocationStreetMap>(result);
                    }
                }

                return FixCityNames(locationStreetMap);
            }
            catch (Exception)
            {
                return new LocationStreetMap { address = new Address { city = "", state = "" } };
            }
        }

        private LocationStreetMap FixCityNames(LocationStreetMap locationStreetMap)
        {
            if (locationStreetMap.address.city == "SP") locationStreetMap.address.city = "São Paulo";
            if (locationStreetMap.address.city == "RJ") locationStreetMap.address.city = "Rio de Janeiro";
            return locationStreetMap;
        }
    }
}