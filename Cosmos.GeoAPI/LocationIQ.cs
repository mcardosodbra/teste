﻿using Cosmos.Model.LocationIQ;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cosmos.GeoAPI
{
    public class LocationIQ : ILocationIQ
    {
        public async Task<LocationIQMap> GetLocation(double latitude, double longitude)
        {
            string token = "839e5233eda90b";

            var url = $"https://us1.locationiq.org/v1/reverse.php?key={token}&lat={latitude.ToString(CultureInfo.InvariantCulture)}&lon={longitude.ToString(CultureInfo.InvariantCulture)}&format=json";

            LocationIQMap locationIQMap = null;

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");

                var response = await httpClient.GetAsync(new Uri(url));
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    locationIQMap = JsonConvert.DeserializeObject<LocationIQMap>(result);
                }
            }

            return locationIQMap;
        }
    }
}