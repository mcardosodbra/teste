﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http.ExceptionHandling;
using Cosmos.DataServices.Dapper;
using Cosmos.Model;
using Cosmos.Utils;
using Newtonsoft.Json;
using NLog;

namespace Cosmos.Log
{
    public class NLogExceptionLogger : ExceptionLogger
    {
        private static readonly NLog.Logger Nlog = LogManager.GetCurrentClassLogger();

        public override void Log(ExceptionLoggerContext context)
        {
            string urlWithAccessToken = "https://hooks.slack.com/services/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";

            SlackClient client = new SlackClient(urlWithAccessToken);

            var json = JsonConvert.SerializeObject(context.Exception, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            Nlog.Log(LogLevel.Error, context.Exception, context.Exception.Message + "|EndPointCall: " + RequestToString(context.Request));

            var objLog = new GlobalDataServices();

            try
            {
                client.PostMessage(username: "API-Messages",
                    text: context.Exception.Message + " - EndPointCall: " + RequestToString(context.Request),
                    channel: "#cosmos-errors");
            }
            catch (Exception e)
            {
                Nlog.Log(LogLevel.Error, $"Error Writing to Slack Error Log - {e.Message}");
            }

            try
            {
                objLog.LogtoDb(context.Exception.Message, RequestToString(context.Request) + " |Details: " + json, Model.LogType.Error);
            }
            catch (Exception e)
            {
                Nlog.Log(LogLevel.Error, $"Error Writing to DB Error Log - {e.Message}");
            }
        }

        public void Log(string message, LogType logType)
        {
            string urlWithAccessToken = "https://hooks.slack.com/services/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";

            SlackClient client = new SlackClient(urlWithAccessToken);

            Nlog.Log(LogLevel.Info, message);

            var objLog = new GlobalDataServices();

            try
            {
                client.PostMessage(username: "API-Messages", text: message, channel: "#cosmos-info");
            }
            catch (Exception e)
            {
                Nlog.Log(LogLevel.Error, $"Error Writing to Slack Error Log - {e.Message}");
            }

            try
            {
                objLog.LogtoDb(message, "", logType);
            }
            catch (Exception e)
            {
                Nlog.Log(LogLevel.Error, $"Error Writing to DB Error Log - {e.Message}");
            }
        }

        private static string RequestToString(HttpRequestMessage request)
        {
            var message = new StringBuilder();
            if (request.Method != null)
                message.Append(request.Method);

            if (request.RequestUri != null)
                message.Append(" ").Append(request.RequestUri);

            return message.ToString();
        }
    }
}