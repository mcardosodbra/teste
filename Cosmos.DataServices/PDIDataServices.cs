﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class PDIDataServices : IPDI
    {
        public void AddPDI(Model.PDI Pdi)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.PDI pdi = Converters.DoReflection<Model.PDI, Entity.PDI>(Pdi, new Entity.PDI());
                dbContext.PDI.Add(pdi);
                dbContext.SaveChanges();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public void UpdatePDI(Model.PDI Pdi)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                PDI pdi = dbContext.PDI.Where(p => p.Id == Pdi.Id).FirstOrDefault();
                pdi = Converters.DoReflection<Model.PDI, Entity.PDI>(Pdi, pdi);
                dbContext.SaveChanges();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.PDI pdi = dbContext.PDI.Where(u => u.Id == id).FirstOrDefault();

                if (pdi != null)
                {
                    pdi.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.PDI pdi = dbContext.PDI.Where(u => u.Id == id).FirstOrDefault();

                if (pdi != null)
                {
                    pdi.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.PDI> GetPDIAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityPDI = dbContext.PDI.ToList();
                List<Model.PDI> listPDI = new List<Model.PDI>();

                foreach (Entity.PDI type in entityPDI)
                    listPDI.Add(Converters.DoReflection(type, new Model.PDI()));

                return listPDI;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.PDI GetPDIById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                PDI entityPDI = dbContext.PDI.Where(p => p.Id == id).SingleOrDefault();
                Model.PDI pdi = Converters.DoReflection<Entity.PDI, Model.PDI>(entityPDI, new Model.PDI());

                return pdi;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}