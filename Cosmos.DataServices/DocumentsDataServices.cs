﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class DocumentsDataServices : IDocuments
    {
        public bool AddDocuments(Model.Document Document)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Document document = Converters.DoReflection<Model.Document, Entity.Document>(Document, new Entity.Document());
                dbContext.Document.Add(document);
                int result = dbContext.SaveChanges();
                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Document GetDocumentsById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Document entityDocument = dbContext.Document.Where(p => p.Id == id).SingleOrDefault();
                Model.Document document = Converters.DoReflection<Entity.Document, Model.Document>(entityDocument, new Model.Document());

                return document;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Document> GetDocumentsAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityDocuments = dbContext.Document.ToList();
                List<Model.Document> listDocuments = new List<Model.Document>();

                foreach (Entity.Document item in entityDocuments)
                {
                    Model.Document document = Converters.DoReflection(item, new Model.Document());
                    document.DocumentType = Converters.DoReflection(item.DocumentType, new Model.DocumentType());

                    listDocuments.Add(document);
                }

                return listDocuments;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateDocuments(Model.Document Document)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Document DocumentOriginal = dbContext.Document.Where(p => p.Id == Document.Id).FirstOrDefault();
                DocumentOriginal = Converters.DoReflection<Model.Document, Entity.Document>(Document, DocumentOriginal);
                int result = dbContext.SaveChanges();
                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Document entity_document = dbContext.Document.Where(u => u.Id == id).FirstOrDefault();

                if (entity_document != null)
                {
                    entity_document.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Document entity_document = dbContext.Document.Where(u => u.Id == id).FirstOrDefault();

                if (entity_document != null)
                {
                    entity_document.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}