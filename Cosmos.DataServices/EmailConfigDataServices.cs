﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using Cosmos.Model;
using System;
using System.Linq;

namespace Cosmos.DataServices
{
    public class EmailConfigDataServices : IEmailConfig
    {
        public Email GetConfigs()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                EmailConfig entityEmailConfig = dbContext.EmailConfig.SingleOrDefault();
                Email documentType = Converters.DoReflection<EmailConfig, Email>(entityEmailConfig, new Email());

                return documentType;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}