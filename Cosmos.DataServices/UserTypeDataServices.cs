﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using Cosmos.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class UserTypeDataServices : IUserType
    {
        public void AddUserType(Model.UserType UserType)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.UserType userType = Converters.DoReflection<Model.UserType, Entity.UserType>(UserType, new Entity.UserType());
                dbContext.UserType.Add(userType);
                dbContext.SaveChanges();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.UserType> GetAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityUserTypes = dbContext.UserType.ToList();
                List<Model.UserType> listUserType = new List<Model.UserType>();

                foreach (Entity.UserType type in entityUserTypes)
                    listUserType.Add(Converters.DoReflection(type, new Model.UserType()));

                return listUserType;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.UserType GetUserTypeById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.UserType entityUserType = dbContext.UserType.Where(p => p.Id == id).SingleOrDefault();
                Model.UserType userType = Converters.DoReflection<Entity.UserType, Model.UserType>(entityUserType, new Model.UserType());

                return userType;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public void UpdateUserType(Model.UserType UserType)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.UserType UserTypeOriginal = dbContext.UserType.Where(p => p.Id == UserType.Id).FirstOrDefault();
                UserTypeOriginal = Converters.DoReflection<Model.UserType, Entity.UserType>(UserType, UserTypeOriginal);
                dbContext.SaveChanges();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.UserType entity_userType = dbContext.UserType.Where(u => u.Id == id).FirstOrDefault();

                if (entity_userType != null)
                {
                    entity_userType.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.UserType entity_userType = dbContext.UserType.Where(u => u.Id == id).FirstOrDefault();

                if (entity_userType != null)
                {
                    entity_userType.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}