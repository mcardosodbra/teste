﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class AppVersionDataServices
    {
        public bool Add(Model.AppVersion AppVersion)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.AppVersion appVersion = Converters.DoReflection<Model.AppVersion, Entity.AppVersion>(AppVersion, new Entity.AppVersion());
                appVersion.Id = Guid.NewGuid().ToString();
                AppVersion.Status = true;
                dbContext.AppVersion.Add(appVersion);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool AddNewVersion(Model.AppVersion AppVersion)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.AppVersion appVersion = Converters.DoReflection<Model.AppVersion, Entity.AppVersion>(AppVersion, new Entity.AppVersion());
                var list = dbContext.AppVersion.Where(s => s.ApplicationId == AppVersion.ApplicationId).ToList();
                if (list.Where(s => s.VersionCode > AppVersion.VersionCode).Any())
                {
                    AppVersion.Downgrade = true;
                    appVersion.DowngradeJustify = "Versão inferior a que estava ativada!";
                }
                list.ToList().ForEach(c => c.Active = false);
                List<AppVersion> VersionOriginal = dbContext.AppVersion.Where(p => p.ApplicationId == AppVersion.ApplicationId).ToList();
                VersionOriginal = Converters.DoReflectionList<Entity.AppVersion, Entity.AppVersion>(list, VersionOriginal);
                dbContext.SaveChanges();
                appVersion.Active = true;
                AppVersion.Status = true;
                appVersion.Id = Guid.NewGuid().ToString();
                dbContext.AppVersion.Add(appVersion);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Update(Model.AppVersion AppVersion)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                AppVersion AppVersionOriginal = dbContext.AppVersion.Where(p => p.Id == AppVersion.Id).FirstOrDefault();
                AppVersion.ReleaseDate = AppVersionOriginal.ReleaseDate;
                AppVersion.Status = true;
                if (!string.IsNullOrEmpty(AppVersion.DowngradeJustify))
                {
                    AppVersion.Downgrade = true;
                }
                AppVersionOriginal = Converters.DoReflection<Model.AppVersion, Entity.AppVersion>(AppVersion, AppVersionOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.AppVersion> GetVersionsByApplicationId(string Id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.AppVersion.Where(x => x.ApplicationId.Equals(Id)).ToList();

                List<Model.AppVersion> versions = new List<Model.AppVersion>();

                foreach (Entity.AppVersion version in entities)
                    versions.Add(Converters.DoReflection(version, new Model.AppVersion()));

                return versions;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.AppVersion> GetVersionsByApplicationApiId(string Id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.AppVersion.Where(x => x.ApplicationId.Equals(Id) && x.Active == true).ToList();

                List<Model.AppVersion> versions = new List<Model.AppVersion>();

                foreach (Entity.AppVersion version in entities)
                    versions.Add(Converters.DoReflection(version, new Model.AppVersion()));

                return versions;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        private void FillApplication(ref Model.AppVersion appVersion)
        {
            appVersion.OpSystems = new OpSystemDataServices().GetVerRequiredById(appVersion.Id);
        }

        public Model.AppVersion GetAppVersionById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.AppVersion entityApp = dbContext.AppVersion.Where(p => p.Id == id).SingleOrDefault();
                Model.AppVersion app = Converters.DoReflection<Entity.AppVersion, Model.AppVersion>(entityApp, new Model.AppVersion());

                if (app != null)
                    FillApplication(ref app);

                return app;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.AppVersion entity_app = dbContext.AppVersion.Where(u => u.Id == id).FirstOrDefault();

                if (entity_app != null)
                {
                    entity_app.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.AppVersion entity_app = dbContext.AppVersion.Where(u => u.Id == id).FirstOrDefault();

                if (entity_app != null)
                {
                    var list = dbContext.AppVersion.Where(s => s.ApplicationId == entity_app.ApplicationId).ToList();
                    list.ToList().ForEach(c => c.Active = false);
                    List<AppVersion> VersionOriginal = dbContext.AppVersion.Where(p => p.ApplicationId == entity_app.ApplicationId).ToList();
                    VersionOriginal = Converters.DoReflectionList<Entity.AppVersion, Entity.AppVersion>(list, VersionOriginal);
                    dbContext.SaveChanges();
                    entity_app.Active = true;
                    entity_app.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsVersionInUse(string Version)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.AppVersion.Where(e => e.Version.Equals(Version)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsVersionInUseForApp(string version, string app)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.AppVersion.Where(e => (e.Version.Equals(version) && e.ApplicationId.Equals(app))).Any();
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}