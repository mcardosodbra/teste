﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class OpSystemDataServices : IOpSystem
    {
        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.OperationalSystem entity_opSystem = dbContext.OperationalSystem.Where(u => u.Id == id).FirstOrDefault();

                if (entity_opSystem != null)
                {
                    entity_opSystem.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool AddOpSystem(Model.OperationalSystem operationalSystem)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.OperationalSystem opSystem = Converters.DoReflection<Model.OperationalSystem, Entity.OperationalSystem>(operationalSystem, new Entity.OperationalSystem());
                dbContext.OperationalSystem.Add(opSystem);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.OperationalSystem entity_opSystem = dbContext.OperationalSystem.Where(u => u.Id == id).FirstOrDefault();

                if (entity_opSystem != null)
                {
                    entity_opSystem.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.OperationalSystem> GetAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityOpSystem = dbContext.OperationalSystem.ToList();
                List<Model.OperationalSystem> listOpSystem = new List<Model.OperationalSystem>();

                foreach (Entity.OperationalSystem opSystem in entityOpSystem)
                {
                    listOpSystem.Add(Converters.DoReflection(opSystem, new Model.OperationalSystem()));
                }

                return listOpSystem;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.OperationalSystem> GetVerRequiredById(string Id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.OperationalSystem.ToList();
                List<Model.OperationalSystem> operationalSystems = new List<Model.OperationalSystem>();

                foreach (Entity.OperationalSystem opSystem in entities)
                    operationalSystems.Add(Converters.DoReflection(opSystem, new Model.OperationalSystem()));

                return operationalSystems;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.OperationalSystem GetById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.OperationalSystem entityOpSystem = dbContext.OperationalSystem.Where(p => p.Id == id).SingleOrDefault();
                Model.OperationalSystem opSystem = Converters.DoReflection<Entity.OperationalSystem, Model.OperationalSystem>(entityOpSystem, new Model.OperationalSystem());

                return opSystem;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateOpSystem(Model.OperationalSystem operationalSystem)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.OperationalSystem opSystem = dbContext.OperationalSystem.Where(p => p.Id == operationalSystem.Id).FirstOrDefault();
                opSystem = Converters.DoReflection<Model.OperationalSystem, Entity.OperationalSystem>(operationalSystem, opSystem);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}