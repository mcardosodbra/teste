﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class AppStoreDataServices
    {
        public bool Add(Model.Application App)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Application app = Converters.DoReflection<Model.Application, Entity.Application>(App, new Entity.Application());
                app.Id = new Guid().ToString();
                app.Type = 1;
                app.LastUpdate = DateTime.Now;
                dbContext.Application.Add(app);
                int result = dbContext.SaveChanges();

                if (App.Versions != null && App.Versions.Count() > 0)
                {
                    App.Versions.First().ApplicationId = dbContext.Application.Where(x => x.Name.Equals(app.Name)).First().Id;
                    App.Versions.First().Status = true;
                    new AppVersionDataServices().Add(App.Versions.First());
                }

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Application> GetAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.Application.ToList().Where(x => x.Status == true).OrderByDescending(s => s.Relevance);
                entities = entities.OrderByDescending(x => x.InclusionDate);
                List<Model.Application> apps = new List<Model.Application>();

                foreach (Entity.Application item in entities)
                {
                    Model.Application app = Converters.DoReflection(item, new Model.Application());
                    FillApplication(ref app);
                    apps.Add(app);
                }

                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Application> GetRemovedApps()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.Application.ToList().Where(x => x.Status == false).OrderByDescending(s => s.Relevance);
                entities = entities.OrderByDescending(x => x.InclusionDate);
                List<Model.Application> apps = new List<Model.Application>();

                foreach (Entity.Application item in entities)
                {
                    Model.Application app = Converters.DoReflection(item, new Model.Application());
                    FillApplication(ref app);
                    apps.Add(app);
                }

                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Application> GetByCategory(string CategoryId)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                List<Model.Application> apps = new List<Model.Application>();

                List<Entity.Application> entities = dbContext.Application.Where(p => p.CategoryId.Equals(CategoryId)).ToList();

                foreach (Entity.Application item in entities)
                {
                    Model.Application app = Converters.DoReflection(item, new Model.Application());

                    if (app != null)
                        FillApplication(ref app);

                    apps.Add(app);
                }

                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Application> GetByCategoryApi(string CategoryId)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                List<Model.Application> apps = new List<Model.Application>();

                List<Entity.Application> entities = dbContext.Application.Where(p => p.CategoryId.Equals(CategoryId) && p.Status == true).ToList();

                foreach (Entity.Application item in entities)
                {
                    Model.Application app = Converters.DoReflection(item, new Model.Application());

                    if (app != null)
                        FillApplicationApi(ref app);

                    apps.Add(app);
                }

                foreach (var item in apps.ToList())
                {
                    if (!item.Versions.Where(s => s.Active == true).Any())
                    {
                        apps.Remove(item);
                    }
                }
                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Application GetApplicationById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Application entity = dbContext.Application.Where(p => p.Id == id).SingleOrDefault();
                Model.Application app = Converters.DoReflection<Entity.Application, Model.Application>(entity, new Model.Application());

                if (app != null)
                    FillApplication(ref app);

                return app;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Application GetApplicationApiById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Application entity = dbContext.Application.Where(p => p.Id == id).SingleOrDefault();
                Model.Application app = Converters.DoReflection<Entity.Application, Model.Application>(entity, new Model.Application());

                if (app != null)
                    FillApplicationApi(ref app);

                foreach (var item in app.Versions.ToList())
                {
                    if (item.Active == false)
                    {
                        app.Versions.Remove(item);
                    }
                }
                return app;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        private void FillApplication(ref Model.Application application)
        {
            AppUserActionDataServices appUsr = new AppUserActionDataServices();
            application.Versions = new AppVersionDataServices().GetVersionsByApplicationId(application.Id);
            foreach (var item in application.Versions)
            {
                item.InstalledCount = appUsr.GetInstalledApps(item.ApplicationId, item.Id);
                item.ActiveCount = appUsr.GetInstalledActive(item.ApplicationId, item.Id);
                item.DownloadCount = appUsr.GetDownloadApps(item.ApplicationId, item.Id);
            }
            application.Category = new CategoryDataServices().GetCategoryById(application.CategoryId);
        }

        private void FillApplicationApi(ref Model.Application application)
        {
            AppUserActionDataServices appUsr = new AppUserActionDataServices();
            application.Versions = new AppVersionDataServices().GetVersionsByApplicationApiId(application.Id);
            foreach (var item in application.Versions)
            {
                item.InstalledCount = appUsr.GetInstalledApps(item.ApplicationId, item.Id);
                item.ActiveCount = appUsr.GetInstalledActive(item.ApplicationId, item.Id);
                item.DownloadCount = appUsr.GetDownloadApps(item.ApplicationId, item.Id);
            }
            application.Category = new CategoryDataServices().GetCategoryById(application.CategoryId);
        }

        //revisar ppq não está atendendo o que precisamos
        public List<Model.Application> SearchByCategory(string CategoryDescription)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                List<Model.Application> apps = new List<Model.Application>();

                List<Entity.Application> entities = dbContext.Application.Where(p => p.CategoryId.Equals(CategoryDescription)).ToList();

                foreach (Entity.Application item in entities)
                {
                    Model.Application app = Converters.DoReflection(item, new Model.Application());

                    if (app != null)
                        FillApplication(ref app);

                    apps.Add(app);
                }

                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public void SearchByAppId()
        {
            throw new NotImplementedException();
        }

        public bool DownloadUpdate(Model.Application App)
        {
            App.DownloadedTimes++;

            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Application AppOriginal = dbContext.Application.Where(p => p.Id == App.Id).FirstOrDefault();
                AppOriginal = Converters.DoReflection<Model.Application, Entity.Application>(App, AppOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Update(Model.Application App)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Application AppOriginal = dbContext.Application.Where(p => p.Id == App.Id).FirstOrDefault();
                AppOriginal = Converters.DoReflection<Model.Application, Entity.Application>(App, AppOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Remove(string id, bool removeFromCentral)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Application entity_app = dbContext.Application.Where(u => u.Id == id).FirstOrDefault();

                if (entity_app != null)
                {
                    entity_app.RemoveFromCentral = removeFromCentral;
                    entity_app.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Application entity_app = dbContext.Application.Where(u => u.Id == id).FirstOrDefault();

                if (entity_app != null)
                {
                    entity_app.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsApplicationNameInUse(string Name)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.Application.Where(e => e.Name.Equals(Name)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Application> GetApplicationsActiveToCentral(string token)
        {
            throw new NotImplementedException();
        }

        public List<Model.Application> GetApplicationsToUpdateVersion(string token)
        {
            throw new NotImplementedException();
        }

        public List<Model.Application> GetApplicationsToRemove(string token)
        {
            throw new NotImplementedException();
        }
    }
}