﻿using System;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class StateDataServices : BaseDapperDataServices, IStateDataServices
    {
        public List<State> SelectState()
        {
            // Select
            List<State> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT S.Id, S.Name, R.Id, R.Name FROM [State] S INNER JOIN Region R on R.Id = S.RegionId";

                ret = db.Query<State, Region, State>(sql, (state, region) =>
                {
                    state.Region = region;
                    return state;
                }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public void InsertState(State state)
        {
            // Insert
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [State] (Name) VALUES (@Name)";

                db.Execute(sql, new { Name = state.Name }, commandType: CommandType.Text);
            }
        }

        public void UpdateState(State state)
        {
            // Update
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [State] SET Name = @Name WHERE Id = @ID";

                db.Execute(sql, new { Name = state.Name, state.Id }, commandType: CommandType.Text);
            }
        }

        public void DeleteState(State state)
        {
            // Delete
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"DELETE FROM [State] WHERE Id = @ID";

                db.Execute(sql, new { state.Name, state.Id }, commandType: CommandType.Text);
            }
        }

        public List<State> SelectStateByRegion(Guid regionId)
        {
            // Select
            List<State> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT S.Id, S.Name, R.Id, R.Name FROM [State] S INNER JOIN Region R on R.Id = S.RegionId WHERE S.RegionId = @RegionId";

                ret = db.Query<State, Region, State>(sql, (state, region) =>
                {
                    state.Region = region;
                    return state;
                }, new { RegionId = regionId }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<State> SelectStateByRegion(Guid[] regionId)
        {
            // Select
            List<State> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT S.Id, S.Name, R.Id, R.Name FROM [State] S INNER JOIN Region R on R.Id = S.RegionId WHERE S.RegionId IN @RegionId ORDER BY S.Name";

                ret = db.Query<State, Region, State>(sql, (state, region) =>
                {
                    state.Region = region;
                    return state;
                }, new { RegionId = regionId }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }
    }
}