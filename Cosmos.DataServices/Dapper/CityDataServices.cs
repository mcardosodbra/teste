﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class CityDataServices : BaseDapperDataServices, ICityDataServices
    {
        public List<City> SelectCity()
        {
            List<City> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT C.Id, C.Name, S.Id, S.Name FROM [City] C INNER JOIN STATE S on S.Id = C.StateId";

                ret = db.Query<City, State, City>(sql, (city, state) =>
                 {
                     city.State = state;
                     return city;
                 }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<City> SelectCity(Guid[] stateId)
        {
            List<City> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name FROM [City] WHERE StateId IN @StateId order by Name";

                ret = db.Query<City>(sql, new { StateId = stateId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }
    }
}