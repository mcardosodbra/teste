﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class ApplicationSuggestionDataServices : BaseDapperDataServices, IApplicationSuggestionDataServices
    {
        public List<ApplicationSuggestion> SelectApplicationSuggestion()
        {
            // Select
            List<ApplicationSuggestion> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT AppS.Id, AppS.Suggestion, AppS.DtCreated,
                                    U.Id, U.Name, U.LastName, U.Telephone, U.Email FROM [ApplicationSuggestion] AppS
                                    INNER JOIN [User] U on U.Id = AppS.UserId ORDER BY DtCreated DESC";

                ret = db.Query<ApplicationSuggestion, User, ApplicationSuggestion>(sql, (applicationsuggestion, user) =>
                    {
                        applicationsuggestion.User = user;
                        return applicationsuggestion;
                    }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public void InsertApplicationSuggestion(ApplicationSuggestion applicationsuggestion)
        {
            // Insert
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [ApplicationSuggestion] (Id, Suggestion, UserId, DtCreated) VALUES (NEWID(), @Suggestion, @UserId, @DtCreation)";

                db.Execute(sql, new { Suggestion = applicationsuggestion.Suggestion, DtCreation = applicationsuggestion.DtCreated, UserId = applicationsuggestion.User.Id }, commandType: CommandType.Text);
            }
        }
    }
}