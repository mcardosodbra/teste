﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class TermsAndConditionsDataServices : BaseDapperDataServices, ITermsAndConditionsDataServices
    {
        public void Active(TermsAndConditions termsAndConditions)
        {
            // Update
            using (SqlConnection db = new SqlConnection(connstring))
            {
                const string sqlCleanActives = @"Update [TermsAndConditions] SET Active = 0";
                db.Execute(sqlCleanActives, commandType: CommandType.Text);

                const string sql = @"UPDATE [TermsAndConditions] SET Active = 1 WHERE Id = @Id";
                db.Execute(sql, new { Id = termsAndConditions.Id }, commandType: CommandType.Text);
            }
        }

        public List<TermsAndConditions> GetTermsAndConditions()
        {
            List<TermAndConditionUserResponse> responses = GetResponsesList();
            List<TermsAndConditions> ret;
            using (SqlConnection db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT a.[Id] ,[TitlePT] ,[TitleEN] ,[TitleES] ,[Register] ,[Version] ,[FilePT]  ,[FileEN], a.Active ,[FileES], a.[Status] ,[IdBrand], b.Id, b.Name, b.Logotype, b.Status FROM [TermsAndConditions] A
                                    INNER JOIN Brand B on a.[IdBrand] = b.Id and a.status = 1 order by a.version desc";

                ret = db.Query<TermsAndConditions, Brand, TermsAndConditions>(sql, (objTerm, brand) =>
                {
                    objTerm.Brand = brand;
                    return objTerm;
                }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            foreach (TermsAndConditions item in ret)
            {
                item.Recuseds = responses.Count(x =>
                        x.TermAndConditionId == item.Id &&
                        x.Accept == false);
                item.Accepteds = responses.Count(x =>
                        x.TermAndConditionId == item.Id &&
                        x.Accept == true);
            }
            return ret;
        }

        public TermsAndConditions GetTermsAndConditions(TermsAndConditions terms)
        {
            List<TermAndConditionUserResponse> responses = GetResponsesList();

            TermsAndConditions ret;
            using (SqlConnection db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT a.[Id] ,[TitlePT] ,[TitleEN] ,[TitleES] ,[Register] ,[Version] ,[FilePT]  ,[FileEN] ,[FileES], a.[Status] ,[IdBrand], a.Active, b.Id, b.Name, b.Logotype, b.Status FROM [TermsAndConditions] A
                                    INNER JOIN Brand B on a.[IdBrand] = b.Id where Id = @Id and a.status = 1 order by a.version desc";

                ret = db.Query<TermsAndConditions, Brand, TermsAndConditions>(sql, (objTerm, brand) =>
                {
                    objTerm.Brand = brand;
                    return objTerm;
                }, new { Id = terms.Id }, splitOn: "Id", commandType: CommandType.Text).FirstOrDefault();
            }

            ret.Recuseds = responses.Count(x =>
                    x.TermAndConditionId == ret.Id &&
                    x.Accept == false);
            ret.Accepteds = responses.Count(x =>
                    x.TermAndConditionId == ret.Id &&
                    x.Accept == true);

            return ret;
        }

        public void Insert(TermsAndConditions termsandconditions)
        {
            // Insert

            using (SqlConnection db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [TermsAndConditions] (Id, TitlePT, TitleEN, TitleES, Register, FilePT, FileEN, FileES, Status, Active, IdBrand ) VALUES (@Id, @TitlePT, @TitleEN, @TitleES, @Register, @FilePT, @FileEN, @FileES, 1, 0, @IdBrand)";

                db.Execute(sql, new { Id = Guid.NewGuid(), TitlePT = termsandconditions.TitlePT, TitleEN = termsandconditions.TitleEN, TitleES = termsandconditions.TitleES, Register = termsandconditions.Register, FilePT = termsandconditions.FilePT, FileEN = termsandconditions.FileEN, FileES = termsandconditions.FileES, IdBrand = termsandconditions.IdBrand }, commandType: CommandType.Text);
            }
        }

        public void Update(TermsAndConditions termsandconditions)
        {
            // Update
            using (SqlConnection db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [TermsAndConditions] SET Id = @Id, TitlePT = @TitlePT, TitleEN = @TitleEN, TitleES = @TitleES, FilePT = @FilePT, FileEN = @FileEN, FileES = @FileES, Status = @Status, Active = @Active WHERE Id = @Id";

                db.Execute(sql, new { Id = termsandconditions.Id, TitlePT = termsandconditions.TitlePT, TitleEN = termsandconditions.TitleEN, TitleES = termsandconditions.TitleES, Register = termsandconditions.Register, FilePT = termsandconditions.FilePT, FileEN = termsandconditions.FileEN, FileES = termsandconditions.FileES, Status = termsandconditions.Status, Active = termsandconditions.Active }, commandType: CommandType.Text);
            }
        }

        private List<TermAndConditionUserResponse> GetResponsesList()
        {
            List<TermAndConditionUserResponse> ret;
            string sql =
                @"select a.[Id] ,[TermAndConditionId] ,[UserId] ,a.[Register] ,[Accept] FROM [dbo].[TermAndConditionUserResponse] a
                    inner join TermsAndConditions b on a.TermAndConditionId = b.Id  WHERE Status = 1";

            using (SqlConnection db = new SqlConnection(connstring))
            {
                ret = db.Query<TermAndConditionUserResponse>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }
    }
}