﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class ApplicationUserActionDataServices : BaseDapperDataServices, IApplicationUserAction
    {
        public bool Add(ApplicationUserAction appUser)
        {
            // Insert
            try
            {
                using (var db = new SqlConnection(connstring))
                {
                    const string sql = @"INSERT INTO [ApplicationUserAction] ([Date], [ApplicationId], [UserId], [Type], [AppVersionID], [Package], [CarId]) VALUES (GETDATE(), @AppID, @UserID, @Type, @AppVersionID, @Package, @CarId)";

                    db.Execute(sql, new { AppID = appUser.ApplicationId, UserID = appUser.UserId, Type = appUser.Type, AppVersionID = appUser.AppVersionId, Package = appUser.Package, CarId = appUser.CarId }, commandType: CommandType.Text);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<ApplicationUserAction> GetAll()
        {
            var sql = "SELECT Id, Date, ApplicationId, UserId, Type, AppVersionID, CarId FROM ApplicationUserAction";
            List<ApplicationUserAction> ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<ApplicationUserAction>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public bool Update(ApplicationUserAction appUser)
        {
            throw new NotImplementedException();
        }
    }
}