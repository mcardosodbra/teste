﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class AppStoreDataServices : BaseDapperDataServices, IAppStoreDataService
    {
        private readonly IApplicationUserAction _applicationUserAction;
        private readonly ICar _car;

        public AppStoreDataServices(IApplicationUserAction applicationUserAction, ICar car)
        {
            _applicationUserAction = applicationUserAction;
            _car = car;
        }

        public bool Active(string id)
        {
            throw new NotImplementedException();
        }

        public bool Add(Application App)
        {
            throw new NotImplementedException();
        }

        public List<Application> GetAll()
        {
            var versionList = GetAppVersionList();
            var appuserActionList = _applicationUserAction.GetAll();

            string sql = "SELECT " +
                         "A.Id, A.Name, A.LogoType, A.InclusionDate, A.CategoryId, A.DownloadedTimes, A.ActiveVersion, A.LastUpdate, A.Relevance, A.PackageName, A.InstalledTimes, A.UninstalledTimes, A.Description, A.DescriptionES, A.DescriptionEN, A.Status, A.RemoveFromCentral, A.Type, " +
                         "C.Id, C.Description, C.Status, C.IconUrl, C.DescriptionEN, c.DescriptionES, C.Register, C.CounterApp " +
                         "FROM Application A " +
                         "INNER JOIN Category C on C.Id = A.CategoryId " +
                         "WHERE A.Status = 1 order by Relevance desc, InclusionDate desc ";

            List<Application> ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Application, Category, Application>(sql, (app, category) =>
                {
                    app.Category = category;
                    return app;
                }, splitOn: "Id, Id", commandType: CommandType.Text).ToList();

                foreach (var app in ret)
                {
                    app.DownloadedTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);

                    app.InstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.Install);

                    app.UninstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.Uninstall);
                }

                foreach (var ver in versionList)
                {
                    ver.InstalledCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.Install);

                    ver.ActiveCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.Active);

                    ver.DownloadCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);
                }

                if (versionList.Count > 0)
                {
                    foreach (var app in ret)
                    {
                        app.Versions.Add(versionList.SingleOrDefault(v => v.ApplicationId == app.Id && v.Active));
                    }
                }
            }

            return ret;
        }

        public List<Application> GetAllPortal()
        {
            var versionList = GetAppVersionListPoral();
            var appuserActionList = _applicationUserAction.GetAll();

            string sql = "SELECT " +
                         "A.Id, A.Name, A.LogoType, A.InclusionDate, A.CategoryId, A.DownloadedTimes, A.ActiveVersion, A.LastUpdate, A.Relevance, A.PackageName, A.InstalledTimes, A.UninstalledTimes, A.Description, A.DescriptionES, A.DescriptionEN, A.Status, A.RemoveFromCentral, A.Type, " +
                         "C.Id, C.Description, C.Status, C.IconUrl, C.DescriptionEN, c.DescriptionES, C.Register, C.CounterApp " +
                         "FROM Application A " +
                         "INNER JOIN Category C on C.Id = A.CategoryId " +
                         "WHERE A.Status = 1 order by Relevance desc, InclusionDate desc";

            List<Application> ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Application, Category, Application>(sql, (app, category) =>
                {
                    app.Category = category;
                    return app;
                }, splitOn: "Id, Id", commandType: CommandType.Text).ToList();
                foreach (var app in ret)
                {
                    app.DownloadedTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);

                    app.InstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.Install);

                    app.UninstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.Uninstall);
                }
                if (versionList.Count > 0)
                {
                    foreach (var app in ret)
                    {
                        var versions = versionList.Where(v => v.ApplicationId == app.Id).ToList();
                        foreach (var item in versions)
                        {
                            app.Versions.Add(item);
                        }
                    }
                }
            }

            return ret;
        }

        public List<Application> GetMultiAppToUpdateVersion(string token)
        {
            var versionList = GetAppVersionList();

            var sql = string.Empty;
            List<Application> ret;

            using (var db = new SqlConnection(connstring))
            {
                sql = @"select app.id, app.name,  app.status, app.logotype, app.inclusionDate, app.CategoryId, (select count(Id) from ApplicationUserAction where ApplicationId = app.id) as downloadedTimes, (select VersionCode from AppVersion where id = appu.AppVersionID) as activeVersion, lastupdate, relevance, packagename,  (select count(id) from ApplicationUserAction where ApplicationId = app.Id and type = 3 ) as InstalledTimes,  (select count(id) from ApplicationUserAction where ApplicationId = app.Id and type = 4 ) as UninstalledTimes, app.Description, app.DescriptionEN, app.DescriptionES, app.Type
                        from application app
                        left join applicationuseraction appU on app.id = appU.applicationid
                        left join Car car on car.userid = appU.userid
                        where app.status = 1 and appU.type = 3 and app.type = 2
                        and car.token = @Token
                        order by relevance desc , InclusionDate desc";

                ret = db.Query<Application>(sql, new { Token = token }, commandType: CommandType.Text).ToList();

                if (versionList.Count > 0)
                {
                    foreach (var item in ret)
                    {
                        var version = versionList.SingleOrDefault(v => v.ApplicationId == item.Id);
                        if (item.ActiveVersion != version.VersionCode)
                        {
                            item.Versions.Add(version);
                        }
                    }
                }
            }
            return ret;
        }

        public List<Application> GetAllByLanguage(Language language)
        {
            List<Application> lstApp;

            switch (language.Name)
            {
                case "es-ES":
                    lstApp = GetAll().Where(a => a.DescriptionES != null).ToList();
                    break;

                case "en-US":
                    lstApp = GetAll().Where(a => a.DescriptionEN != null).ToList();
                    break;

                default:
                    lstApp = GetAll().Where(a => a.Description != null).ToList();
                    break;
            }

            return lstApp;
        }

        private List<AppVersion> GetAppVersionList()
        {
            List<AppVersion> ret;
            var sql =
                @"SELECT id, Active, version, versioncode , ApkSize, ApkUri, ApplicationId, Downgrade, DowngradeJustify, Package, ReleaseDate, ReleaseNote, releaseNoteEN, releasenoteES
                    FROM appversion WHERE Status = 1";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<AppVersion>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        private List<ApplicationUserAction> GetInstalledApps(string carId)
        {
            List<ApplicationUserAction> ret;
            var sql =
                @"select id, date, ApplicationId, UserId, [type], AppVersionID, package, carid from ApplicationUserAction where carid = @carId and type in (3,4) order by date desc";
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<ApplicationUserAction>(sql, new { carId = carId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        private List<AppVersion> GetAppVersionListPoral()
        {
            List<AppVersion> ret;
            var sql =
                @"SELECT id, Active, version, versioncode , ApkSize, ApkUri, ApplicationId, Downgrade, DowngradeJustify, Package, ReleaseDate, ReleaseNote, releaseNoteEN, releasenoteES
                    FROM appversion";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<AppVersion>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        private List<AppVersion> GetAppVersionListByAppId(Guid applicationId)
        {
            List<AppVersion> ret;
            var sql =
                @"SELECT id, Active, version, versioncode , ApkSize, ApkUri, ApplicationId, Downgrade, DowngradeJustify, Package, ReleaseDate, ReleaseNote, releaseNoteEN, releasenoteES
                    FROM appversion WHERE Status = 1 and ApplicationId = @ApplicationId";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<AppVersion>(sql, new { ApplicationId = applicationId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        private List<AppVersion> GetAppVersionListByAppIdPortal(Guid applicationId)
        {
            List<AppVersion> ret;
            var sql =
                @"SELECT id, Active, version, versioncode , ApkSize, ApkUri, ApplicationId, Downgrade, DowngradeJustify, Package, ReleaseDate, ReleaseNote, releaseNoteEN, releasenoteES
                    FROM appversion WHERE ApplicationId = @ApplicationId";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<AppVersion>(sql, new { ApplicationId = applicationId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public Application GetApplicationById(string id)
        {
            var versionList = GetAppVersionListByAppId(new Guid(id));
            var appuserActionList = _applicationUserAction.GetAll();

            string sql = "SELECT " +
                         "A.Id, A.Name, A.LogoType, A.InclusionDate, A.CategoryId, A.DownloadedTimes, A.ActiveVersion, A.LastUpdate, A.Relevance, A.PackageName, A.InstalledTimes, A.UninstalledTimes, A.Description, A.DescriptionES, A.DescriptionEN, A.Status, A.RemoveFromCentral, A.Type," +
                         "C.Id, C.Description, C.Status, C.IconUrl, C.DescriptionEN, c.DescriptionES, C.Register, C.CounterApp " +
                         "FROM Application A " +
                         "INNER JOIN Category C on C.Id = A.CategoryId " +
                         "WHERE A.Status = 1 AND A.Id = @ApplicationId and a.type = 1";

            Application ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Application, Category, Application>(sql, (app, category) =>
                {
                    app.Category = category;
                    return app;
                }, new { ApplicationId = id }, splitOn: "Id, Id", commandType: CommandType.Text).Single();

                foreach (var ver in versionList)
                {
                    ver.InstalledCount = appuserActionList.Count(x =>
                x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                x.Type == ApplicationUserActionType.Install);

                    ver.ActiveCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.Active);

                    ver.DownloadCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);
                }

                if (versionList.Count > 0)
                {
                    ret.Versions.Add(versionList.SingleOrDefault(v => v.ApplicationId == ret.Id && v.Active));
                }
            }

            return ret;
        }

        public Application GetApplicationByPackageName(string packageName)
        {
            var appuserActionList = _applicationUserAction.GetAll();

            string sql = @"SELECT DISTINCT A.Id, A.Name, A.LogoType, A.InclusionDate, A.CategoryId, A.DownloadedTimes, A.ActiveVersion, A.LastUpdate, A.Relevance, A.PackageName, A.InstalledTimes, A.UninstalledTimes, A.Description, A.DescriptionES, A.DescriptionEN, A.Status, A.RemoveFromCentral, A.Type,
                            C.Id, C.Description, C.Status, C.IconUrl, C.DescriptionEN, c.DescriptionES, C.Register, C.CounterApp
                            FROM Application A
                            INNER JOIN Category C on C.Id = A.CategoryId
                            WHERE A.Status = 1 and a.PackageName = @PackageName";

            Application ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Application, Category, Application>(sql, (app, category) =>
                {
                    app.Category = category;
                    return app;
                }, new { PackageName = packageName }, splitOn: "Id, Id", commandType: CommandType.Text).SingleOrDefault();
                if (ret != null)
                {
                    var versionList = GetAppVersionListByAppId(new Guid(ret.Id));
                    foreach (var ver in versionList)
                    {
                        ver.InstalledCount = appuserActionList.Count(x =>
                    x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                    x.Type == ApplicationUserActionType.Install);

                        ver.ActiveCount = appuserActionList.Count(x =>
                            x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                            x.Type == ApplicationUserActionType.Active);

                        ver.DownloadCount = appuserActionList.Count(x =>
                            x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                            x.Type == ApplicationUserActionType.DownloadCompleted);
                    }

                    if (versionList.Count > 0)
                    {
                        ret.Versions.Add(versionList.SingleOrDefault(v => v.ApplicationId == ret.Id && v.Active));
                    }
                }
            }

            return ret;
        }

        public Application GetApplicationByIdPortal(string id)
        {
            var versionList = GetAppVersionListByAppIdPortal(new Guid(id));
            var appuserActionList = _applicationUserAction.GetAll();

            string sql = "SELECT " +
                         "A.Id, A.Name, A.LogoType, A.InclusionDate, A.CategoryId, A.DownloadedTimes, A.ActiveVersion, A.LastUpdate, A.Relevance, A.PackageName, A.InstalledTimes, A.UninstalledTimes, A.Description, A.DescriptionES, A.DescriptionEN, A.Status, A.RemoveFromCentral, A.Type," +
                         "C.Id, C.Description, C.Status, C.IconUrl, C.DescriptionEN, c.DescriptionES, C.Register, C.CounterApp " +
                         "FROM Application A " +
                         "INNER JOIN Category C on C.Id = A.CategoryId " +
                         "WHERE A.Status = 1 AND A.Id = @ApplicationId";

            Application ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Application, Category, Application>(sql, (app, category) =>
                {
                    app.Category = category;
                    return app;
                }, new { ApplicationId = id }, splitOn: "Id, Id", commandType: CommandType.Text).Single();

                ret.DownloadedTimes = appuserActionList.Count(x =>
                    x.ApplicationId == ret.Id &&
                    x.Type == ApplicationUserActionType.DownloadCompleted);

                ret.InstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == ret.Id &&
                        x.Type == ApplicationUserActionType.Install);

                ret.UninstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == ret.Id &&
                        x.Type == ApplicationUserActionType.Uninstall);
                foreach (var ver in versionList)
                {
                    ver.InstalledCount = appuserActionList.Count(x =>
                x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                x.Type == ApplicationUserActionType.Install);
                    ver.ActiveCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.Active);
                    ver.DownloadCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);
                }

                if (versionList.Count > 0)
                {
                    var versions = versionList.Where(v => v.ApplicationId == ret.Id).ToList();
                    foreach (var item in versions)
                    {
                        ret.Versions.Add(item);
                    }
                }
            }

            return ret;
        }

        public List<Application> GetApplicationsActiveToCentral(string token)
        {
            List<Application> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_ListActiveApps";

                ret = db.Query<Application, AppVersion, Application>(sql, (App, AppVersion) =>
                {
                    App.Versions.Add(AppVersion);
                    return App;
                }, new { Token = token }, splitOn: "Id", commandType: CommandType.StoredProcedure).ToList();
            }

            return ret;
        }

        public List<Application> GetApplicationsToRemove(string token)
        {
            var sql = string.Empty;
            List<Application> ret;

            using (var db = new SqlConnection(connstring))
            {
                sql = @"select app.id, app.name,  app.status, app.logotype, app.inclusionDate, app.CategoryId, (select count(Id) from ApplicationUserAction where ApplicationId = app.id) as downloadedTimes, (select VersionCode from AppVersion where id = appu.AppVersionID) as activeVersion, lastupdate, relevance, packagename,  (select count(id) from ApplicationUserAction where ApplicationId = app.Id and type = 3 ) as InstalledTimes,  (select count(id) from ApplicationUserAction where ApplicationId = app.Id and type = 4 ) as UninstalledTimes, app.Description, app.DescriptionEN, app.DescriptionES, A.Type,
		                ver.id, ver.Active, ver.ApkSize, ver.ApkUri, ver.ApplicationId, ver.Downgrade, ver.DowngradeJustify, ver.Package, ver.ReleaseDate, ver.ReleaseNote, ver.releaseNoteEN, ver.releasenoteES
                        from application app
                        inner join applicationuseraction appU on app.id = appU.applicationid
                        inner join appversion ver on appU.AppVersionID = ver.id
                        inner join Car car on car.userid = appU.userid
                        where appU.type = 3 and app.RemoveFromCentral = 1 and app.type = 1
                        and car.token = @Token
                        order by relevance desc , InclusionDate desc ";

                ret = db.Query<Application, AppVersion, Application>(sql, (App, AppVersion) =>
                {
                    App.Versions.Add(AppVersion);
                    return App;
                }, new { Token = token }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<Application> GetApplicationsToUpdateVersion(string token)
        {
            var versionList = GetAppVersionList();
            var appuserActionList = _applicationUserAction.GetAll();

            List<Application> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_ListAppsToUpdate";

                ret = db.Query<Application, AppVersion, Application>(sql, (App, AppVersion) =>
                {
                    App.Versions.Add(AppVersion);
                    return App;
                }, new { Token = token }, splitOn: "Id", commandType: CommandType.StoredProcedure).ToList();

                foreach (var app in ret)
                {
                    app.DownloadedTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);

                    app.InstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.Install);

                    app.UninstalledTimes = appuserActionList.Count(x =>
                        x.ApplicationId == app.Id &&
                        x.Type == ApplicationUserActionType.Uninstall);
                }

                foreach (var ver in versionList)
                {
                    ver.InstalledCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.Install);

                    ver.ActiveCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.Active);

                    ver.DownloadCount = appuserActionList.Count(x =>
                        x.ApplicationId == ver.ApplicationId && x.AppVersionId == ver.Id &&
                        x.Type == ApplicationUserActionType.DownloadCompleted);
                }
            }

            return ret;
        }

        public List<Application> GetByCategory(string CategoryId)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string id, bool removeFromCentral)
        {
            throw new NotImplementedException();
        }

        public List<Application> SearchByCategory(string CategoryDescription)
        {
            throw new NotImplementedException();
        }

        public bool Update(Application App)
        {
            throw new NotImplementedException();
        }
    }
}