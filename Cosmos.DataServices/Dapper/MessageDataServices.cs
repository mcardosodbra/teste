﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Model.GeoAPIOpenStreetMap;
using Cosmos.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cosmos.DataServices.Dapper
{
    public class MessageDataServices : BaseDapperDataServices, IMessageDataService
    {
        private ICar _car;

        public MessageDataServices(ICar car)
        {
            _car = car;
        }

        public List<Message> GetMessageList()
        {
            var textList = GetMessageTextList();
            // Select
            List<Message> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_GetMessageList";

                ret = db.Query<Message>(sql, new { BrandId = UserSession.User.BrandId },
                    commandType: CommandType.StoredProcedure).ToList();
            }

            foreach (var message in ret)
            {
                message.MessageText = textList.Where(x => x.MessageId == message.Id).ToList();
            }

            return ret;
        }

        public List<Message> GetMessageDetail(Guid messageId)
        {
            var textList = GetMessageTextList(messageId);
            // Select
            List<Message> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_GetMessageDetail";

                ret = db.Query<Message>(sql, new[]
                {
                    typeof(Message),
                    typeof(User)
                },
                    objects =>
                    {
                        var message = objects[0] as Message;
                        var userCreator = objects[1] as User;
                        message.Creator = userCreator;
                        message.MessageText = textList.Where(x => x.MessageId == message.Id).ToList();
                        return message;
                    }, new { MessageId = messageId },
                    splitOn: "Id",
                    commandType: CommandType.StoredProcedure).ToList();
            }
            return ret;
        }

        public List<MessageText> GetMessageTextList(Guid messageId)
        {
            List<MessageText> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT MT.Id, MT.LanguageId, MT.Title, MT.Text, MT.Banner, MT.MessageId,
                                    L.Id, L.Name
                                    FROM MessageText MT
                                    INNER JOIN [Message] M on M.Id = MT.MessageId
                                    INNER JOIN [Language] L on L.Id = MT.LanguageId
                                    WHERE MT.MessageId = @MessageId
                                    order by MT.Id";

                ret = db.Query<MessageText, Language, MessageText>(sql, (messagetext, language) =>
                {
                    messagetext.Language = language;
                    return messagetext;
                }, new { MessageId = messageId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<MessageText> GetMessageTextList()
        {
            List<MessageText> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT MT.Id, MT.LanguageId, MT.Title, MT.Text, MT.Banner, MT.MessageId,
                                    L.Id, L.Name
                                    FROM MessageText MT
                                    INNER JOIN [Message] M on M.Id = MT.MessageId
                                    INNER JOIN [Language] L on L.Id = MT.LanguageId
                                    WHERE M.BrandIdDestiny = @BrandId
                                    order by MT.Id";

                ret = db.Query<MessageText, Language, MessageText>(sql, (messagetext, language) =>
                {
                    messagetext.Language = language;
                    return messagetext;
                }, new { BrandId = UserSession.User.BrandId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<MessageText> GetMessageTextList(Guid brandId, Language lang)
        {
            List<MessageText> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT MT.Id, MT.LanguageId, MT.Title, MT.Text, MT.Banner, MT.MessageId,
                                    L.Id, L.Name
                                    FROM MessageText MT
                                    INNER JOIN [Message] M on M.Id = MT.MessageId
                                    INNER JOIN [Language] L on L.Id = MT.LanguageId
                                    WHERE M.BrandIdDestiny = @BrandId AND MT.LanguageId = @LanguageId
                                    order by MT.Id";

                ret = db.Query<MessageText, Language, MessageText>(sql, (messagetext, language) =>
                {
                    messagetext.Language = language;
                    return messagetext;
                }, new { BrandId = brandId, LanguageId = lang.Id }, commandType: CommandType.Text).ToList();
            }

            return ret;
        }

        public int ApproveMessage(Guid id, Guid approverId)
        {
            var param = new DynamicParameters();
            param.Add("MessageId", id);
            param.Add("ApproverId", approverId);
            param.Add("Status", dbType: DbType.Int32, direction: ParameterDirection.Output);

            int status;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_ApproveMessage";

                db.Execute(sql, param, commandType: CommandType.StoredProcedure);
                status = param.Get<int>("Status");
            }
            return status;
        }

        public void ReproveMessage(Guid id, Guid approverId, string reason)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_ReproveMessage";

                db.Execute(sql, new { MessageId = id, ApproverId = approverId, Reason = reason }, commandType: CommandType.StoredProcedure);
            }
        }

        public List<Message> GetMessage(string token, string region, string state, string city, Guid brandId, Language language)
        {
            var msgList = GetMessageTextList(brandId, language);
            var user = _car.GetUserByToken(token);
            var vehicle = _car.GetVehicleByToken(token);

            List<Message> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_GetMessage";
                ret = db.Query<Message>(sql, new[]
                    {
                        typeof(Message)
                    },
                    objects =>
                    {
                        var message = objects[0] as Message;
                        message.MessageText = msgList.Where(x => x.MessageId == message.Id).ToList();
                        return message;
                    }, new { Token = token, State = state, City = city }, commandType: CommandType.StoredProcedure).ToList();
            }

            foreach (var message in ret)
            {
                foreach (var messagetext in message.MessageText)
                {
                    messagetext.Text = messagetext.Text.Replace(MessageTags.Email, user.Email);
                    messagetext.Text = messagetext.Text.Replace(MessageTags.FirstName, user.Name);
                    messagetext.Text = messagetext.Text.Replace(MessageTags.Phone, user.Telephone);
                    messagetext.Text = messagetext.Text.Replace(MessageTags.Vehicle, vehicle.Name);
                    messagetext.Text = messagetext.Text.Replace(MessageTags.State, state);
                    messagetext.Text = messagetext.Text.Replace(MessageTags.City, city);
                }
            }

            return ret;
        }

        public void InsertMessage(Message message)
        {
            try
            {
                using (var db = new SqlConnection(connstring))
                {
                    // Inserindo Mensagem
                    const string sql = @"SP_InsertMessage";
                    db.Open();

                    db.Execute(sql, new
                    {
                        Id = message.Id,
                        ExpireDate = message.ExpireDate,
                        ScheduledDate = message.ScheduledDate,
                        MessageType = message.MessageTypeId,
                        MessageStatus = (int)MessageStatus.WaitingApproval,
                        UserId = message.UserId,
                        BrandIdDestiny = message.BrandIdDestiny,
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        CheckAllCities = message.CheckAllCities
                    }, commandType: CommandType.StoredProcedure);

                    // Inserindo Aprovadores
                    const string sqlApprovers = @"INSERT INTO [MessageApprover] (Id, ApproverId, MessageId, Approved) VALUES (@Id, @ApproverId, @MessageId, @Approved)";

                    foreach (var approver in message.Approver)
                    {
                        db.Execute(sqlApprovers, new
                        {
                            Id = Guid.NewGuid(),
                            ApproverId = approver.Id,
                            Approved = false,
                            MessageId = message.Id
                        }, commandType: CommandType.Text);
                    }

                    // Inserindo texto das mensagens
                    const string sqlText = @"INSERT INTO [MessageText] (Id, LanguageId, Title, Text, Banner, MessageId) VALUES (@Id, @LanguageId, @Title, @Text, @Banner, @MessageId)";

                    foreach (var messagetext in message.MessageText)
                    {
                        db.Execute(sqlText, new
                        {
                            Id = Guid.NewGuid(),
                            LanguageId = messagetext.Language.Id,
                            Title = messagetext.Title,
                            Text = messagetext.Text,
                            Banner = messagetext.Banner,
                            MessageId = message.Id
                        }, commandType: CommandType.Text);
                    }

                    //// Inserindo Veículo Destino
                    //if (message.VehicleIdDestiny.Length > 0)
                    //{
                    //    string sqlVehicle = @"INSERT INTO [MessageVehicle] (MessageId, VehicleId) VALUES (@MessageId, @VehicleId)";

                    //    foreach (var vehicle in message.VehicleIdDestiny)
                    //    {
                    //        db.Execute(sqlVehicle, new
                    //        {
                    //            VehicleId = vehicle,
                    //            MessageId = message.Id
                    //        }, commandType: CommandType.Text);
                    //    }
                    //}

                    // Inserindo SO Destino
                    if (message.SOIdDestiny.Count > 0)
                    {
                        string sqlSO = @"INSERT INTO [MessageSO] (MessageId, SOId) VALUES (@MessageId, @SoId)";

                        foreach (var so in message.SOIdDestiny)
                        {
                            db.Execute(sqlSO, new
                            {
                                SoId = so.Id,
                                MessageId = message.Id
                            }, commandType: CommandType.Text);
                        }
                    }

                    // Inserindo Regiões Destino
                    if (message.RegionIdDestiny.Length > 0)
                    {
                        const string sqlRegion = @"INSERT INTO [MessageRegion] (MessageId, RegionId) VALUES (@MessageId, @RegionId)";

                        foreach (var region in message.RegionIdDestiny)
                        {
                            db.Execute(sqlRegion, new
                            {
                                RegionId = region,
                                MessageId = message.Id
                            }, commandType: CommandType.Text);
                        }
                    }

                    // Inserindo Estado Destino
                    if (message.StateIdDestiny.Length > 0)
                    {
                        const string sqlState = @"INSERT INTO [MessageState] (MessageId, StateId) VALUES (@MessageId, @StateId)";

                        foreach (var state in message.StateIdDestiny)
                        {
                            db.Execute(sqlState, new
                            {
                                StateId = state,
                                MessageId = message.Id
                            }, commandType: CommandType.Text);
                        }
                    }

                    // Inserindo Cidade Destino
                    if (message.CityIdDestiny.Length > 0)
                    {
                        if (!message.CheckAllCities)
                        {
                            using (SqlBulkCopy copy = new SqlBulkCopy(connstring))
                            {
                                var dt = new DataTable("messagecity");
                                dt.Columns.Add("MessageId", typeof(Guid));
                                dt.Columns.Add("CityId", typeof(Guid));

                                for (int i = 0; i < message.CityIdDestiny.Length; i++)
                                {
                                    dt.Rows.Add(message.Id, message.CityIdDestiny[i]);
                                }

                                copy.DestinationTableName = "MessageCity";
                                copy.WriteToServer(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public void UpdateMessage(Message message)
        {
            // Update
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [Message] SET Title = @Title, ExpireDate = @ExpireDate, ScheduledDate = @ScheduledDate, Text = @Text, Banner = @Banner WHERE Title = @Title";

                db.Execute(sql, new { ExpireDate = message.ExpireDate, ScheduledDate = message.ScheduledDate }, commandType: CommandType.Text);
            }
        }

        public void DeleteMessage(Message message)
        {
            // Delete
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"DELETE FROM [Message] WHERE Id = @Id";

                db.Execute(sql, new { message.Id }, commandType: CommandType.Text);
            }
        }

        public async Task MessageRead(Guid userId, Guid messageid, string token)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_ReadMessageHistory";

                await db.ExecuteAsync(sql, new { Id = Guid.NewGuid(), MessageId = messageid, UserId = userId, Token = token }, commandType: CommandType.StoredProcedure);
            }
        }

        public void ChangeStatus(Guid id, int status)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [Message] SET MessageStatus = @Status, UpdatedAt = GETUTCDATE() WHERE Id = @Id";

                db.Execute(sql, new { Id = id, Status = status }, commandType: CommandType.Text);
            }
        }

        public Message GetMessageById(Guid id)
        {
            Message ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT M.Id, M.ExpireDate, M.ScheduledDate, M.MessageType, M.MessageStatus, M.UserId, M.UserIdDestiny, M.BrandIdDestiny, M.RegionIdDestiny, M.CreatedAt, M.UpdatedAt, M.ReprovedReason,
                                    R.Id, R.Name,
                                    B.Id, B.Name,
                                    U.Id, U.Name,
                                    U2.Id, U2.Name, U2.Email
                                    FROM [Message] M
                                    LEFT JOIN Region R on R.Id = M.RegionIdDestiny
                                    LEFT JOIN Brand B on B.Id = M.BrandIdDestiny
                                    LEFT JOIN [User] U on U.Id = M.UserIdDestiny
                                    LEFT JOIN [User] U2 on U2.Id = M.UserId WHERE M.Id = @Id";

                ret = db.Query<Message>(sql, new[]
                    {
                        typeof(Message),
                        typeof(Region),
                        typeof(Brand),
                        typeof(User),
                        typeof(User)
                    },
                    objects =>
                    {
                        var message = objects[0] as Message;
                        var region = objects[1] as Region;
                        var brand = objects[2] as Brand;
                        var user = objects[3] as User;
                        var userCreator = objects[4] as User;
                        message.RegionDestiny = region;
                        message.BrandDestiny = brand;
                        message.UserDestiny = user;
                        message.Creator = userCreator;

                        return message;
                    }, new { Id = id },
                    splitOn: "Id, Id, Id, Id",
                    commandType: CommandType.Text).SingleOrDefault();
            }
            return ret;
        }
    }
}