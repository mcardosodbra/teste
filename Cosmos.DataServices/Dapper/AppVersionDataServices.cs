﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;

namespace Cosmos.DataServices.Dapper
{
    public class AppVersionDataServices : BaseDapperDataServices, IAppVersionDataServices
    {
        public bool Update(AppVersion appVersion)
        {
            throw new NotImplementedException();
        }

        public List<AppVersion> GetVersionsByApplicationId(string id)
        {
            throw new NotImplementedException();
        }

        public AppVersion GetAppVersionById(string id)
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public bool Active(string id)
        {
            throw new NotImplementedException();
        }

        public bool IsVersionInUseForApp(string version, string app)
        {
            throw new NotImplementedException();
        }

        public void InsertVersion(Guid versionId, Guid applicationId, int versionCode, string packageName)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO AppVersion (Id, ApplicationId, Version, Package, Status, VersionCode, Active, ApkSize, ReleaseDate) VALUES (@Id, @ApplicationId, @Version, @Package, @Status, @VersionCode, @Active, @ApkSize, GETDATE())";

                db.Execute(sql, new { Id = versionId, ApplicationId = applicationId, Version = "AutomaticGenerated", Package = packageName, Status = 0, VersionCode = versionCode, Active = 0, ApkSize = 0 }, commandType: CommandType.Text);
            }
        }
    }
}