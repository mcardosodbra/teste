﻿using System;
using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public partial class UserDataServices : BaseDapperDataServices, IUser
    {
        public User GetUserByEmail(string Email)
        {
            throw new System.NotImplementedException();
        }

        public bool AddUser(User User)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [User] (Id, Name, LastName, Telephone, TypeId, BrandId, Email, Password, InclusionDate, Status)
                VALUES (@Id, @Name, @LastName, @Telephone, @TypeId, @BrandId, @Email, @Password, GETUTCDATE(), @Status)";

                db.Execute(sql, new { Id = User.Id, Name = User.Name, LastName = User.LastName, Email = User.Email, BrandId = User.BrandId, Telephone = User.Telephone, TypeId = User.TypeId, Password = User.Password, Status = User.Status }, commandType: CommandType.Text);
            }
            return true;
        }

        public bool UpdateUser(User User)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [User] SET Email = @Email, BrandId = @BrandId, Name = @Name, Telephone = @Telephone, TypeId = @TypeId, LastName = @LastName WHERE Id = @UserId";

                db.Execute(sql, new { UserId = User.Id, Email = User.Email, BrandId = User.BrandId, Name = User.Name, LastName = User.LastName, Telephone = User.Telephone, TypeId = User.TypeId }, commandType: CommandType.Text);
            }
            return true;
        }

        public List<User> GetUserAll()
        {
            throw new System.NotImplementedException();
        }

        public User GetUserById(string id)
        {
            throw new System.NotImplementedException();
        }

        public void GetUserByVehicle()
        {
            throw new System.NotImplementedException();
        }

        public User GetUserByType(string id)
        {
            throw new System.NotImplementedException();
        }

        public bool Active(string id)
        {
            throw new System.NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new System.NotImplementedException();
        }

        public List<Permission> GetPermissionList()
        {
            List<Permission> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name, Description, Type as PermissionType FROM [Permission] ORDER BY Type, Name";

                ret = db.Query<Permission>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<Permission> GetPermissionListByUser(Guid userId)
        {
            List<Permission> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name, Description, Type FROM [Permission] P
                                    INNER JOIN UserPermission UP on UP.PermissionId = P.Id
                                    WHERE UP.UserId = @UserId
                                    ORDER BY Type, Name";

                ret = db.Query<Permission>(sql, new { UserId = userId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public void UpdatePermissions(Guid userId, string permissions)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SP_UpdateUserPermissions";

                db.Execute(sql, new { UserId = userId, Permissions = permissions }, commandType: CommandType.StoredProcedure);
            }
        }
    }
}