﻿using System.Configuration;

namespace Cosmos.DataServices.Dapper
{
    public abstract class BaseDapperDataServices
    {
        public string connstring
        {
            get
            {
                var connectionString = ConfigurationManager.ConnectionStrings["dbMultiAppEntities"].ConnectionString;
                if (connectionString.ToLower().StartsWith("metadata="))
                {
                    System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder efBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(connectionString);
                    connectionString = efBuilder.ProviderConnectionString;
                }

                return connectionString;
            }
        }
    }
}