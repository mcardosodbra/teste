﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosmos.DataServices.Dapper
{
    public class ApplicationUserActionErrorDataServices : BaseDapperDataServices, IApplicationUserActionErrorDataServices
    {
        public void insert(Model.ApplicationUserActionError objError)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [dbo].[ApplicationUserActionError] ([Id] ,[ErrorCode] ,[Message] ,[Payload] ,[Origem] ,[UserId] ,[Type])
                                    VALUES  (@Id ,@ErrorCode ,@Message ,@Payload ,@Origem ,@UserId ,@Type)";
                db.Execute(sql, new { Id = new Guid(), ErrorCode = objError.ErrorCode, Message = objError.Message, Payload = objError.Payload, Origem = objError.Origem, UserId = objError.UserId, Type = objError.Type }, commandType: CommandType.Text);
            }
        }
    }
}