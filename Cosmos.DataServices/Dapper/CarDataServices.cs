﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using Cosmos.Utils;
using Cosmos.Model.GeoAPIOpenStreetMap;

namespace Cosmos.DataServices.Dapper
{
    public class CarDataServices : BaseDapperDataServices, ICar
    {
        public bool AddCar(Car Car)
        {
            throw new NotImplementedException();
        }

        public bool UpdateCar(Car Car)
        {
            throw new NotImplementedException();
        }

        public Car GetCarsById(string id)
        {
            throw new NotImplementedException();
        }

        public List<Car> GetAllCars()
        {
            throw new NotImplementedException();
        }

        public List<Car> GetAllCarsByYUser(string userId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public bool TokenValid(string token)
        {
            throw new NotImplementedException();
        }

        public bool Logout(string token)
        {
            throw new NotImplementedException();
        }

        public Guid GetUserIdByToken(string token)
        {
            throw new NotImplementedException();
        }

        public Guid GetBrandIdbyToken(string token)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateLocation(Guid carId, List<CarLocation> carLocations)
        {
            using (var copy = new SqlBulkCopy(connstring))
            {
                var dt = new DataTable("CarLocationHistory");
                dt.Columns.Add("DtRegister", typeof(DateTime));
                dt.Columns.Add("CarId", typeof(Guid));
                dt.Columns.Add("Latitude", typeof(string));
                dt.Columns.Add("Longitude", typeof(string));
                dt.Columns.Add("Speed", typeof(double));
                dt.Columns.Add("Accuracy", typeof(double));

                foreach (var location in carLocations)
                {
                    dt.Rows.Add(location.DtRegister, carId, location.Latitude, location.Longitude,
                        location.Speed, location.Accuracy);
                }

                copy.DestinationTableName = "CarLocationHistory";
                await copy.WriteToServerAsync(dt);
            }
        }

        public User GetUserByToken(string token)
        {
            User ret;
            var sql = @"SELECT U.[Name], U.Telephone, U.Email from [User] U
		                INNER JOIN Car C on C.UserId = U.Id
                        WHERE C.token = @token";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<User>(sql, new { token = token }, commandType: CommandType.Text).FirstOrDefault();
            }
            return ret;
        }

        public Vehicle GetVehicleByToken(string token)
        {
            Vehicle ret;
            var sql = @"select V.Id, V.[Name], B.Id, B.[Name] FROM Vehicle V
                        INNER JOIN SO S on S.VehicleId = V.Id
                        INNER JOIN Car C on C.SOID = S.Id
                        INNER JOIN Brand B on B.Id = V.BrandId
                        where C.Token = @token";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Vehicle, Brand, Vehicle>(sql, (vehicle, brand) =>
                {
                    vehicle.Brand = brand;
                    return vehicle;
                }, new { token = token }, commandType: CommandType.Text).FirstOrDefault();
            }
            return ret;
        }

        public GeoCoordinate GetCarLastCoordinate(Guid carId)
        {
            GeoCoordinate ret;
            var sql = @"SELECT TOP 1 DtRegister, CarId, Longitude, Latitude FROM CarLocationHistory
                        ORDER BY DtRegister DESC";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<GeoCoordinate>(sql, new { carId = carId }, commandType: CommandType.Text).FirstOrDefault();
            }
            return ret;
        }

        public List<CarLocation> GetLocationList()
        {
            List<CarLocation> ret;
            var sql = @"SELECT TOP 10000 DtRegister, CarId, Latitude, Longitude, Speed, Accuracy FROM CarLocationHistory
                        ORDER By DtRegister DESC";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<CarLocation>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public Car GetCarsByToken(string token)
        {
            Car ret;
            var sql = @"SELECT [Id] ,[UserId] ,[VIN] ,[Status] ,[UUID] ,[CAN] ,[MCU] ,[SO] ,[LIcensePlate] ,[Token] ,[LastMessageRequest] ,[SOID], DtLastLocationUpdate
                        FROM [dbo].[Car] WHERE token = @token";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Car>(sql, new { token = token }, commandType: CommandType.Text).FirstOrDefault();
            }
            return ret;
        }

        public Task UpdateCurrentCarLocation(Guid carId, LocationStreetMap location)
        {
            var sql = "SP_UpdateCarCurrentLocation";
            using (var db = new SqlConnection(connstring))
            {
                db.Query<GeoCoordinate>(sql, new { CarId = carId, State = location.address.state, City = location.address.city }, commandType: CommandType.StoredProcedure);
            }

            return Task.CompletedTask;
        }

        public CarCurrentLocation GetCarCurrentLocation(Guid carId)
        {
            CarCurrentLocation ret;
            var sql = "SELECT CL.CarId, Cl.RegionId, Cl.StateId, Cl.CityId, CL.DtLastUpdate, " +
                "C.Id, C.SOID,R.Id, R.[Name],S.Id, S.[Name], " +
                "CT.Id, CT.[Name] " +
                "FROM CarLocation CL " +
                "inner join Car C on C.Id = CL.CarId " +
                "inner join Region R on R.Id = CL.RegionId " +
                "inner join [State] S on S.Id = CL.StateId " +
                "inner join City CT on CT.Id = CL.CityId " +
                "WHERE CL.CarId = @CarId";

            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<CarCurrentLocation, Car, Region, State, City, CarCurrentLocation>(sql, (currentloc, car, region, state, city) =>
                {
                    currentloc.Car = car;
                    currentloc.City = city;
                    currentloc.State = state;
                    currentloc.Region = region;
                    return currentloc;
                }, new { CarId = carId }, commandType: CommandType.Text).SingleOrDefault();
            }
            return ret;
        }
    }
}