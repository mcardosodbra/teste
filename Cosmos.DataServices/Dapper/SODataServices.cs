﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class SODataServices : BaseDapperDataServices, ISODataServices
    {
        public List<SO> GetListSO()
        {
            List<SO> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT S.Id, S.YearModel, S.YearFab, S.Version, S.Status, S.ReleaseDate, B.Id, B.Name, V.Id, V.Name, V.Status FROM [SO] S
                                    LEFT JOIN Brand B on B.Id = S.BrandId
                                    LEFT JOIN Vehicle V on V.Id = S.VehicleId";

                ret = db.Query<SO, Brand, Vehicle, SO>(sql, (so, brand, vehicle) =>
                    {
                        so.Brand = brand;
                        so.Vehicle = vehicle;
                        return so;
                    },
                    splitOn: "Id, Id").ToList();
            }
            return ret;
        }

        public Guid InsertSO(SO so)
        {
            var soId = Guid.NewGuid();

            if (so.Vehicle == null) // Caso onde novo login onde não possui detalhes do SO
            {
                using (var db = new SqlConnection(connstring))
                {
                    const string sql = @"INSERT INTO [SO] (Id, Version, Status) VALUES (@Id, @Version, 1)";

                    db.Execute(sql, new { Version = so.Version, Id = soId, }, commandType: CommandType.Text);
                }
            }
            else
            {
                using (var db = new SqlConnection(connstring))
                {
                    const string sql =
                        @"INSERT INTO [SO] (Id, YearModel, YearFab, Version, BrandId, VehicleId, Status, ReleaseDate) VALUES (@Id, @YearModel, @YearFab, @Version, @BrandId, @VehicleId, 1, @ReleaseDate)";

                    db.Execute(sql,
                        new
                        {
                            YearModel = so.YearModel,
                            YearFab = so.YearFab,
                            Version = so.Version,
                            Id = soId,
                            BrandId = so.Vehicle.BrandId,
                            VehicleId = so.Vehicle.Id,
                            ReleaseDate = so.ReleaseDate
                        }, commandType: CommandType.Text);
                }
            }

            return soId;
        }

        public void UpdateSO(SO so)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [SO] SET YearModel = @YearModel, YearFab = @YearFab, Version = @Version, Status = @Status, ReleaseDate = @ReleaseDate, BrandId = @BrandId, VehicleId = @VehicleId WHERE Id = @Id";

                db.Execute(sql, new { YearModel = so.YearModel, YearFab = so.YearFab, Version = so.Version, Id = so.Id, Status = so.Status, ReleaseDate = so.ReleaseDate, VehicleId = so.Vehicle.Id, BrandId = so.Vehicle.BrandId }, commandType: CommandType.Text);
            }
        }

        public void DeleteSO(SO so)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"DELETE FROM [SO] WHERE Id = @Id";

                db.Execute(sql, new { Id = so.Id }, commandType: CommandType.Text);
            }
        }

        public SO GetSODetail(Guid SOId)
        {
            SO ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT S.Id, S.YearModel, S.YearFab, S.Version, S.Status, S.ReleaseDate, B.Id, B.Logotype, B.Name, V.Id, V.Name, V.Status FROM [SO] S
                                    LEFT JOIN Brand B on B.Id = S.BrandId
                                    LEFT JOIN Vehicle V on V.Id = S.VehicleId WHERE S.Id = @Id";

                ret = db.Query<SO, Brand, Vehicle, SO>(sql, (so, brand, vehicle) =>
                    {
                        so.Brand = brand;
                        so.Vehicle = vehicle;
                        return so;
                    }, param: new { Id = SOId },
                    splitOn: "Id, Id").SingleOrDefault();
            }
            return ret;
        }
    }
}