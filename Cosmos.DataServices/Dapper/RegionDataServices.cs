﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class RegionDataServices : BaseDapperDataServices, IRegionDataServices
    {
        public List<Region> SelectRegion()
        {
            // Select
            List<Region> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name FROM [Region] order by Name";

                ret = db.Query<Region>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public void InsertRegion(Region region)
        {
            // Insert
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [Region] (Name) VALUES (@Name)";

                db.Execute(sql, new { Name = region.Name }, commandType: CommandType.Text);
            }
        }

        public void UpdateRegion(Region region)
        {
            // Update
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [Region] SET Name = @Name WHERE Id = @ID";

                db.Execute(sql, new { Name = region.Name, region.Id }, commandType: CommandType.Text);
            }
        }

        public void DeleteRegion(Region region)
        {
            // Delete
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"DELETE FROM [Region] WHERE Id = @ID";

                db.Execute(sql, new { region.Name, region.Id }, commandType: CommandType.Text);
            }
        }
    }
}