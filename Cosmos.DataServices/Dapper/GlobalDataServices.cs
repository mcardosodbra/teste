﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class GlobalDataServices : BaseDapperDataServices, IGlobalDataServices
    {
        public List<Language> LanguageList()
        {
            List<Language> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name from Language order by Name";

                ret = db.Query<Language>(sql, new { }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<Permission> PermissionList()
        {
            List<Permission> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name from Permission order by Name";

                ret = db.Query<Permission>(sql, new { }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public void LogtoDb(string message, string endpointcall, LogType logType)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO SystemLog (Id, Message, Type, DtCreated, SystemId, EndPointCall) VALUES (NEWID(), @Message, @Type, GETUTCDATE(), 1, @EndPointCall)";

                db.Execute(sql, new { Message = message, Type = logType, EndPointCall = endpointcall }, commandType: CommandType.Text);
            }
        }
    }
}