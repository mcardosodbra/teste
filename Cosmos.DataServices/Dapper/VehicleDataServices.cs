﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cosmos.Utils;

namespace Cosmos.DataServices.Dapper
{
    public partial class VehicleDataServices : BaseDapperDataServices, IVehicle
    {
        public bool AddVehicle(Vehicle vehicle)
        {
            throw new NotImplementedException();
        }

        public bool UpdateVehicle(Vehicle vehicle)
        {
            throw new NotImplementedException();
        }

        public Vehicle GetVehicleById(string id)
        {
            throw new NotImplementedException();
        }

        public List<Vehicle> GetVehicleAll()
        {
            // Select
            List<Vehicle> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name FROM [Vehicle] WHERE BrandId = @BrandId ORDER BY Name";

                ret = db.Query<Vehicle>(sql, new { BrandId = UserSession.User.BrandId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public bool Active(string id)
        {
            throw new NotImplementedException();
        }

        public List<Vehicle> GetVehicleByBrand(Guid brandId)
        {
            List<Vehicle> ret;
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"SELECT Id, Name FROM [Vehicle] WHERE BrandId = @BrandId ORDER BY Name";

                ret = db.Query<Vehicle>(sql, new { BrandId = brandId }, commandType: CommandType.Text).ToList();
            }
            return ret;
        }
    }
}