﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosmos.DataServices.Dapper
{
    public class CategoryDataServices : BaseDapperDataServices, ICategory
    {
        public bool Active(string id)
        {
            throw new NotImplementedException();
        }

        public bool AddCategory(Category Category)
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public List<Category> GetAllCategory()
        {
            List<Category> ret;
            var applications = GetApplications();
            var action = GetApplicationsActions();
            var sql =
              @"SELECT [Id], [Description], [Status], [IconUrl], [DescriptionEN], [DescriptionES], [Register], [CounterApp] FROM [dbo].[Category]";
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Category>(sql, commandType: CommandType.Text).ToList();
            }
            foreach (var item in ret)
            {
                var apps = applications.Where(s => s.CategoryId == item.Id).ToList();
                foreach (var app in apps)
                {
                    item.CounterApp += action.Where(s => s.ApplicationId == app.Id).Count();
                }
            }
            return ret;
        }

        public Category GetCategoryById(string id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateCategory(Category Category)
        {
            throw new NotImplementedException();
        }

        private List<Application> GetApplications()
        {
            var sql =
                @"SELECT [Id], [Name], [Logotype], [InclusionDate], [CategoryId], [DownloadedTimes], [ActiveVersion], [LastUpdate], [Relevance], [PackageName], [InstalledTimes], [UninstalledTimes]
                    ,[Description], [DescriptionES], [DescriptionEN], [Status], [RemoveFromCentral], [Type] FROM [dbo].[Application] WHERE Status = 1";
            List<Application> ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<Application>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        private List<ApplicationUserAction> GetApplicationsActions()
        {
            var sql =
                @"SELECT [Id], [Date], [ApplicationId], [UserId], [Type], [AppVersionID], [Package] FROM [dbo].[ApplicationUserAction] where type = 3";
            List<ApplicationUserAction> ret;
            using (var db = new SqlConnection(connstring))
            {
                ret = db.Query<ApplicationUserAction>(sql, commandType: CommandType.Text).ToList();
            }
            return ret;
        }
    }
}