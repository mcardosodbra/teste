﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Model;
using Cosmos.Utils;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices.Dapper
{
    public class ApproverDataServices : BaseDapperDataServices, IApproverDataServices
    {
        public List<Approver> SelectApprover(Guid brandId)
        {
            string sql = string.Empty;

            // Select
            List<Approver> ret;
            using (var db = new SqlConnection(connstring))
            {
                sql = @"SELECT A.Id, A.Name, A.Email, A.Position, A.Sector, A.Status, A.Approved, A.ApproverType, B.Id, B.Name
                                    FROM [Approver] A
                                    INNER JOIN Brand B on B.Id = A.BrandId WHERE BrandId = @BrandId order by A.Name";

                ret = db.Query<Approver, Brand, Approver>(sql, (approver, brand) =>
                {
                    approver.Brand = brand;
                    return approver;
                }, new { BrandId = brandId }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<Approver> SelectApprover(Guid brandId, bool approved)
        {
            string sql = string.Empty;

            // Select
            List<Approver> ret;
            using (var db = new SqlConnection(connstring))
            {
                sql = @"SELECT A.Id, A.Name, A.Email, A.Position, A.Sector, A.Status, A.Approved, A.ApproverType, B.Id, B.Name
                                    FROM [Approver] A
                                    INNER JOIN Brand B on B.Id = A.BrandId WHERE BrandId = @BrandId AND A.Approved = @Approved order by A.Name";

                ret = db.Query<Approver, Brand, Approver>(sql, (approver, brand) =>
                {
                    approver.Brand = brand;
                    return approver;
                }, new { BrandId = brandId, Approved = approved }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<Approver> SelectApprover(Guid brandId, bool approved, ApproverType approverType)
        {
            string sql = string.Empty;

            // Select
            List<Approver> ret;
            using (var db = new SqlConnection(connstring))
            {
                if (approved)
                    sql = @"SELECT A.Id, A.Email, A.Position, A.Sector, A.Status, A.Approved, A.ApproverType, A.Name + ' - ' + A.Position + ' - ' + A.Sector as Name, B.Id, B.Name
                                    FROM [Approver] A
                                    INNER JOIN Brand B on B.Id = A.BrandId WHERE BrandId = @BrandId and Approved = 1 AND ApproverType = @ApproverType order by A.Name";
                else
                    sql = @"SELECT A.Id, A.Name, A.Email, A.Position, A.Sector, A.Status, A.Approved, A.ApproverType, B.Id, B.Name
                                    FROM [Approver] A
                                    INNER JOIN Brand B on B.Id = A.BrandId WHERE BrandId = @BrandId AND ApproverType = @ApproverType order by A.Name";

                ret = db.Query<Approver, Brand, Approver>(sql, (approver, brand) =>
                {
                    approver.Brand = brand;
                    return approver;
                }, new { BrandId = brandId, ApproverType = (int)approverType }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public List<Approver> SelectApprover(Guid brandId, bool approved, Guid messageId)
        {
            string sql = string.Empty;

            // Select
            List<Approver> ret;
            using (var db = new SqlConnection(connstring))
            {
                sql = @"SELECT A.Id, A.Name, A.Email, A.Position, A.Sector, A.Status, A.Approved, A.ApproverType, B.Id, B.Name
                                    FROM [Approver] A
                                    INNER JOIN Brand B on B.Id = A.BrandId
                                    INNER JOIN MessageApprover MA on MA.ApproverId = A.Id
                                    WHERE BrandId = @BrandId AND A.Approved = 1 and MA.MessageId = @MessageId
                                    order by A.Name";

                ret = db.Query<Approver, Brand, Approver>(sql, (approver, brand) =>
                {
                    approver.Brand = brand;
                    return approver;
                }, new { BrandId = brandId, MessageId = messageId }, splitOn: "Id", commandType: CommandType.Text).ToList();
            }
            return ret;
        }

        public Guid InsertApprover(Approver approver)
        {
            var guid = Guid.NewGuid();
            // Insert
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"INSERT INTO [Approver] (Id, Name, Email, Position, Sector, Status, Approved, BrandId, ApproverType) VALUES (@Id, @Name, @Email, @Position, @Sector, @Status, @Approved, @BrandId, @ApproverType)";

                db.Execute(sql, new { Name = approver.Name, Email = approver.Email, Position = approver.Position, Sector = approver.Sector, Status = 1, Approved = false, Id = guid, ApproverType = approver.ApproverTypeId, BrandId = UserSession.User.BrandId }, commandType: CommandType.Text);
            }

            return guid;
        }

        public void UpdateApprover(Approver approver)
        {
            // Update
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [Approver] SET Name = @Name, Email = @Email, Position = @Position, Sector = @Sector, ApproverType = @ApproverType WHERE Id = @Id";

                db.Execute(sql, new { Name = approver.Name, Email = approver.Email, Position = approver.Position, Sector = approver.Sector, Id = approver.Id, ApproverType = approver.ApproverType }, commandType: CommandType.Text);
            }
        }

        public void DeleteApprover(Approver approver)
        {
            // Delete
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"DELETE FROM [Approver] WHERE Id = @Id";

                db.Execute(sql, new { approver.Id }, commandType: CommandType.Text);
            }
        }

        public void ActivateApprover(Guid approver)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [Approver] SET Approved = 1 WHERE Id = @Id";

                db.Execute(sql, new { Id = approver }, commandType: CommandType.Text);
            }
        }

        public void DeActivateApprover(Guid approver)
        {
            using (var db = new SqlConnection(connstring))
            {
                const string sql = @"UPDATE [Approver] SET Approved = 0 WHERE Id = @Id";

                db.Execute(sql, new { Id = approver }, commandType: CommandType.Text);
            }
        }

        public Approver SelectApproverById(Guid approverId)
        {
            Approver ret;
            using (var db = new SqlConnection(connstring))
            {
                var sql = @"SELECT A.Id, A.Name, A.Email, A.Position, A.Sector, A.Status, A.Approved, A.ApproverType, B.Id, B.Name
                                    FROM [Approver] A
                                    INNER JOIN Brand B on B.Id = A.BrandId WHERE A.Id = @Id order by A.Name";

                ret = db.Query<Approver, Brand, Approver>(sql, (approver, brand) =>
                {
                    approver.Brand = brand;
                    return approver;
                }, new { Id = approverId }, splitOn: "Id", commandType: CommandType.Text).SingleOrDefault();
            }
            return ret;
        }
    }
}