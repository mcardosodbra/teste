﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class UserAppsDataServices : IUserApps
    {
        public void AddUserApp(Model.UserApp App)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.UserApp userApp = Converters.DoReflection<Model.UserApp, Entity.UserApp>(App, new Entity.UserApp());
                dbContext.UserApp.Add(userApp);
                dbContext.SaveChanges();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.UserApp> GetAllUserApps()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityUserApps = dbContext.UserApp.ToList();
                List<Model.UserApp> listUserApps = new List<Model.UserApp>();

                foreach (Entity.UserApp type in entityUserApps)
                    listUserApps.Add(Converters.DoReflection(type, new Model.UserApp()));

                return listUserApps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.UserApp GetByUserAppId(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                UserApp entityUserApp = dbContext.UserApp.Where(p => p.Id == id).SingleOrDefault();
                Model.UserApp userApp = Converters.DoReflection<Entity.UserApp, Model.UserApp>(entityUserApp, new Model.UserApp());

                return userApp;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.UserApp> GetByUserId(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                dbContext = new dbMultiAppEntities();

                var entityUserApp = dbContext.UserApp.Where(p => p.UserId == id).ToList();

                // revisar, não retorna entity, apenas model

                return null;
                //return entityUserApp;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public void UpdateUserApp(Model.UserApp App)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                UserApp UserAppOriginal = dbContext.UserApp.Where(p => p.Id == App.Id).FirstOrDefault();
                UserAppOriginal = Converters.DoReflection<Model.UserApp, Entity.UserApp>(App, UserAppOriginal);
                dbContext.SaveChanges();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.UserApp entity_userApp = dbContext.UserApp.Where(u => u.Id == id).FirstOrDefault();

                if (entity_userApp != null)
                {
                    entity_userApp.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.UserApp entity_userApp = dbContext.UserApp.Where(u => u.Id == id).FirstOrDefault();

                if (entity_userApp != null)
                {
                    entity_userApp.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}