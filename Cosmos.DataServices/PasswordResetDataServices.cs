﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Linq;
using PasswordReset = Cosmos.Model.PasswordReset;

namespace Cosmos.DataServices
{
    public class PasswordResetDataServices : IPasswordReset
    {
        public PasswordReset GetByTokenByUserId(string userId)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.PasswordReset entityPasswordReset =
                    dbContext.PasswordReset.SingleOrDefault(s => (s.UserId == userId) && (s.Status == true));
                Model.PasswordReset documentType =
                    Converters.DoReflection<Entity.PasswordReset, Model.PasswordReset>(entityPasswordReset,
                        new Model.PasswordReset());

                return documentType;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool InsertToken(PasswordReset objToken)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.PasswordReset password = Converters.DoReflection<Model.PasswordReset, Entity.PasswordReset>(objToken, new Entity.PasswordReset());
                dbContext.PasswordReset.Add(password);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateTokenStatus(Model.PasswordReset Objtoken)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.PasswordReset UserOriginal = dbContext.PasswordReset.FirstOrDefault(p => p.Id == Objtoken.Id);

                UserOriginal = Converters.DoReflection<Model.PasswordReset, Entity.PasswordReset>(Objtoken, UserOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.PasswordReset GetByToken(string token)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.PasswordReset entityPasswordReset =
                    dbContext.PasswordReset.SingleOrDefault(s => (s.Token == token) && (s.Status == true));
                Model.PasswordReset documentType =
                    Converters.DoReflection<Entity.PasswordReset, Model.PasswordReset>(entityPasswordReset,
                        new Model.PasswordReset());

                return documentType;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}