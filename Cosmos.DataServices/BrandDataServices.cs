﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class BrandDataServices : IBrand
    {
        public bool AddBrand(Model.Brand Brand)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Brand brand = Converters.DoReflection<Model.Brand, Entity.Brand>(Brand, new Entity.Brand());
                dbContext.Brand.Add(brand);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Brand> GetAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityBrand = dbContext.Brand.ToList();
                List<Model.Brand> listBrand = new List<Model.Brand>();

                foreach (Entity.Brand brand in entityBrand)
                    listBrand.Add(Converters.DoReflection(brand, new Model.Brand()));

                return listBrand;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Brand GetBrandById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Brand entityBrand = dbContext.Brand.Where(p => p.Id == id).SingleOrDefault();
                Model.Brand brand = Converters.DoReflection<Entity.Brand, Model.Brand>(entityBrand, new Model.Brand());

                return brand;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateBrand(Model.Brand Brand)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Brand BrandOriginal = dbContext.Brand.Where(p => p.Id == Brand.Id).FirstOrDefault();
                BrandOriginal = Converters.DoReflection<Model.Brand, Entity.Brand>(Brand, BrandOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Brand entity_brand = dbContext.Brand.Where(u => u.Id == id).FirstOrDefault();

                if (entity_brand != null)
                {
                    entity_brand.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Brand entity_brand = dbContext.Brand.Where(u => u.Id == id).FirstOrDefault();

                if (entity_brand != null)
                {
                    entity_brand.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsBrandNameInUse(string Name)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.Brand.Where(e => e.Name.Equals(Name)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}