﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Cosmos.Model;
using Cosmos.Utils;
using Car = Cosmos.Entity.Car;
using Vehicle = Cosmos.Model.Vehicle;
using Cosmos.Model.GeoAPIOpenStreetMap;

namespace Cosmos.DataServices
{
    public class CarDataServices : ICar
    {
        public bool AddCar(Model.Car Car)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Car car = Converters.DoReflection<Model.Car, Entity.Car>(Car, new Entity.Car());

                car.Id = new Guid().ToString();
                dbContext.Car.Add(car);
                int result = dbContext.SaveChanges();
                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateCar(Model.Car Car)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                if (Car.LastMessageRequest == DateTime.MinValue)
                    Car.LastMessageRequest = null;

                Entity.Car CarOriginal = dbContext.Car.FirstOrDefault(p => (p.UUID == Car.UUID) && (p.UserId == Car.UserId));

                if (CarOriginal == null)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

                CarOriginal = Converters.DoReflection<Model.Car, Entity.Car>(Car, CarOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Car GetCarsById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Car entity = dbContext.Car.Where(p => p.Id == id).SingleOrDefault();
                Model.Car app = Converters.DoReflection<Entity.Car, Model.Car>(entity, new Model.Car());

                return app;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Car GetCarsById(string UUID, string userID)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Car entity = dbContext.Car.Where(p => (p.UUID == UUID) && (p.UserId == userID)).SingleOrDefault();
                Model.Car app = Converters.DoReflection<Entity.Car, Model.Car>(entity, new Model.Car());

                return app;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Car> GetAllCars()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.Car.ToList();

                List<Model.Car> apps = new List<Model.Car>();

                foreach (Entity.Car item in entities)
                {
                    Model.Car app = Converters.DoReflection(item, new Model.Car());
                    app.User = Converters.DoReflection(item.User, new Model.User());

                    apps.Add(app);
                }
                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Car> GetAllCarsByYUser(string userId)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.Car.ToList().Where(x => x.UserId == userId);

                List<Model.Car> apps = new List<Model.Car>();

                foreach (Entity.Car item in entities)
                {
                    Model.Car app = Converters.DoReflection(item, new Model.Car());
                    apps.Add(app);
                }
                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Car entity_app = dbContext.Car.Where(u => u.Id == id).FirstOrDefault();

                if (entity_app != null)
                {
                    entity_app.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool TokenValid(string token)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entities = dbContext.Car.ToList().Where(x => (x.Token == token) && (x.Status != false));

                return entities.Any();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Logout(string token)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Car CarOriginal = dbContext.Car.FirstOrDefault(p => p.Token == token);
                CarOriginal.Status = false;
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Guid GetUserIdByToken(string token)
        {
            token = token.Replace("Bearer ", "");

            using (dbMultiAppEntities dbContext = new dbMultiAppEntities())
            {
                Car entity = dbContext.Car.SingleOrDefault(x => x.Token == token);

                return Guid.Parse(entity.UserId);
            }
        }

        public Guid GetBrandIdbyToken(string token)
        {
            token = token.Replace("Bearer ", "");

            using (dbMultiAppEntities dbContext = new dbMultiAppEntities())
            {
                Car entity = dbContext.Car.SingleOrDefault(x => x.Token == token);

                return Guid.Parse(entity.User.BrandId);
            }
        }

        public Task UpdateLocation(Guid carId, List<CarLocation> carLocations)
        {
            throw new NotImplementedException();
        }

        public Model.User GetUserByToken(string token)
        {
            throw new NotImplementedException();
        }

        public Vehicle GetVehicleByToken(string token)
        {
            throw new NotImplementedException();
        }

        public GeoCoordinate GetCarLastCoordinate(Guid carId)
        {
            throw new NotImplementedException();
        }

        public List<CarLocation> GetLocationList()
        {
            throw new NotImplementedException();
        }

        public Model.Car GetCarsByToken(string token)
        {
            throw new NotImplementedException();
        }

        Task ICar.UpdateCurrentCarLocation(Guid carId, LocationStreetMap location)
        {
            throw new NotImplementedException();
        }

        public CarCurrentLocation GetCarCurrentLocation(Guid carId)
        {
            throw new NotImplementedException();
        }
    }
}