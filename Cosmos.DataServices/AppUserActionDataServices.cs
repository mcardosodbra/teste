﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationUserAction = Cosmos.Model.ApplicationUserAction;

namespace Cosmos.DataServices
{
    public class AppUserActionDataServices
    {
        private readonly dbMultiAppEntities dbContext = new dbMultiAppEntities();

        public bool Add(ApplicationUserAction App)
        {
            try
            {
                App.Date = DateTime.Now;
                var app = Converters.DoReflection(App, new Entity.ApplicationUserAction());
                dbContext.ApplicationUserAction.Add(app);
                var result = dbContext.SaveChanges();
                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Update(ApplicationUserAction App)
        {
            try
            {
                var AppOriginal = dbContext.ApplicationUserAction.Where(p => p.Id == App.Id).FirstOrDefault();
                AppOriginal = Converters.DoReflection(App, AppOriginal);
                var result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Entity.ApplicationUserAction entity_app = dbContext.ApplicationUserAction.Where(u => u.Id == id).FirstOrDefault();

                if (entity_app != null)
                {
                    dbContext.ApplicationUserAction.Remove(entity_app);
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<ApplicationUserAction> GetAll()
        {
            try
            {
                var entities = dbContext.ApplicationUserAction.ToList();

                var apps = new List<ApplicationUserAction>();

                foreach (var item in entities)
                {
                    var app = Converters.DoReflection(item, new ApplicationUserAction());
                    apps.Add(app);
                }

                return apps;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public int GetInstalledApps(string appId, string appVersion)
        {
            try
            {
                var entities = dbContext.ApplicationUserAction.ToList().Where(s => s.ApplicationId == appId && s.AppVersionID == appVersion);

                return entities.Count();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public int GetDownloadApps(string appId, string appVersion)
        {
            try
            {
                var entities = dbContext.ApplicationUserAction.ToList().Where(s => s.ApplicationId == appId && s.AppVersionID == appVersion);

                return entities.Count();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public int GetInstalledActive(string appId, string appVersion)
        {
            try
            {
                var entities = dbContext.ApplicationUserAction.ToList().Where(s => s.ApplicationId == appId && s.AppVersionID == appVersion);

                return entities.Count();
            }
            catch (Exception err)
            {
                throw err;
            }
        }
    }
}