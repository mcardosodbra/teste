﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.DataServices.Interfaces
{
    public interface IUserApps
    {
		void AddUserApp(Model.UserApp App);
		void UpdateUserApp(Model.UserApp App);
		Model.UserApp GetByUserAppId(string id);
		List<Model.UserApp> GetByUserId(string id);
		List<Model.UserApp> GetAllUserApps();
		bool Delete(string id);
		bool Active(string id);

	}
}
