﻿using Cosmos.Model;

namespace Cosmos.DataServices.Interfaces
{
    public interface IPasswordReset
    {
        PasswordReset GetByToken(string token);

        PasswordReset GetByTokenByUserId(string userId);

        bool InsertToken(PasswordReset objToken);

        bool UpdateTokenStatus(PasswordReset Objtoken);
    }
}