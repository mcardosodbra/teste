﻿using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface ICategory
    {
		bool AddCategory(Model.Category Category);
		bool UpdateCategory(Model.Category Category);
		Model.Category GetCategoryById(string id);
		List<Model.Category> GetAllCategory();
		bool Delete(string id);
		bool Active(string id);
	}
}
