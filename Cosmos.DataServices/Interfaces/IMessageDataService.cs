﻿using Cosmos.Model;
using Cosmos.Model.GeoAPIOpenStreetMap;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cosmos.DataServices.Interfaces
{
    public interface IMessageDataService
    {
        List<Message> GetMessageList();

        List<Message> GetMessage(string token, string region, string state, string city, Guid brandId, Language languaage);

        void InsertMessage(Message message);

        void UpdateMessage(Message message);

        void DeleteMessage(Message message);

        Task MessageRead(Guid userId, Guid messageid, string token);

        void ChangeStatus(Guid id, int status);

        Message GetMessageById(Guid id);

        List<MessageText> GetMessageTextList();

        int ApproveMessage(Guid id, Guid approverId);

        List<Message> GetMessageDetail(Guid messageId);

        void ReproveMessage(Guid id, Guid approverId, string reason);
    }
}