﻿using System;
using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface ICityDataServices
    {
        List<City> SelectCity();

        List<City> SelectCity(Guid[] stateId);
    }
}