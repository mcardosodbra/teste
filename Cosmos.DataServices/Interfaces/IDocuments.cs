﻿using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IDocuments
    {
		bool AddDocuments(Model.Document Document);
		bool UpdateDocuments(Model.Document Document);
		Model.Document GetDocumentsById(string id);
		List<Model.Document> GetDocumentsAll();
		bool Delete(string id);
		bool Active(string id);
	}
}
