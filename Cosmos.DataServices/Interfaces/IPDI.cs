﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.DataServices.Interfaces
{
    public interface IPDI
    {
        void AddPDI(Model.PDI Pdi);
        void UpdatePDI(Model.PDI Pdi);
        List<Model.PDI> GetPDIAll();
        Model.PDI GetPDIById(string id);
        bool Delete(string id);
        bool Active(string id);
    }
}
