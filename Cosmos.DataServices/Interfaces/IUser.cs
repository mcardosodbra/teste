﻿using Cosmos.Model;
using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IUser
    {
        Model.User GetUserByEmail(string Email);

        bool AddUser(Model.User User);

        bool UpdateUser(Model.User User);

        List<Model.User> GetUserAll();

        Model.User GetUserById(string id);

        void GetUserByVehicle();

        Model.User GetUserByType(string id);

        bool Active(string id);

        bool Delete(string id);

        List<Permission> GetPermissionList();

        List<Permission> GetPermissionListByUser(Guid userId);
    }
}