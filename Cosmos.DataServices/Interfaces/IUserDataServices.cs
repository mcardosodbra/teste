﻿using Cosmos.Model;

namespace Cosmos.DataServices.Interfaces
{
    public interface IUserDataServices
    {
        User GetUserByToken(string token);
    }
}