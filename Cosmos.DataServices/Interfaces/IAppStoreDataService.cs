﻿using System;
using System.Collections.Generic;
using Cosmos.Model;

namespace Cosmos.DataServices.Interfaces
{
    public interface IAppStoreDataService
    {
        bool Add(Model.Application App);

        bool Update(Model.Application App);

        Model.Application GetApplicationById(string id);

        List<Model.Application> GetAll();

        List<Model.Application> GetAllPortal();

        List<Model.Application> SearchByCategory(string CategoryDescription);

        Model.Application GetApplicationByIdPortal(string id);

        Model.Application GetApplicationByPackageName(string packageName);

        bool Remove(string id, bool removeFromCentral);

        bool Active(string id);

        List<Model.Application> GetByCategory(string CategoryId);

        List<Model.Application> GetApplicationsActiveToCentral(string token);

        List<Model.Application> GetApplicationsToUpdateVersion(string token);

        List<Model.Application> GetApplicationsToRemove(string token);

        List<Model.Application> GetMultiAppToUpdateVersion(string token);

        List<Application> GetAllByLanguage(Language language);
    }
}