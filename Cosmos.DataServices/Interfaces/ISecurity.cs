﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.DataServices.Interfaces
{
    public interface ISecurity
    {
        void LoginValidation();
        void TokenGeneration();
    }
}
