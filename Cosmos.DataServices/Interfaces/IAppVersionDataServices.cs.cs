﻿using Cosmos.Model;
using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IAppVersionDataServices
    {
        bool Update(AppVersion appVersion);

        List<AppVersion> GetVersionsByApplicationId(string id);

        AppVersion GetAppVersionById(string id);

        bool Delete(string id);

        bool Active(string id);

        bool IsVersionInUseForApp(string version, string app);

        void InsertVersion(Guid versionId, Guid applicationId, int versionCode, string packageName);
    }
}