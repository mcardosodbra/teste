﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.DataServices.Interfaces
{
    public interface IDocumentType
    {
		void AddDocumentType(Model.DocumentType DocumentType);
		void UpdateDocumentType(Model.DocumentType DocumentType);
		Model.DocumentType GetDocumentTypeById(string id);
		List<Model.DocumentType> GetDocumentTypeAll();
		bool Delete(string id);
		bool Active(string id);
	}
}
