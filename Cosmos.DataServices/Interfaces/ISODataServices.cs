﻿using Cosmos.Model;
using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface ISODataServices
    {
        List<SO> GetListSO();

        Guid InsertSO(SO so);

        void UpdateSO(SO so);

        void DeleteSO(SO so);

        SO GetSODetail(Guid SOId);
    }
}