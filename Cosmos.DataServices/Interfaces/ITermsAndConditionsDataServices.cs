﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface ITermsAndConditionsDataServices
    {
        void Insert(TermsAndConditions termsAndConditions);

        void Update(TermsAndConditions termsAndConditions);

        void Active(TermsAndConditions termsAndConditions);

        List<TermsAndConditions> GetTermsAndConditions();

        TermsAndConditions GetTermsAndConditions(TermsAndConditions terms);
    }
}