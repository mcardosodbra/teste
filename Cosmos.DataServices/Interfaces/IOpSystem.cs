﻿using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IOpSystem
    {
        bool AddOpSystem(Model.OperationalSystem operationalSystem);
        bool UpdateOpSystem(Model.OperationalSystem operationalSystem);
        Model.OperationalSystem GetById(string id);
        List<Model.OperationalSystem> GetAll();
        bool Delete(string id);
        bool Active(string id);
    }
}
