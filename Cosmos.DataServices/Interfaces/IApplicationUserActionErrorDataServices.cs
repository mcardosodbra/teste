﻿using Cosmos.Model;

namespace Cosmos.DataServices.Interfaces
{
    public interface IApplicationUserActionErrorDataServices
    {
        void insert(ApplicationUserActionError objError);
    }
}