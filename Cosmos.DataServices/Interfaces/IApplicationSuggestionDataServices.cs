﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IApplicationSuggestionDataServices
    {
        List<ApplicationSuggestion> SelectApplicationSuggestion();

        void InsertApplicationSuggestion(ApplicationSuggestion applicationsuggestion);
    }
}