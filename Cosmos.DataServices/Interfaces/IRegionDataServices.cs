﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IRegionDataServices
    {
        List<Region> SelectRegion();

        void InsertRegion(Region region);

        void UpdateRegion(Region region);

        void DeleteRegion(Region region);
    }
}