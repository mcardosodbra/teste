﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.DataServices.Interfaces
{
    public interface IUserType
    {
		void AddUserType(Model.UserType UserType);
		void UpdateUserType(Model.UserType UserType);
		Model.UserType GetUserTypeById(string id);
		List<Model.UserType> GetAll();
		bool Active(string id);
		bool Delete(string id);
	}
}
