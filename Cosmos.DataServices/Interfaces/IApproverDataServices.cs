﻿using System;
using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IApproverDataServices
    {
        List<Approver> SelectApprover(Guid brandId, bool approved);

        List<Approver> SelectApprover(Guid brandId, bool approved, ApproverType approverType);

        List<Approver> SelectApprover(Guid brandId, bool approved, Guid messageId);

        Approver SelectApproverById(Guid approverId);

        Guid InsertApprover(Approver approver);

        void UpdateApprover(Approver approver);

        void DeleteApprover(Approver approver);

        void ActivateApprover(Guid approver);

        void DeActivateApprover(Guid approver);

        List<Approver> SelectApprover(Guid brandId);
    }
}