﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IGlobalDataServices
    {
        List<Language> LanguageList();

        List<Permission> PermissionList();

        void LogtoDb(string message, string endpointcall, LogType logType);
    }
}