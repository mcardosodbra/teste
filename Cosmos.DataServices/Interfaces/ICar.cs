﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cosmos.Model;
using Cosmos.Model.GeoAPIOpenStreetMap;
using Cosmos.Utils;

namespace Cosmos.DataServices.Interfaces
{
    public interface ICar
    {
        bool AddCar(Model.Car Car);

        bool UpdateCar(Model.Car Car);

        Model.Car GetCarsById(string id);

        Model.Car GetCarsByToken(string token);

        List<Model.Car> GetAllCars();

        List<Model.Car> GetAllCarsByYUser(string userId);

        bool Delete(string id);

        bool TokenValid(string token);

        bool Logout(string token);

        Guid GetUserIdByToken(string token);

        Guid GetBrandIdbyToken(string token);

        Task UpdateLocation(Guid carId, List<CarLocation> carLocations);

        User GetUserByToken(string token);

        Vehicle GetVehicleByToken(string token);

        GeoCoordinate GetCarLastCoordinate(Guid carId);

        List<CarLocation> GetLocationList();

        Task UpdateCurrentCarLocation(Guid carId, LocationStreetMap location);

        CarCurrentLocation GetCarCurrentLocation(Guid carId);
    }
}