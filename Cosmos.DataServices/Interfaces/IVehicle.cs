﻿using Cosmos.Entity;
using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IVehicle
    {
        bool AddVehicle(Model.Vehicle vehicle);

        bool UpdateVehicle(Model.Vehicle vehicle);

        Model.Vehicle GetVehicleById(string id);

        List<Model.Vehicle> GetVehicleAll();

        bool Delete(string id);

        bool Active(string id);

        List<Model.Vehicle> GetVehicleByBrand(Guid brandId);
    }
}