﻿using Cosmos.Model;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IApplicationUserAction
    {
        bool Add(ApplicationUserAction appUser);

        bool Update(ApplicationUserAction appUser);

        bool Delete(int id);

        List<ApplicationUserAction> GetAll();
    }
}