﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cosmos.Model;

namespace Cosmos.DataServices.Interfaces
{
    public interface IEmailConfig
    {
        Email GetConfigs();
    }
}
