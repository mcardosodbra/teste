﻿using System;
using System.Collections.Generic;

namespace Cosmos.DataServices.Interfaces
{
    public interface IBrand
    {
		bool AddBrand(Model.Brand Brand);
		bool UpdateBrand(Model.Brand Brand);
		List<Model.Brand> GetAll();
		Model.Brand GetBrandById(string id);
		bool Delete(string id);
		bool Active(string id);
	}
}
