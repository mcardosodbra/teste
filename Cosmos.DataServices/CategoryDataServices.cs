﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Cosmos.DataServices
{
    public class CategoryDataServices : ICategory
    {
        public bool AddCategory(Model.Category Category)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Category category = Converters.DoReflection<Model.Category, Entity.Category>(Category, new Entity.Category());
                category.Register = DateTime.Now;
                dbContext.Category.Add(category);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Category> GetAllCategory()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityCategory = dbContext.Category.ToList();
                List<Model.Category> listCategory = new List<Model.Category>();

                foreach (Entity.Category category in entityCategory)
                    listCategory.Add(Converters.DoReflection(category, new Model.Category()));

                return listCategory;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Category> GetAllCategoryForApi()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                {
                    var entityCategory = dbContext.Category.SqlQuery(@"select distinct cat.* from category cat
inner join Application app on app.CategoryId = cat.Id
inner join AppVersion appV on app.id = Appv.applicationid
where appv.active = 1").ToList();
                    List<Model.Category> listCategory = new List<Model.Category>();

                    foreach (Entity.Category category in entityCategory)
                        listCategory.Add(Converters.DoReflection(category, new Model.Category()));
                    foreach (var item in listCategory)
                    {
                        item.CounterApp = dbContext.Database.SqlQuery<int>(@"select count(app.id) from category cat
inner join Application app on app.CategoryId = cat.Id
inner join AppVersion appV on app.id = Appv.applicationid
where appv.active = 1 and cat.id = @id", new SqlParameter("id", item.Id)).FirstOrDefault();
                    }

                    return listCategory;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Category GetCategoryById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Category entityCategory = dbContext.Category.Where(p => p.Id == id).SingleOrDefault();
                Model.Category category = Converters.DoReflection<Entity.Category, Model.Category>(entityCategory, new Model.Category());

                return category;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateCategory(Model.Category Category)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Category CategoryOriginal = dbContext.Category.Where(p => p.Id == Category.Id).FirstOrDefault();
                Category.Register = CategoryOriginal.Register;
                CategoryOriginal = Converters.DoReflection<Model.Category, Entity.Category>(Category, CategoryOriginal);

                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Category entity_category = dbContext.Category.Where(u => u.Id == id).FirstOrDefault();

                if (entity_category != null)
                {
                    dbContext.Entry(entity_category).State = EntityState.Deleted;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Category entity_category = dbContext.Category.Where(u => u.Id == id).FirstOrDefault();

                if (entity_category != null)
                {
                    entity_category.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsCategoryNameInUse(string Name)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.Category.Where(e => e.Description.Equals(Name)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}