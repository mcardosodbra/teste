﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using Vehicle = Cosmos.Model.Vehicle;

namespace Cosmos.DataServices
{
    public class VehicleDataServices : IVehicle
    {
        public bool AddVehicle(Model.Vehicle vehicle)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Vehicle entityVehicle = Converters.DoReflection<Model.Vehicle, Entity.Vehicle>(vehicle, new Entity.Vehicle());
                dbContext.Vehicle.Add(entityVehicle);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool UpdateVehicle(Model.Vehicle vehicle)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Vehicle entityVehicle = dbContext.Vehicle.Where(p => p.Id == vehicle.Id).FirstOrDefault();
                entityVehicle = Converters.DoReflection<Model.Vehicle, Entity.Vehicle>(vehicle, entityVehicle);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Model.Vehicle> GetVehicleAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityVehicles = dbContext.Vehicle.ToList();
                List<Model.Vehicle> listVehicles = new List<Model.Vehicle>();

                foreach (Entity.Vehicle item in entityVehicles)
                {
                    Model.Vehicle vehicle = Converters.DoReflection(item, new Model.Vehicle());
                    vehicle.Brand = Converters.DoReflection(new DataServices.BrandDataServices().GetBrandById(vehicle.BrandId), new Model.Brand());
                    listVehicles.Add(vehicle);
                }

                return listVehicles;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.Vehicle GetVehicleById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.Vehicle entityVehicle = dbContext.Vehicle.Where(p => p.Id == id).SingleOrDefault();
                Model.Vehicle vehicle = Converters.DoReflection<Entity.Vehicle, Model.Vehicle>(entityVehicle, new Model.Vehicle());

                return vehicle;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Vehicle entity_vehicle = dbContext.Vehicle.Where(u => u.Id == id).FirstOrDefault();

                if (entity_vehicle != null)
                {
                    entity_vehicle.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Vehicle> GetVehicleByBrand(Guid brandId)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityVehicles = dbContext.Vehicle.Where(x => x.BrandId == brandId.ToString()).ToList();
                List<Model.Vehicle> listVehicles = new List<Model.Vehicle>();

                foreach (Entity.Vehicle item in entityVehicles)
                {
                    Model.Vehicle vehicle = Converters.DoReflection(item, new Model.Vehicle());
                    vehicle.Brand = Converters.DoReflection(new DataServices.BrandDataServices().GetBrandById(vehicle.BrandId), new Model.Brand());
                    listVehicles.Add(vehicle);
                }

                return listVehicles;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.Vehicle entity_vehicle = dbContext.Vehicle.Where(u => u.Id == id).FirstOrDefault();

                if (entity_vehicle != null)
                {
                    entity_vehicle.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsVehicleNameInUse(string Name)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.Vehicle.Where(e => e.Name.Equals(Name)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}