﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using Cosmos.Model;
using Permission = Cosmos.Model.Permission;
using User = Cosmos.Entity.User;

namespace Cosmos.DataServices
{
    public class UserDataServices : IUser
    {
        public bool AddUser(Model.User User)
        {
            //dbMultiAppEntities dbContext = new dbMultiAppEntities();
            //try
            //{
            //    Entity.User user = Converters.DoReflection<Model.User, Entity.User>(User, new Entity.User());
            //    user.Id = User.Id;
            //    dbContext.User.Add(user);
            //    int result = dbContext.SaveChanges();

            //    return result > 0;
            //}
            //catch (Exception err)
            //{
            //    throw err;
            //}
            //finally
            //{
            //    dbContext.Dispose();
            //}
            return true;
        }

        public List<Model.User> GetUserAll()
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                var entityUsers = dbContext.User.ToList();
                List<Model.User> listUsers = new List<Model.User>();

                foreach (Entity.User item in entityUsers)
                {
                    Model.User user = Converters.DoReflection(item, new Model.User());
                    user.UserType = Converters.DoReflection(item.UserType, new Model.UserType());

                    //Model.Brand brand = new DataServices.BrandDataServices().GetBrandById(user.BrandId);
                    user.Brand = Converters.DoReflection(item.Brand, new Model.Brand());

                    listUsers.Add(user);
                }

                return listUsers;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.User GetUserById(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                User entityUser = dbContext.User.Where(p => p.Id == id).SingleOrDefault();
                Model.User user = Converters.DoReflection<Entity.User, Model.User>(entityUser, new Model.User());

                return user;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.User GetUserByEmail(string Email)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                User entityUser = dbContext.User.Where(p => p.Email == Email).SingleOrDefault();
                Model.User user = Converters.DoReflection<Entity.User, Model.User>(entityUser, new Model.User());

                return user;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.User GetUserByType(string id)
        {
            throw new NotImplementedException();
        }

        public void GetUserByVehicle()
        {
            throw new NotImplementedException();
        }

        public bool UpdateUser(Model.User User)
        {
            //dbMultiAppEntities dbContext = new dbMultiAppEntities();
            //try
            //{
            //    User.Permission = new Permission();

            //    User UserOriginal = dbContext.User.Where(p => p.Id == User.Id).FirstOrDefault();
            //    User.Password = UserOriginal.Password;

            //    dbContext.SaveChanges();

            //    int result = dbContext.SaveChanges();

            //    return result > 0;
            //}
            //catch (Exception err)
            //{
            //    throw err;
            //}
            //finally
            //{
            //    dbContext.Dispose();
            //}
            return false;
        }

        public bool UpdateUserPassword(Model.User User)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                User.Permission = new Permission();
                User UserOriginal = dbContext.User.Where(p => p.Id == User.Id).FirstOrDefault();
                UserOriginal = Converters.DoReflection<Model.User, Entity.User>(User, UserOriginal);
                int result = dbContext.SaveChanges();

                return result > 0;
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool Delete(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.User entity_user = dbContext.User.Where(u => u.Id == id).FirstOrDefault();

                if (entity_user != null)
                {
                    entity_user.Status = false;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public List<Permission> GetPermissionList()
        {
            throw new NotImplementedException();
        }

        public List<Permission> GetPermissionListByUser(Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool Active(string id)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                Entity.User entity_user = dbContext.User.Where(u => u.Id == id).FirstOrDefault();

                if (entity_user != null)
                {
                    entity_user.Status = true;
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public Model.User UserValidate(string Email, string Password)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();
            try
            {
                Entity.User user = dbContext.User.FirstOrDefault(u => u.Email.Equals(Email) && u.Password.Equals(Password));

                if (user != null)
                {
                    Model.User user_ = Converters.DoReflection<Entity.User, Model.User>(user, new Model.User());

                    if (user_ != null)
                    {
                        user_.UserType = Converters.DoReflection<Entity.UserType, Model.UserType>(user.UserType, new Model.UserType());
                        user_.Brand = Converters.DoReflection<Entity.Brand, Model.Brand>(user.Brand, new Model.Brand());
                        user_.Permission = Converters.DoReflection<Entity.Permission, Model.Permission>(user.Permission, new Model.Permission());
                    }

                    return user_;
                }
                else
                {
                    //throw new Exception("Usuário não encontrado.");
                    return null;
                }
            }
            catch (Exception err)
            {
                throw new Exception(err.Message);
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsEmailInUse(string Email)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.User.Where(e => e.Email.Equals(Email)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsLicensePlateInUse(string Licenseplate)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.User.Where(e => e.Email.Equals(Licenseplate)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }

        public bool IsVinInUse(string Vin)
        {
            dbMultiAppEntities dbContext = new dbMultiAppEntities();

            try
            {
                return dbContext.User.Where(e => e.Email.Equals(Vin)).Count() > 0;
            }
            finally
            {
                dbContext.Dispose();
            }
        }
    }
}