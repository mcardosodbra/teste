﻿using Cosmos.DataServices.Interfaces;
using Cosmos.Entity;
using Cosmos.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cosmos.DataServices
{
    public class DocumentTypeDataServices : IDocumentType
	{
		public void AddDocumentType(Model.DocumentType DocumentType)
		{
			dbMultiAppEntities dbContext = new dbMultiAppEntities();
			try
			{
				Entity.DocumentType documentType = Converters.DoReflection<Model.DocumentType, Entity.DocumentType>(DocumentType, new Entity.DocumentType());
				dbContext.DocumentType.Add(documentType);
				dbContext.SaveChanges();
			}
			catch (Exception err)
			{
				throw err;
			}
			finally
			{
				dbContext.Dispose();
			}
		}

		public Model.DocumentType GetDocumentTypeById(string id)
		{
			dbMultiAppEntities dbContext = new dbMultiAppEntities();
			try
			{
				Entity.DocumentType entityDocumentType = dbContext.DocumentType.Where(p => p.Id == id).SingleOrDefault();
				Model.DocumentType documentType = Converters.DoReflection<Entity.DocumentType, Model.DocumentType>(entityDocumentType, new Model.DocumentType());
				
				return documentType;                
			}
			catch (Exception err)
			{
				throw err;
			}
			finally
			{
				dbContext.Dispose();
			}
		}

		public List<Model.DocumentType> GetDocumentTypeAll()
		{
			dbMultiAppEntities dbContext = new dbMultiAppEntities();
			try
			{
				var entityDocumentType = dbContext.DocumentType.ToList();
				List<Model.DocumentType> listDocumentType = new List<Model.DocumentType>();

				foreach (Entity.DocumentType type in entityDocumentType)
					listDocumentType.Add(Converters.DoReflection(type, new Model.DocumentType()));

				return listDocumentType;
			}
			catch (Exception err)
			{
				throw err;
			}
			finally
			{
				dbContext.Dispose();
			}
		}

		public void UpdateDocumentType(Model.DocumentType DocumentType)
		{
			dbMultiAppEntities dbContext = new dbMultiAppEntities();
			try
			{
				Entity.DocumentType DocumentTypeOriginal = dbContext.DocumentType.Where(p => p.Id == DocumentType.Id).FirstOrDefault();
				DocumentTypeOriginal = Converters.DoReflection<Model.DocumentType, Entity.DocumentType>(DocumentType, DocumentTypeOriginal);
				dbContext.SaveChanges();
			}
			catch (Exception err)
			{
				throw err;
			}
			finally
			{
				dbContext.Dispose();
			}
		}

		public bool Delete(string id)
		{
			dbMultiAppEntities dbContext = new dbMultiAppEntities();

			try
			{

				Entity.DocumentType entity_documentType = dbContext.DocumentType.Where(u => u.Id == id).FirstOrDefault();

				if (entity_documentType != null)
				{
					entity_documentType.Status = false;
					dbContext.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception err)
			{
				throw err;
			}
			finally
			{
				dbContext.Dispose();
			}
		}

		public bool Active(string id)
		{
			dbMultiAppEntities dbContext = new dbMultiAppEntities();

			try
			{

				Entity.DocumentType entity_documentType = dbContext.DocumentType.Where(u => u.Id == id).FirstOrDefault();

				if (entity_documentType != null)
				{
					entity_documentType.Status = true;
					dbContext.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception err)
			{
				throw err;
			}
			finally
			{
				dbContext.Dispose();
			}
		}
	}
}
