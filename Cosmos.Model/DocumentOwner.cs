﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class DocumentOwner
    {
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string DocumentId { get; set; }
        public bool Readed { get; set; }
        public bool Status { get; set; }

        public virtual Document Document { get; set; }
    }
}
