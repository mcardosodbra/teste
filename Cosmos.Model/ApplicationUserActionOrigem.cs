﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cosmos.Model
{
    public enum ApplicationUserActionOrigem
    {
        Central = 1,
        PainelMontadora = 2,
        PainelDisruptiv = 3
    }
}