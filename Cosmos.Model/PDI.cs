﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class PDI
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string OpSystemId { get; set; }
        public bool Status { get; set; }

        public virtual OperationalSystem OperationalSystem { get; set; }
    }
}
