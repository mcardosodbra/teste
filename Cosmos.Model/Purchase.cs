﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class Purchase
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ApplicationId { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool Status { get; set; }

        public Application Application { get; set; }
        public User User { get; set; }
    }
}
