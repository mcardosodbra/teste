﻿using System;

namespace Cosmos.Model
{
    public class State
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Region Region { get; set; }
        public bool IsSelected { get; set; }
    }
}