﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Cosmos.Model
{
    public class Category
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string DescriptionEN { get; set; }
        public string DescriptionES { get; set; }
        public bool Status { get; set; }
        public string IconUrl { get; set; }
        public DateTime? Register { get; set; }
        public int CounterApp { get; set; }
        public List<Application> App { get; set; }
        public HttpPostedFileBase imageData { get; set; }
    }
}