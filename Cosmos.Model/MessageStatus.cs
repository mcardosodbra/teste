﻿using System.ComponentModel;

namespace Cosmos.Model
{
    public enum MessageStatus
    {
        [Description("Aguardando Aprovação")]
        WaitingApproval = 1,

        [Description("Aprovada")]
        Approved = 2,

        [Description("Reprovada")]
        Reproved = 3,

        [Description("Agendada")]
        Scheduled = 4,

        [Description("Enviada")]
        Sent = 5,

        [Description("Expirada")]
        Expired = 6,

        [Description("Cancelada")]
        Canceled = 7,

        [Description("Pronta para envio")]
        Ready = 8
    };
}