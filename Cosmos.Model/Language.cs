﻿using System;

namespace Cosmos.Model
{
    public class Language
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}