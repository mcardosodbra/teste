﻿using System;

namespace Cosmos.Model
{
    public class Permission
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public PermissionType PermissionType { get; set; }
        public bool Checked { get; set; }
    }
}