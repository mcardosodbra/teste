﻿namespace Cosmos.Model
{
    public class Manual
    {
        public string Id { get; set; }
        public string VehicleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        public Vehicle Vehicle { get; set; }
    }
}