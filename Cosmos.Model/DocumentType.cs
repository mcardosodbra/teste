﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class DocumentType
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }
}
