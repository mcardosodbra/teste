﻿using System;

namespace Cosmos.Model
{
    public class TermAndConditionUserResponse
    {
        public Guid Id { get; set; }
        public Guid TermAndConditionId { get; set; }
        public Guid UserId { get; set; }
        public DateTime Register { get; set; }
        public bool Accept { get; set; }
    }
}