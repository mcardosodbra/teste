﻿using System;

namespace Cosmos.Model
{
    public class MessageApprover
    {
        public Guid Id { get; set; }
        public Approver Approver { get; set; }
        public Message Message { get; set; }
    }
}