﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class Notification
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string BrandId { get; set; }
        public string TypeId { get; set; }
        public string Description { get; set; }
        public DateTime SendDate { get; set; }
        public DateTime PublishedDate { get; set; }
        public double Download { get; set; }
        public bool Approval { get; set; }
        public bool Status { get; set; }

        public Brand Brand { get; set; }
        public NotificationType NotificationType { get; set; }
        public User User { get; set; }
    }
}
