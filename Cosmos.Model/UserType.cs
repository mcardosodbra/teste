﻿namespace Cosmos.Model
{
    public class UserType
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
    }
}