﻿using System;
using System.Web;

namespace Cosmos.Model
{
    public class TermsAndConditions
    {
        public Guid Id { get; set; }
        public Guid IdBrand { get; set; }
        public Brand Brand { get; set; }
        public string TitlePT { get; set; }
        public string TitleEN { get; set; }
        public string TitleES { get; set; }
        public DateTime Register { get; set; }
        public int Version { get; set; }
        public string FilePT { get; set; }
        public string FileEN { get; set; }
        public string FileES { get; set; }
        public bool Status { get; set; }
        public bool Active { get; set; }
        public int Accepteds { get; set; }
        public int Recuseds { get; set; }
        public HttpPostedFileBase PDFPT { get; set; }
        public HttpPostedFileBase PDFEN { get; set; }
        public HttpPostedFileBase PDFES { get; set; }
    }
}