﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class GUI
    {
        public string Id { get; set; }
        public string PDIId { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public bool Status { get; set; }

        public PDI PDI { get; set; }
    }
}
