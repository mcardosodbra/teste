﻿using System;

namespace Cosmos.Model
{
    public class LocationCache
    {
        public DateTime DtRegister { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}