﻿namespace Cosmos.Model
{
    public enum PermissionType
    {
        Approver = 1,
        Message = 2
    }
}