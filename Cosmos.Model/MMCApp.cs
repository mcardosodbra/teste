﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class MMCApp
    {
        public string Id { get; set; }
        public string UserAppId { get; set; }
        public string MultimediaId { get; set; }
        public string Version { get; set; }
        public bool Status { get; set; }

        public UserApp UserApp { get; set; }
        public MultimediaCentral MultimediaCentral { get; set; }
    }
}
