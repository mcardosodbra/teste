﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class Log
    {
        public string Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }

        public User User { get; set; }
    }
}
