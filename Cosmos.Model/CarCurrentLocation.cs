﻿using System;

namespace Cosmos.Model
{
    public class CarCurrentLocation
    {
        public Car Car { get; set; }
        public Region Region { get; set; }
        public State State { get; set; }
        public City City { get; set; }
        public DateTime DtLastUpdate { get; set; }
    }
}