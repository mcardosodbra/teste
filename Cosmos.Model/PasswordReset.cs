﻿
using System;

namespace Cosmos.Model
{
    public class PasswordReset
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public DateTime DtRegister { get; set; }
        public bool Status { get; set; }
        public string UserId { get; set; }
    }
}
