﻿using System;

namespace Cosmos.Model
{
    public class ApplicationUserActionError
    {
        public Guid Id { get; set; }
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public string Payload { get; set; }
        public ApplicationUserActionOrigem Origem { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
    }
}