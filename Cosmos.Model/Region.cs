﻿using System;

namespace Cosmos.Model
{
    public class Region
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}