﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class OperationalSystem
    {
        public string Id { get; set; }
        public string Version { get; set; }
        public bool Status { get; set; }
    }
}
