﻿namespace Cosmos.Model
{
    public enum ApplicationUserActionType
    {
        Download = 1,
        DownloadCompleted = 2,
        Install = 3,
        Uninstall = 4,
        Active = 5,
        Error = 6
    }
}