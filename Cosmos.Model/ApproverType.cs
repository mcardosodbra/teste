﻿namespace Cosmos.Model
{
    public enum ApproverType
    {
        Mandatorio = 1,
        Ocasional = 2
    };
}