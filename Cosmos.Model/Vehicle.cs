﻿using System;

namespace Cosmos.Model
{
    public class Vehicle
    {
        public string Id { get; set; }
        public string BrandId { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }

        public Brand Brand { get; set; }
        public bool IsSelected { get; set; }
    }
}