﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class MultimediaCentral
    {
        public string Id { get; set; }
        public string PDIId { get; set; }
        public string CarId { get; set; }
        public string Firmware { get; set; }
        public bool Status { get; set; }

        public virtual PDI PDI { get; set; }
        public virtual Car Car { get; set; }
    }
}
