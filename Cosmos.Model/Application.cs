﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Cosmos.Model
{
    public class Application
    {
        public Application()
        {
            APK = new object();
            Versions = new List<Model.AppVersion>();
            Category = new Model.Category();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public int ActiveVersion { get; set; }
        public string Logotype { get; set; }
        public bool Status { get; set; }
        public DateTime InclusionDate { get; set; }
        public string Description { get; set; }
        public string DescriptionES { get; set; }
        public string DescriptionEN { get; set; }
        public bool RemoveFromCentral { get; set; }
        public int Relevance { get; set; }
        public string CategoryId { get; set; }
        public DateTime LastUpdate { get; set; }
        public int DownloadedTimes { get; set; }
        public int InstalledTimes { get; set; }
        public int UninstalledTimes { get; set; }
        public string PackageName { get; set; }
        public ApplicationType Type { get; set; }
        public HttpPostedFileBase LogoTypeData { get; set; }

        public int InstalledCentrals { get; set; }

        public object APK
        {
            get;
            set;
        }

        public List<Model.AppVersion> Versions
        {
            get;
            set;
        }

        public Category Category
        {
            get;
            set;
        }
    }
}