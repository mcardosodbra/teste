﻿using System;

namespace Cosmos.Model
{
    public class User
    {
        public User()
        {
            UserType = new UserType();
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string TypeId { get; set; }
        public string BrandId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string LicensePlate { get; set; }
        public string VIN { get; set; }
        public SO SO { get; set; }
        public string sSO { get; set; }
        public string MCU { get; set; }
        public string CAN { get; set; }
        public string UUID { get; set; }
        public DateTime InclusionDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public bool? Visible { get; set; }
        public bool Status { get; set; }

        public UserType UserType { get; set; }
        public Brand Brand { get; set; }
        public Nullable<System.Guid> PermissionId { get; set; }
        public Permission Permission { get; set; }
    }
}