﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class StockApp
    {
        public string Id { get; set; }
        public string PDIId { get; set; }
        public string ApplicationId { get; set; }
        public bool Status { get; set; }

        public Application Application { get; set; }
        public PDI PDI { get; set; }
    }
}
