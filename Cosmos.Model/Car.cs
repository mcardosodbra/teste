﻿using System;

namespace Cosmos.Model
{
    public class Car
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string VIN { get; set; }
        public bool Status { get; set; }
        public string LicensePlate { get; set; }
        public string UUID { get; set; }
        public string CAN { get; set; }
        public string MCU { get; set; }
        public string Token { get; set; }
        public DateTime? LastMessageRequest { get; set; }

        public SO SO { get; set; }

        //public Vehicle Vehicle { get; set; }
        public User User { get; set; }

        public Guid SOID { get; set; }
        public DateTime? DtLastLocationUpdate { get; set; }
    }
}