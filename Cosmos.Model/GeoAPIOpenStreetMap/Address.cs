﻿namespace Cosmos.Model.GeoAPIOpenStreetMap
{
    public class Address
    {
        public string police { get; set; }
        public string road { get; set; }
        public string city_district { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string state_district { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
    }
}