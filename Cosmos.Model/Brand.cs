﻿namespace Cosmos.Model
{
    public class Brand
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Logotype { get; set; }
        public bool Status { get; set; }
    }
}