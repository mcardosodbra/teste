﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class UserApp
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ApplicationId { get; set; }
        public string SerialNumber { get; set; }
        public bool Status { get; set; }

        public Application Application { get; set; }
        public User User { get; set; }
    }
}
