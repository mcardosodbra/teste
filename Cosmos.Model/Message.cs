﻿using System;
using System.Collections.Generic;

namespace Cosmos.Model
{
    public class Message
    {
        public Guid Id { get; set; }
        public MessageType MessageType { get; set; }
        public int MessageTypeId { get; set; }
        public MessageStatus MessageStatus { get; set; }
        public Guid UserIdDestiny { get; set; }
        public Guid BrandIdDestiny { get; set; }
        public Guid[] RegionIdDestiny { get; set; }
        public User UserDestiny { get; set; }
        public Brand BrandDestiny { get; set; }
        public Region RegionDestiny { get; set; }
        public User Creator { get; set; }
        public Guid UserId { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string ReprovedReason { get; set; }
        public List<MessageText> MessageText { get; set; }
        public Guid[] VehicleIdDestiny { get; set; }
        public Guid[] StateIdDestiny { get; set; }
        public Guid[] CityIdDestiny { get; set; }
        public int[] YearList { get; set; }
        public Vehicle VehicleDestiny { get; set; }
        public List<Approver> Approver { get; set; }
        public string VehicleList { get; set; }
        public string RegionList { get; set; }
        public bool CheckAllCities { get; set; }
        public DateTime PublishedDate { get; set; }
        public List<SO> SOIdDestiny { get; set; }
    }
}