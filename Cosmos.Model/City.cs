﻿using System;

namespace Cosmos.Model
{
    public class City
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public State State { get; set; }
    }
}