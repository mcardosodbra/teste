﻿using System;

namespace Cosmos.Model
{
    public class ApplicationSuggestion
    {
        public Guid Id { get; set; }
        public string Suggestion { get; set; }
        public User User { get; set; }
        public DateTime DtCreated { get; set; }
    }
}