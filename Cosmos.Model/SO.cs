﻿using System;

namespace Cosmos.Model
{
    public class SO
    {
        public Guid Id { get; set; }
        public Brand Brand { get; set; }
        public Vehicle Vehicle { get; set; }
        public int? YearModel { get; set; }
        public int? YearFab { get; set; }
        public string Version { get; set; }
        public bool Status { get; set; }
        public DateTime? ReleaseDate { get; set; }
    }
}