﻿namespace Cosmos.Model
{
    public enum MessageType
    {
        Alerta = 1,
        Evento = 2,
        Novidade = 3,
        Atualização = 4,
        Promoções = 5,
        Recall = 6,
        Serviços = 7
    };
}