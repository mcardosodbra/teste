﻿namespace Cosmos.Model
{
    public class Email
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public string AddressFrom { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string AddressTo { get; set; }
    }
}