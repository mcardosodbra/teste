﻿namespace Cosmos.Model
{
    public class Product
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public User User { get; set; }
    }
}