﻿using System;

namespace Cosmos.Model
{
    public class MessageText
    {
        public Guid Id { get; set; }
        public Language Language { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Banner { get; set; }
        public Guid MessageId { get; set; }
    }
}