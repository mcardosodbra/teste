﻿using System;

namespace Cosmos.Model
{
    public class Approver
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public string Sector { get; set; }
        public bool Status { get; set; }
        public bool Approved { get; set; }
        public ApproverType ApproverType { get; set; }
        public Brand Brand { get; set; }
        public int ApproverTypeId { get; set; }
        public bool IsSelected { get; set; }
    }
}