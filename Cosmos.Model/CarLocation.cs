﻿using System;

namespace Cosmos.Model
{
    public class CarLocation
    {
        public DateTime DtRegister { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Speed { get; set; }
        public double Accuracy { get; set; }
    }
}