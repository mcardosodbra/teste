﻿namespace Cosmos.Model
{
    public static class MessageTags
    {
        public static string FirstName { get; set; } = "$PrimeiroNome";
        public static string Phone { get; set; } = "$Telefone";
        public static string Email { get; set; } = "$Email";
        public static string Vehicle { get; set; } = "$ModeloVeiculo";
        public static string State { get; set; } = "$Estado";
        public static string City { get; set; } = "$Cidade";
    }
}