﻿using System;

namespace Cosmos.Model
{
    public class Document
    {
        public Document()
        {
            FileDocument = new object();
        }

        public string Id { get; set; }
        public string Title { get; set; }
        public string TypeId { get; set; }
        public string Description { get; set; }
        public DateTime InclusionDate { get; set; }
        public bool Status { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public object FileDocument
        {
            get;
            set;
        }
    }
}