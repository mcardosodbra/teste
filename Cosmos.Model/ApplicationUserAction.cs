﻿using System;

namespace Cosmos.Model
{
    public class ApplicationUserAction
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string ApplicationId { get; set; }
        public string UserId { get; set; }
        public string Package { get; set; }
        public string AppVersionId { get; set; }
        public AppVersion AppVersion { get; set; }
        public string CarId { get; set; }
        public ApplicationUserActionType Type { get; set; }
        public ApplicationUserActionError Error { get; set; }
    }
}