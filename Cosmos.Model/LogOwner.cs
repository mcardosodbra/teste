﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmos.Model
{
    public class LogOwner
    {
        public string Id { get; set; }
        public string OwnedId { get; set; }
        public string LogId { get; set; }
        public bool Status { get; set; }

        public virtual Log Log { get; set; }
    }
}
