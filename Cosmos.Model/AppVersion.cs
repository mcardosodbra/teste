﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cosmos.Model
{
    public class AppVersion
    {
        public string Id { get; set; }
        public string ApplicationId { get; set; }
        public string Version { get; set; }
        public string Package { get; set; }
        public string ApkUri { get; set; }
        public DateTime ReleaseDate { get; set; }
        public bool Status { get; set; }
        public int InstalledCount { get; set; }
        public int DownloadCount { get; set; }
        public int ActiveCount { get; set; }
        public bool Downgrade { get; set; }
        public string ReleaseNote { get; set; }
        public string ReleaseNoteES { get; set; }
        public string ReleaseNoteEN { get; set; }
        public bool Active { get; set; }
        public int ApkSize { get; set; }
        public string DowngradeJustify { get; set; }
        public int VersionCode { get; set; }
        public Application Application { get; set; }
        public OperatingSystem OperatingSystem { get; set; }
        public List<Model.OperationalSystem> OpSystems{ get; set;}
        public HttpPostedFileBase ApkData { get; set; }

    }
}
