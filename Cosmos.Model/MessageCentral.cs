﻿using System;
using System.Collections.Generic;

namespace Cosmos.Model
{
    public class MessageCentral
    {
        public Guid Id { get; set; }

        public MessageType MessageType { get; set; }

        public List<MessageText> MessageText { get; set; }

        public DateTime ExpiredDate { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public MessageStatus MessageStatus { get; set; }
        public DateTime PublishedDate { get; set; }
    }
}