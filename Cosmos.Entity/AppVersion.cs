//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cosmos.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppVersion
    {
        public string ApplicationId { get; set; }
        public string Version { get; set; }
        public string Package { get; set; }
        public string ApkUri { get; set; }
        public System.DateTime ReleaseDate { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<int> VersionCode { get; set; }
        public Nullable<bool> Downgrade { get; set; }
        public string ReleaseNote { get; set; }
        public string ReleaseNoteES { get; set; }
        public string ReleaseNoteEN { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<int> ApkSize { get; set; }
        public string DowngradeJustify { get; set; }
        public string Id { get; set; }
    
        public virtual Application Application { get; set; }
    }
}
