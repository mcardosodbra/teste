//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cosmos.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocumentOwner
    {
        public string Id { get; set; }
        public string OwnerId { get; set; }
        public string DocumentId { get; set; }
        public bool Readed { get; set; }
        public bool Status { get; set; }
    
        public virtual Document Document { get; set; }
    }
}
