dashboard = window.dashboard || {};

Dropzone.autoDiscover = false;
$(document).ready(function(){
  $("div.dropzone").dropzone({ 
    url: "/upload.php" ,
    dictDefaultMessage: "Arraste o arquivo até aqui, ou clique para selecionar!",
    dictInvalidFileType: "ERRO: Tipo de arquivo não permitido!"
  });
  formato =$('[name="formato_mensagem"]:checked').val();
  if(formato == 'texto') {
    $('.tipo-mensagem-form').show();
  }
  if(formato == 'imagem') {
    $('.tipo-imagem-form').show();
  }
  $('[name="formato_mensagem"]').change(function(){
    $('.formato-mensagem').hide();
    formato =$('[name="formato_mensagem"]:checked').val();
    if(formato == 'texto') {
      $('.tipo-mensagem-form').show();
    }
    if(formato == 'imagem') {
      $('.tipo-imagem-form').show();
    }
  });
  $('.tipo-mensagem-form textarea').keydown(function(e){
    var count = 260 - $(this).val().length;
    var k = e.which || e.keyCode
    if(count <= 0 && ((k >= 48 && k <= 90) || k == 32)) {
      return false;
    }
  })
  $('.tipo-mensagem-form textarea').keyup(function(e){
    var count = 260 - $(this).val().length;
    var k = e.which || e.keyCode
    //console.log(k)
    if(count <= 0 && ((k >= 48 && k <= 90) || k == 32)) {
      return false;
    }
    $('.tipo-mensagem-form small span').html( count )
  })
  $scrollbarh = $(".custom-scrollbar-h");
$scrollbarh.tinyscrollbar({ axis: 'x'});
$scrollbar = $("._custom-scrollbar");
$scrollbar.tinyscrollbar();
$scrollbarCidades = $(".custom-scrollbar-cidades");
$scrollbarCidades.tinyscrollbar();  
$(".custom-scrollbar").each(function(){
  $(this).tinyscrollbar();
})
  $.datepicker.regional['pt-BR'] = {
          closeText: 'Fechar',
          prevText: '&#x3c;Anterior',
          nextText: 'Pr&oacute;ximo&#x3e;',
          currentText: 'Hoje',
          monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
          'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
          monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
          'Jul','Ago','Set','Out','Nov','Dez'],
          dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
          dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
          dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
          weekHeader: 'Sm',
          dateFormat: 'dd/mm/yy',
          firstDay: 0,
          isRTL: false,
          showMonthAfterYear: false,
          yearSuffix: ''};
  $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
  $('.date').datepicker({
      changeMonth: true,
      changeYear: true,
    dateFormat: 'dd/mm/yy'
  });
  
  $('.e-cidade').removeClass('active')
});

$(document).on('click', '.remove-veiculo', function(){
  $(this).parents('tr').remove();
  var scrollbar5 = $scrollbar.data("plugin_tinyscrollbar")
  $(".custom-scrollbar").data('plugin_tinyscrollbar').update();
})            
$(document).on('change', '[name="regiao[]"]', function(){
  $('.e-regiao').hide();
  $('[name="regiao[]"]:checked').each(function(){
    regiao = $(this).val();
    $('.e-regiao-' + regiao).show();
  });    
  var scrollbar5 = $scrollbar.data("plugin_tinyscrollbar")
  $(".custom-scrollbar").data('plugin_tinyscrollbar').update();
});
$(document).on('change', '[name="estado[]"]', function(){
  //$('.e-cidade').hide();
  $('.e-tab').removeClass('visible');
  $('[name="estado[]"]:checked').each(function(){
    estado = $(this).val();
    //$('.e-cidade-' + estado).show();
    $('.e-tab-' + estado).addClass('visible');
  });
});
$(document).on('click', '[name="todas_regioes"]', function(){
  lista = $('.lista-regioes li:visible')
  if($(this).is(':checked')) {
    $('[name="regiao[]"]:not(:checked)', lista).click()
  } else {
    $('[name="regiao[]"]:checked', lista).click()
  }
});
$(document).on('click', '[name="todas_estados"]', function(){
  lista = $('.lista-estados li:visible')
  if($(this).is(':checked')) {
    $('[name="estado[]"]:not(:checked)', lista).click()
  } else {
    $('[name="estado[]"]:checked', lista).click()
  }
});
$(document).on('click', '[name="todas_cidades"]', function(){
  lista = $('.lista-cidades li:visible')
  if($(this).is(':checked')) {
    $('[name="cidade[]"]:not(:checked)', lista).click()
  } else {
    $('[name="cidade[]"]:checked', lista).click()
  }
  setTimeout(function(){
    var scrollbar5 = $scrollbar.data("plugin_tinyscrollbar")
    $(".custom-scrollbar").data('plugin_tinyscrollbar').update();
  }, 200);
});  
$(document).on('click', '.e-tab', function(){
  $('.e-tab').not(this).removeClass('active');
  uf = $(this).data('uf');
  $('.e-cidade').not('.e-cidade-' + uf).removeClass('active');
  if(!$(this).hasClass('.active')) {
    $(this).toggleClass('active');
    $('.e-cidade-' + uf).toggleClass('active');
  }
  setTimeout(function(){
    var scrollbar5 = $scrollbarCidades.data("plugin_tinyscrollbar")
    $(".custom-scrollbar-cidades").data("plugin_tinyscrollbar").update();
  }, 200);
});        
  dashboard.adicionarVeiculo = function () {
    var modelo = $('[name="modelo"]').val();
    var ano = $('[name="ano"]').val();
    $('.section.veiculos table tbody').append('<tr><td><input readonly name="modelo[]" value="' + modelo + '" /></td><td><input readonly name="modelo[]" value="' + ano + '" /><a href="javascript:;" class="remove-veiculo">x</a></td></tr>');
    $.fancybox.close('all');
    setTimeout(function(){
      var scrollbar5 = $scrollbar.data("plugin_tinyscrollbar")
      $(".custom-scrollbar").data('plugin_tinyscrollbar').update();
    }, 500)
  }
  $(document).bind("DOMSubtreeModified", function(){
    setTimeout(function(){
      var scrollbar5 = $scrollbar.data("plugin_tinyscrollbar")
      $(".custom-scrollbar").data('plugin_tinyscrollbar').update();
    }, 1000)
  });